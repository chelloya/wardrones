var webSocket = new WebSocket("ws://192.168.1.9:8080/wardrones/websocketendpoint");
var message ="";
webSocket.onopen = function(message){ wsOpen(message);};
webSocket.onmessage = function(message){ wsGetMessage(message);};

var distancia_t1=150;
var distancia_t2=150;
var distancia_a1=300;
var distancia_a2=300;



var id="";
var mascara="";
var objTerrestre1= {apodo: "Terrestre1", x:"0", y:"0", z:"0", id:"", camara: "true", canion: "true", municiones: "5", blindaje: "0", angulo:"0",disparar:"false", destruido: "false", disparador:""}; 
var objTerrestre2= {apodo: "Terrestre2", x:"0", y:"0", z:"0", id:"", camara: "true", canion: "true", municiones: "5", blindaje: "0", angulo:"0",disparar:"false", destruido: "false", disparador:""};
var objAereo1= {apodo: "Aereo1", x:"0", y:"0", z:"0", id:"", camara: "true", canion: "true", municiones: "3", bomba: "true", motor: "0", angulo:"0",disparar:"false", destruido: "false", disparador:""}; 
var objAereo2= {apodo: "Aereo2", x:"0", y:"0", z:"0", id:"", camara: "true", canion: "true", municiones: "3", bomba: "true", motor: "0", angulo:"0",disparar:"false", destruido: "false", disparador:""};
var objBase= {x1: "0", y1: "0", x2: "0", y2: "0", x3: "0", y3: "0", x4: "0", y4: "0", vidaPolvorin: "0", vidaPista: "0", destruido: "false"};
var objPausa = {estaPausada: "false"};

var pasaje0= {x1: "0", y1:"0", x2: "0", y2:"0", x3: "0", y3:"0", x4: "0", y4:"0", esPuente:"false"};
var pasaje1= {x1: "0", y1:"0", x2: "0", y2:"0", x3: "0", y3:"0", x4: "0", y4:"0", esPuente:"false"};
var pasaje2= {x1: "0", y1:"0", x2: "0", y2:"0", x3: "0", y3:"0", x4: "0", y4:"0", esPuente:"false"};
var pasaje3= {x1: "0", y1:"0", x2: "0", y2:"0", x3: "0", y3:"0", x4: "0", y4:"0", esPuente:"false"};
var pasaje4= {x1: "0", y1:"0", x2: "0", y2:"0", x3: "0", y3:"0", x4: "0", y4:"0", esPuente:"false"};
var pasaje5= {x1: "0", y1:"0", x2: "0", y2:"0", x3: "0", y3:"0", x4: "0", y4:"0", esPuente:"false"};
var pasaje6= {x1: "0", y1:"0", x2: "0", y2:"0", x3: "0", y3:"0", x4: "0", y4:"0", esPuente:"false"};
var pasaje7= {x1: "0", y1:"0", x2: "0", y2:"0", x3: "0", y3:"0", x4: "0", y4:"0", esPuente:"false"};
var pasaje8= {x1: "0", y1:"0", x2: "0", y2:"0", x3: "0", y3:"0", x4: "0", y4:"0", esPuente:"false"};
var pasaje9= {x1: "0", y1:"0", x2: "0", y2:"0", x3: "0", y3:"0", x4: "0", y4:"0", esPuente:"false"};
var pasaje10= {x1: "0", y1:"0", x2: "0", y2:"0", x3: "0", y3:"0", x4: "0", y4:"0", esPuente:"false"};
var pasaje11= {x1: "0", y1:"0", x2: "0", y2:"0", x3: "0", y3:"0", x4: "0", y4:"0", esPuente:"false"};
var pasaje12= {x1: "0", y1:"0", x2: "0", y2:"0", x3: "0", y3:"0", x4: "0", y4:"0", esPuente:"false"};




function wsOpen(message)
{
	
}

function wsSendMessage(msj)
{
	webSocket.send(msj);
}

function wsCloseConnection()
{
	webSocket.close();
}

function wsGetMessage(message)
{	
	console.log(message.data);
	
	var index = message.data.indexOf("_"); 
	var accion = message.data.substr(0, index); 
	var parts = message.data.substr(index + 1);
	var obj = JSON.parse(parts);
	
	switch(accion){
		case "mover":
			if (obj.rol == "A1") 
			{
				objAereo1.x=obj.x;
				objAereo1.y=obj.y;
				objAereo1.id=obj.id;
				objAereo1.angulo=obj.angulo;
			}
			
			if 
			(obj.rol == "A2")
			{
				objAereo2.x=obj.x;
				objAereo2.y=obj.y;
				objAereo2.id=obj.id;
				objAereo2.angulo=obj.angulo;
			}
			
			if 
			(obj.rol == "T1") 
			{
				objTerrestre1.x=obj.x;
				objTerrestre1.y=obj.y;
				objTerrestre1.id=obj.id;
				objTerrestre1.angulo=obj.angulo;
			}
			
			if 
			(obj.rol == "T2") 
			{
				objTerrestre2.x=obj.x;
				objTerrestre2.y=obj.y;
				objTerrestre2.id=obj.id;
				objTerrestre2.angulo=obj.angulo;
			}
		break;
		case "participantes":
			if (id=="")
			{
				id=obj.myid;
			}
			
			$('#lista-aereo').empty();
			$('#lista-terrestre').empty();
			$('#lista-espera').empty();
			
			var i=0;
			while (i<obj.participantes.length)
			{
				switch (obj.participantes[i].stringRol)
				{
					case "A1":
					case "A2":
						$('#lista-aereo').append("<li class='jugadorSeleccionado'>"+obj.participantes[i].apodo+"</li>");
					break;
					case "T1":
					case "T2":
						$('#lista-terrestre').append("<li class='jugadorSeleccionado'>"+obj.participantes[i].apodo+"</li>");
					break;
					case "noAsignado":
						$('#lista-espera').append("<li class='jugadorSeleccionado'>"+obj.participantes[i].apodo+"</li>");
					break;
				}
				i++;
			}
			
			$("#seccion-apodo").hide();
			$("#seccion-lobby").show();
		break;
		case "participantesEnLobby":
			$('#lista-aereo').empty();
			$('#lista-terrestre').empty();
			$('#lista-espera').empty();
			
			var i=0;
			while (i<obj.participantesEnLobby.length)
			{
				switch (obj.participantesEnLobby[i].stringRol)
				{
					case "A1":
					case "A2":
						$('#lista-aereo').append("<li class='jugadorSeleccionado'>"+obj.participantesEnLobby[i].apodo+"</li>");
					break;
					case "T1":
					case "T2":
						$('#lista-terrestre').append("<li class='jugadorSeleccionado'>"+obj.participantesEnLobby[i].apodo+"</li>");
					break;
					case "noAsignado":
						$('#lista-espera').append("<li class='jugadorSeleccionado'>"+obj.participantesEnLobby[i].apodo+"</li>");
					break;
				}
				i++;
			}
			
			$("#seccion-apodo").hide();
			$("#seccion-lobby").show();
		break;
		case "iniciar":
			$("#estado-partida").empty();			
			$("#estado-partida").append("<li><p style='color: #e74c3c; font-size: 15px;'><span class='fontawesome-circle' style='color: #e74c3c; margin-right: 6px;'></span>Partida en curso</p></li>");
			//A1
			objAereo1.apodo = obj.drones[2].apodo;
			objAereo1.id = obj.drones[2].id;
			objAereo1.x = obj.drones[2].x;
			objAereo1.y = obj.drones[2].y;
			objAereo1.z = obj.drones[2].z;
			objAereo1.camara = obj.drones[2].camara;
			objAereo1.canion = obj.drones[2].canion;
			objAereo1.municiones = obj.drones[2].municiones;
			objAereo1.bomba = obj.drones[2].bomba;
			objAereo1.motor = obj.drones[2].motor;
			objAereo1.angulo = obj.drones[2].angulo;
			objAereo1.destruido = obj.drones[2].destruido;
			
			//A2
			objAereo2.apodo = obj.drones[3].apodo;
			objAereo2.id = obj.drones[3].id;
			objAereo2.x = obj.drones[3].x;
			objAereo2.y = obj.drones[3].y;
			objAereo2.z = obj.drones[3].z;
			objAereo2.camara = obj.drones[3].camara;
			objAereo2.canion = obj.drones[3].canion;
			objAereo2.municiones = obj.drones[3].municiones;
			objAereo2.bomba = obj.drones[3].bomba;
			objAereo2.motor = obj.drones[3].motor;
			objAereo2.angulo = obj.drones[3].angulo;
			objAereo2.destruido = obj.drones[3].destruido;
			
			//T1
			objTerrestre1.apodo = obj.drones[0].apodo;
			objTerrestre1.id = obj.drones[0].id;
			objTerrestre1.x = obj.drones[0].x;
			objTerrestre1.y = obj.drones[0].y;
			objTerrestre1.z = obj.drones[0].z;
			objTerrestre1.camara = obj.drones[0].camara;
			objTerrestre1.canion = obj.drones[0].canion;
			objTerrestre1.municiones = obj.drones[0].municiones;
			objTerrestre1.blindaje = obj.drones[0].blindaje;
			objTerrestre1.angulo = obj.drones[0].angulo;
			objTerrestre1.destruido = obj.drones[0].destruido;
			
			//T2
			objTerrestre2.apodo = obj.drones[1].apodo;
			objTerrestre2.id = obj.drones[1].id;
			objTerrestre2.x = obj.drones[1].x;
			objTerrestre2.y = obj.drones[1].y;
			objTerrestre2.z = obj.drones[1].z;
			objTerrestre2.camara = obj.drones[1].camara;
			objTerrestre2.canion = obj.drones[1].canion;
			objTerrestre2.municiones = obj.drones[1].municiones;
			objTerrestre2.blindaje = obj.drones[1].blindaje;
			objTerrestre2.angulo = obj.drones[1].angulo;
			objTerrestre2.destruido = obj.drones[1].destruido;
			
			//BASE
			objBase.x1 = obj.base.x1;
			objBase.x2 = obj.base.x2;
			objBase.x3 = obj.base.x3;
			objBase.x4 = obj.base.x4;
			objBase.y1 = obj.base.y1;
			objBase.y2 = obj.base.y2;
			objBase.y3 = obj.base.y3;
			objBase.y4 = obj.base.y4;
			objBase.vidaPolvorin = obj.base.vidaPolvorin;
			objBase.vidaPista = obj.base.vidaPista;
			
			
			pasaje0 = {x1: obj.puentes[0].x1, y1: obj.puentes[0].y1, x2: obj.puentes[0].x2, y2: obj.puentes[0].y2, x3: obj.puentes[0].x3, y3: obj.puentes[0].y3, x4: obj.puentes[0].x4, y4: obj.puentes[0].y4, esPuente: obj.puentes[0].esPuente};
			pasaje1 = {x1: obj.puentes[1].x1, y1: obj.puentes[1].y1, x2: obj.puentes[1].x2, y2: obj.puentes[1].y2, x3: obj.puentes[1].x3, y3: obj.puentes[1].y3, x4: obj.puentes[1].x4, y4: obj.puentes[1].y4, esPuente: obj.puentes[1].esPuente};
			pasaje2 = {x1: obj.puentes[2].x1, y1: obj.puentes[2].y1, x2: obj.puentes[2].x2, y2: obj.puentes[2].y2, x3: obj.puentes[2].x3, y3: obj.puentes[2].y3, x4: obj.puentes[2].x4, y4: obj.puentes[2].y4, esPuente: obj.puentes[2].esPuente};
			pasaje3 = {x1: obj.puentes[3].x1, y1: obj.puentes[3].y1, x2: obj.puentes[3].x2, y2: obj.puentes[3].y2, x3: obj.puentes[3].x3, y3: obj.puentes[3].y3, x4: obj.puentes[3].x4, y4: obj.puentes[3].y4, esPuente: obj.puentes[3].esPuente};
			pasaje4 = {x1: obj.puentes[4].x1, y1: obj.puentes[4].y1, x2: obj.puentes[4].x2, y2: obj.puentes[4].y2, x3: obj.puentes[4].x3, y3: obj.puentes[4].y3, x4: obj.puentes[4].x4, y4: obj.puentes[4].y4, esPuente: obj.puentes[4].esPuente};
			pasaje5 = {x1: obj.puentes[5].x1, y1: obj.puentes[5].y1, x2: obj.puentes[5].x2, y2: obj.puentes[5].y2, x3: obj.puentes[5].x3, y3: obj.puentes[5].y3, x4: obj.puentes[5].x4, y4: obj.puentes[5].y4, esPuente: obj.puentes[5].esPuente};
			pasaje6 = {x1: obj.puentes[6].x1, y1: obj.puentes[6].y1, x2: obj.puentes[6].x2, y2: obj.puentes[6].y2, x3: obj.puentes[6].x3, y3: obj.puentes[6].y3, x4: obj.puentes[6].x4, y4: obj.puentes[6].y4, esPuente: obj.puentes[6].esPuente};
			pasaje7 = {x1: obj.puentes[7].x1, y1: obj.puentes[7].y1, x2: obj.puentes[7].x2, y2: obj.puentes[7].y2, x3: obj.puentes[7].x3, y3: obj.puentes[7].y3, x4: obj.puentes[7].x4, y4: obj.puentes[7].y4, esPuente: obj.puentes[7].esPuente};
			pasaje8 = {x1: obj.puentes[8].x1, y1: obj.puentes[8].y1, x2: obj.puentes[8].x2, y2: obj.puentes[8].y2, x3: obj.puentes[8].x3, y3: obj.puentes[8].y3, x4: obj.puentes[8].x4, y4: obj.puentes[8].y4, esPuente: obj.puentes[8].esPuente};
			pasaje9 = {x1: obj.puentes[9].x1, y1: obj.puentes[9].y1, x2: obj.puentes[9].x2, y2: obj.puentes[9].y2, x3: obj.puentes[9].x3, y3: obj.puentes[9].y3, x4: obj.puentes[9].x4, y4: obj.puentes[9].y4, esPuente: obj.puentes[9].esPuente};
			pasaje10 = {x1: obj.puentes[10].x1, y1: obj.puentes[10].y1, x2: obj.puentes[10].x2, y2: obj.puentes[10].y2, x3: obj.puentes[10].x3, y3: obj.puentes[10].y3, x4: obj.puentes[10].x4, y4: obj.puentes[10].y4, esPuente: obj.puentes[10].esPuente};
			pasaje11 = {x1: obj.puentes[11].x1, y1: obj.puentes[11].y1, x2: obj.puentes[11].x2, y2: obj.puentes[11].y2, x3: obj.puentes[11].x3, y3: obj.puentes[11].y3, x4: obj.puentes[11].x4, y4: obj.puentes[11].y4, esPuente: obj.puentes[11].esPuente};
			pasaje12 = {x1: obj.puentes[12].x1, y1: obj.puentes[12].y1, x2: obj.puentes[12].x2, y2: obj.puentes[12].y2, x3: obj.puentes[12].x3, y3: obj.puentes[12].y3, x4: obj.puentes[12].x4, y4: obj.puentes[12].y4, esPuente: obj.puentes[12].esPuente};
			
			////PARA CAMBIO DE DRONE, MASCARAS /////
			switch (id)
			{
				case objTerrestre1.id:
					mascara="T1";
				break;
				case objTerrestre2.id:
					mascara="T2";
				break;
				case objAereo1.id:
					mascara="A1";
				break;
				case objAereo2.id:
					mascara="A2";
				break;
			}

			$("#seccion-lobby").hide();
			
			init();
			
		break;
		case "cambiarAltura":
			if(obj.id == 0)
				objTerrestre1.z = obj.z;
			if(obj.id == 1)
				objTerrestre2.z = obj.z;
			if (obj.id == 2)
				objAereo1.z = obj.z;
			if (obj.id == 3)
				objAereo2.z = obj.z;
		break;
		case "impacto":
			///DISPARA T1
			if(obj.rolDispara == "T1")
			{
				if(obj.municiones == "5")
					obTerrestre1.municiones = "5";
				if(obj.municiones == "4")
					objTerrestre1.municiones = "4";
				if(obj.municiones == "3")
					objTerrestre1.municiones = "3";
				if(obj.municiones == "2")
					objTerrestre1.municiones = "2";
				if(obj.municiones == "1")
					objTerrestre1.municiones = "1";
				if(obj.municiones == "0")
					objTerrestre1.municiones = "0";
				objTerrestre1.disparar=obj.disparar;
			}
			
			//RECIBE T1
			if(obj.rolRecibe == "T1")
			{
				if(obj.canion == "false")
					objTerrestre1.canion = "false";
				if(obj.camara == "false")
					objTerrestre1.camara = "false";
				if(obj.blindaje == "2"){
					objTerrestre1.blindaje = "2";
				}
				if (obj.blindaje == "1")
				{
					objTerrestre1.blindaje = "1";
				}
				if (obj.blindaje == "0")
				{
					objTerrestre1.blindaje = "0";
				}
				if(obj.destruido == "true")
					objTerrestre1.destruido = "true";
				
				////PARA DISPARO SINCRONIZADO
				objTerrestre1.disparador=obj.rolDispara;
			}
			
			///DISPARA T2
			if(obj.rolDispara == "T2") 
			{
				if(obj.municiones == "5")
					objTerrestre2.municiones = "5";
				if(obj.municiones == "4")
					objTerrestre2.municiones = "4";
				if(obj.municiones == "3")
					objTerrestre2.municiones = "3";
				if(obj.municiones == "2")
					objTerrestre2.municiones = "2";
				if(obj.municiones == "1")
					objTerrestre2.municiones = "1";
				if(obj.municiones == "0")
					objTerrestre2.municiones = "0";
				objTerrestre2.disparar=obj.disparar;
			}
			
			//RECIBE T2
			if(obj.rolRecibe == "T2")
			{
				if(obj.canion == "false")
					objTerrestre2.canion = "false";
				if(obj.camara == "false")
					objTerrestre2.camara = "false";
				if(obj.blindaje == "2"){
					objTerrestre2.blindaje = "2";
				}
				if (obj.blindaje == "1")
				{
					objTerrestre2.blindaje = "1";
				}
				if (obj.blindaje == "0")
				{
					objTerrestre2.blindaje = "0";
				}
				if(obj.destruido == "true")
					objTerrestre2.destruido = "true";
				
				objTerrestre2.disparador=obj.rolDispara;
			}

			///DISPARA A1
			if(obj.rolDispara == "A1") 
			{
				if(obj.municiones == "3")
					objAereo1.municiones = "3";
				if(obj.municiones == "2")
					objAereo1.municiones = "2";
				if(obj.municiones == "1")
					objAereo1.municiones = "1";
				if(obj.municiones == "0")
					objAereo1.municiones = "0";
				objAereo1.disparar=obj.disparar;
			}	

			//RECIBE A1
			if(obj.rolRecibe == "A1")
			{
				if(obj.canion == "false")
					objAereo1.canion = "false";
				if(obj.explotoBomba == "true")
					objAereo1.destruido = "true";
				if(obj.motor == "1") {
					objAereo1.motor = "1";
				}
				else if(obj.motor == "0")
				{
					objAereo1.motor = "0";
				}
				if(obj.camara == "false")
					objAereo1.camara = "false";
				if(obj.destruido == "true")
					objAereo1.destruido = "true";
				
				objAereo1.disparador=obj.rolDispara;
			}
					
			///DISPARA A2
			if(obj.rolDispara == "A2")
			{
				if(obj.municiones == "3")
					objAereo2.municiones = "3";
				if(obj.municiones == "2")
					objAereo2.municiones = "2";
				if(obj.municiones == "1")
					objAereo2.municiones = "1";
				if(obj.municiones == "0")
					objAereo2.municiones = "0";
				objAereo2.disparar=obj.disparar;
			}	

			//RECIBE A2
			if(obj.rolRecibe == "A2")
			{
				if(obj.canion == "false")
					objAereo2.canion = "false";
				if(obj.explotoBomba == "true")
					objAereo2.destruido = "true";
				if(obj.motor == "1") {
					objAereo2.motor = "1";
				}
				else if(obj.motor == "0")
				{
					objAereo2.motor = "0";
				}
				if(obj.camara == "false")
					objAereo2.camara = "false";
				if(obj.destruido == "true")
					objAereo2.destruido = "true";
				
				objAereo2.disparador=obj.rolDispara;
			}
			
			// RECIBE BASE
			if(obj.rolRecibe == "BASE"){
				if(obj.vidaPolvorin == "2")
					objBase.vidaPolvorin = "2";
				if(obj.vidaPolvorin == "1")
					objBase.vidaPolvorin = "1";
				if(obj.vidaPolvorin == "0")
					objBase.vidaPolvorin = "0";
				
				if(obj.vidaPista == "10")
					objBase.vidaPista = "10";
				if(obj.vidaPista == "9")
					objBase.vidaPista = "9";
				if(obj.vidaPista == "8")
					objBase.vidaPista = "8";
				if(obj.vidaPista == "7")
					objBase.vidaPista = "7";
				if(obj.vidaPista == "6")
					objBase.vidaPista = "6";
				if(obj.vidaPista == "5")
					objBase.vidaPista = "5";
				if(obj.vidaPista == "4")
					objBase.vidaPista = "4";
				if(obj.vidaPista == "3")
					objBase.vidaPista = "3";
				if(obj.vidaPista == "2")
					objBase.vidaPista = "2";
				if(obj.vidaPista == "1")
					objBase.vidaPista = "1";
				if(obj.vidaPista == "0")
					objBase.vidaPista = "0";
					
				if(obj.destruido == "true")
				{
					objBase.destruido = "true";
				}
				
				if(obj.finalizarPartida == "true")
				{
					$("#bando-ganador").empty();
					$("#sobrevivientes").empty();
					
					if(obj.bandoGanador == "terrestre")
						$("#bando-ganador").append("<h1>¡Bando ganador: terrestre!</h1>");
					else
						$("#bando-ganador").append("<h1>¡Bando ganador: aereo!</h1>");
					
					var i = 0;
					for(i = 0; i<obj.sobrevivientes.length; i++)
					{
						$("#sobrevivientes").append("<h2>"+obj.sobrevivientes[i].apodo+"</h2>");
					}
					
					mostrarEstadisticas();
					
					$("#estado-partida").empty();
					$("#estado-partida").append("<li><p style='color: #2ecc71; font-size: 15px;'><span class='fontawesome-circle' style='color: #2ecc71; margin-right: 6px;'></span>Partida habilitada</p></li>");
				}
			}

			////DISTANCIA DE LA BALA
			
			if(obj.rolDispara == "T1" && obj.rolRecibe == "A1")
			{
				distancia_t1=Math.sqrt(Math.pow((objTerrestre1.x - objAereo1.x),2) + Math.pow((objTerrestre1.y - objAereo1.y),2));				
			}
			
			
			if(obj.rolDispara == "T1" && obj.rolRecibe == "A2")
			{
				distancia_t1=Math.sqrt(Math.pow((objTerrestre1.x - objAereo2.x),2) + Math.pow((objTerrestre1.y - objAereo2.y),2));				
			}
			
			////DISTANCIA BALA A LA BASE
			
			if(obj.rolDispara == "T1" && obj.rolRecibe == "BASE")
			{
				distancia_t1=Math.sqrt(Math.pow((objTerrestre1.x - objBase.x1),2) + Math.pow((objTerrestre1.y - objBase.y1),2));				
			}
			
			
			if(obj.rolDispara == "T1" &&  typeof obj.rolRecibe == 'undefined')
				distancia_t1=150;
			
			if(obj.rolDispara == "T2" && obj.rolRecibe == "A1")
			{
				distancia_t2=Math.sqrt(Math.pow((objTerrestre2.x - objAereo1.x),2) + Math.pow((objTerrestre2.y - objAereo1.y),2));				
			}
			
			
			if(obj.rolDispara == "T2" && obj.rolRecibe == "A2")
			{
				distancia_t2=Math.sqrt(Math.pow((objTerrestre2.x - objAereo2.x),2) + Math.pow((objTerrestre2.y - objAereo2.y),2));				
			}
			
			
			if(obj.rolDispara == "T2" && obj.rolRecibe == "BASE")
			{
				distancia_t2=Math.sqrt(Math.pow((objTerrestre2.x - objBase.x1),2) + Math.pow((objTerrestre2.y - objBase.y1),2));				
			}
			
			if(obj.rolDispara == "T2" &&  typeof obj.rolRecibe == 'undefined')
				distancia_t2=150;
			
			
			if(obj.rolDispara == "A1" && obj.rolRecibe == "T1")
			{
				distancia_a1=Math.sqrt(Math.pow(( objAereo1.x -objTerrestre1.x),2) + Math.pow((objAereo1.y - objTerrestre1.y),2));				
			}
			
			
			if(obj.rolDispara == "A1" && obj.rolRecibe == "T2")
			{
				distancia_a1=Math.sqrt(Math.pow((objAereo1.x - objTerrestre2.x),2) + Math.pow((objAereo1.y-objTerrestre2.y),2));				
			}
			
			if(obj.rolDispara == "A1" &&  typeof obj.rolRecibe == 'undefined')
				distancia_a1=300;
			
			if(obj.rolDispara == "A2" && obj.rolRecibe == "T1")
			{
				distancia_a2=Math.sqrt(Math.pow((objAereo2.x - objTerrestre1.x),2) + Math.pow((objAereo2.y- objTerrestre1.y),2));				
			}
			
			
			if(obj.rolDispara == "A2" && obj.rolRecibe == "T2")
			{
				distancia_a2=Math.sqrt(Math.pow((objAereo2.x - objTerrestre2.x),2) + Math.pow((objAereo2.y - objTerrestre2.y),2));				
			}
			
			
			if(obj.rolDispara == "A2" &&  typeof obj.rolRecibe == 'undefined')
				distancia_a2=300;
			
			
			
			
			if(obj.finalizarPartida == "true")
			{
				$("#bando-ganador").empty();
				$("#sobrevivientes").empty();
				
				if(obj.bandoGanador == "terrestre")
					$("#bando-ganador").append("<h1>¡Bando ganador: terrestre!</h1>");
				else
					$("#bando-ganador").append("<h1>¡Bando ganador: aereo!</h1>");

				var i = 0;
				for(i = 0; i<obj.sobrevivientes.length; i++)
				{
					$("#sobrevivientes").append("<h2>"+obj.sobrevivientes[i].apodo+"</h2>");
				}
				
				mostrarEstadisticas();	
				
				$("#estado-partida").empty();
				$("#estado-partida").append("<li><p style='color: #2ecc71; font-size: 15px;'><span class='fontawesome-circle' style='color: #2ecc71; margin-right: 6px;'></span>Partida habilitada</p></li>");
			}
		break;
		case "cambiarDrone":
			if (id==obj.id)
				mascara=obj.mask;
		break;
		case "disparoBomba":
			if(obj.rolRecibe == "T1")
			{
				objTerrestre1.destruido = "true";
				if(obj.rolDispara == "A1")
					objAereo1.bomba = "false";
				if(obj.rolDispara == "A2")
					objAereo2.bomba = "false";
			}
			
			if(obj.rolRecibe == "T2")
			{
				objTerrestre2.destruido = "true";
				if(obj.rolDispara == "A1")
					objAereo1.bomba = "false";
				if(obj.rolDispara == "A2")
					objAereo2.bomba = "false";
			}
			if(obj.rolRecibe == "BASE")
			{
				objBase.destruido = "true";
				if(obj.rolDispara == "A1")
					objAereo1.bomba = "false";
				if(obj.rolDispara == "A2")
					objAereo2.bomba = "false";
			}
			
			if(obj.impacta == "false")
			{
				if(obj.rolDispara == "A1")
					objAereo1.bomba = "false";
				if(obj.rolDispara == "A2")
					objAereo2.bomba = "false";
			}
			
			if(obj.finalizarPartida == "true")
			{
	
				$("#bando-ganador").empty();
				$("#sobrevivientes").empty();
				
				if(obj.bandoGanador == "terrestre")
					$("#bando-ganador").append("<h1>¡Bando ganador: terrestre!</h1>");
				else
					$("#bando-ganador").append("<h1>¡Bando ganador: aereo!</h1>");

				var i = 0;
				for(i = 0; i<obj.sobrevivientes.length; i++)
				{
					$("#sobrevivientes").append("<h2>"+obj.sobrevivientes[i].apodo+"</h2>");
				}
				
				mostrarEstadisticas();
				
				$("#estado-partida").empty();
				$("#estado-partida").append("<li><p style='color: #2ecc71; font-size: 15px;'><span class='fontawesome-circle' style='color: #2ecc71; margin-right: 6px;'></span>Partida habilitada</p></li>");
			}
		break;
		case "recargar":
			if(obj.rol == "A1")
			{
				if(obj.bomba == "true")
				{
					objAereo1.bomba = "true";
				}
				objAereo1.municiones = "3";
			}
			
			if(obj.rol == "A2")
			{
				if(obj.bomba == "true")
				{
					objAereo2.bomba = "true";
				}
				objAereo2.municiones = "3";
			}
			
			if(obj.rol == "T1")
				objTerrestre1.municiones = "5";
			if(obj.rol == "T2")
				objTerrestre2.municiones = "5";
		break;
		/*case "listarPartidas":
			$('#listado-partidas').empty();
			
			var i = 0;
			while(i < obj.partidas.length){
				$('#listado-partidas').append("<option value='+obj.partidas[i].id+'>"+obj.partidas[i].id+" - "+ obj.partidas[i].fecha +"</option>");
				i++;
			}
			
			
		break;*/
		case "pausar":
			if(obj.pausar == "true"){
				objPausa.estaPausada = "true";
				$("#seccion-pausa").show();
			} else {
				objPausa.estaPausada = "false";
				$("#seccion-pausa").hide();
				$("#seccion-guardar").hide();
				$("#seccion-validar-guardar").hide();
				$("#seccion-controles-pausa").hide();
			}
		break;
		case "irAlApodo":
			$('#error-recuperar').empty();
			
			if(obj.error == "true")
			{
				$("#error-recuperar").append("<li>"+obj.texto+"</li>");
				$("#seccion-error-recuperar").show();				
			}
			else
			{
				$("#seccion-menu").hide();
				$("#seccion-apodo").show();
			}
		break;
		case "validarIrAlLobby":
			$('#error-recuperar').empty();
			
			if(obj.error == "true")
			{
				$("#error-recuperar").append("<li>"+obj.texto+"</li>");
				$("#seccion-error-recuperar").show();				
			}
		break;
		case "listarPartidas":
			$('#error-recuperar').empty();
			
			if (obj.error == "true")
			{				
				if(obj.texto == "Ya hay partida cargada")
				{
					$("#error-recuperar").append("<li>"+obj.texto+"</li>");
					$("#seccion-error-recuperar").show();
				}
				else
				{
					$("#error-recuperar").append("<li>"+obj.texto+"</li>");
					$("#seccion-error-recuperar").show();
				}
			}
			else
			{
				$("#seccion-menu").hide();
				$("#seccion-recuperar").show();
				$('#listado-partidas').empty();
				
				var i = 0;
				while(i < obj.partidas.length){
					$('#listado-partidas').append("<option value='+obj.partidas[i].id+'>"+obj.partidas[i].id+" - "+ obj.partidas[i].fecha +"</option>");
					i++;
				}
			}
		break;
		case "recuperar":
			if (obj.error == "true")
			{
				if(obj.texto == "Ya hay partida cargada")
				{
					$("#error-recuperar").append("<li>"+obj.texto+"</li>");
					
					$("#seccion-error-recuperar").show();
				}
				else
				{
					$("#error-recuperar").append("<li>"+obj.texto+"</li>");
					$("#seccion-error-recuperar").show();
				}
			}
			else
			{
				$("#seccion-recuperar").hide();
				$("#seccion-apodo").show();
			}
		break;
		case "guardar":
			$('#texto-validacion').empty();
			
			if(obj.error == "true")
			{
				$("#texto-validacion").append("<li><p class='error'>"+obj.texto+"</p></li>");
			}
			else
			{
				$("#texto-validacion").append("<li><p class='exito'>"+obj.texto+"</p></li>");
			}
			
			$("#seccion-guardar").hide();
			$("#seccion-validar-guardar").show();
		break;
		case "validarIrAGuardar":
			$("#texto-validacion").empty();
			
			if(obj.error == "true")
			{
				$("#texto-validacion").append("<li><p>"+obj.texto+"</p></li>");
				$("#seccion-validar-guardar").show();
			}
			else if (obj.error == "false")
			{
				$("#seccion-pausa").hide();
				$("#seccion-guardar").show();
			}
		break;
		case "abandonoSesion":
			if(obj.rol == "T1")
			{	
				if(obj.unJugador == "true")
				{
					objTerrestre1.destruido = "true";
					objTerrestre2.destruido = "true";
				}
				else
					objTerrestre1.destruido = "true";
			}
			
			if(obj.rol == "T2")
			{
				if(obj.unJugador == "true")
				{
					objTerrestre1.destruido = "true";
					objTerrestre2.destruido = "true";
				}
				else
					objTerrestre2.destruido = "true";
			}
			
			if(obj.rol == "A1")
			{
				if(obj.unJugador == "true")
				{
					objAereo1.destruido = "true";
					objAereo2.destruido = "true";
				}
				else
					objAereo1.destruido = "true";
			}
			
			if(obj.rol == "A2")
			{
				if(obj.unJugador == "true")
				{
					objAereo1.destruido = "true";
					objAereo2.destruido = "true";
				}
				else
					objAereo2.destruido = "true";
			}
			
			if(obj.finalizarPartida == "true")
			{
				$("#bando-ganador").empty();
				$("#sobrevivientes").empty();
				
				if(obj.bandoGanador == "terrestre")
					$("#bando-ganador").append("<h1>¡Bando ganador: terrestre!</h1>");
				else
					$("#bando-ganador").append("<h1>¡Bando ganador: aereo!</h1>");
				
				var i = 0;
				for(i = 0; i<obj.sobrevivientes.length; i++)
				{
					$("#sobrevivientes").append("<h2>"+obj.sobrevivientes[i].apodo+"</h2>");
				}
				
				mostrarEstadisticas();	
				
				$("#estado-partida").empty();
				$("#estado-partida").append("<li><p style='color: #2ecc71; font-size: 15px;'><span class='fontawesome-circle' style='color: #2ecc71; margin-right: 6px;'></span>Partida habilitada</p></li>");
			}
		break;
		case "abandonoPartida":
			if(obj.rol == "T1")
			{	
				if(obj.unJugador == "true")
				{
					objTerrestre1.destruido = "true";
					objTerrestre2.destruido = "true";
				}
				else
					objTerrestre1.destruido = "true";
			}
			
			if(obj.rol == "T2")
			{
				if(obj.unJugador == "true")
				{
					objTerrestre1.destruido = "true";
					objTerrestre2.destruido = "true";
				}
				else
					objTerrestre2.destruido = "true";
			}
			
			if(obj.rol == "A1")
			{
				if(obj.unJugador == "true")
				{
					objAereo1.destruido = "true";
					objAereo2.destruido = "true";
				}
				else
					objAereo1.destruido = "true";
			}
			
			if(obj.rol == "A2")
			{
				if(obj.unJugador == "true")
				{
					objAereo1.destruido = "true";
					objAereo2.destruido = "true";
				}
				else
					objAereo2.destruido = "true";
			}
			
			if(obj.finalizarPartida == "true")
			{
				$("#bando-ganador").empty();
				$("#sobrevivientes").empty();
				
				if(obj.bandoGanador == "terrestre")
					$("#bando-ganador").append("<h1>¡Bando ganador: terrestre!</h1>");
				else
					$("#bando-ganador").append("<h1>¡Bando ganador: aereo!</h1>");

				var i = 0;
				for(i = 0; i<obj.sobrevivientes.length; i++)
				{
					$("#sobrevivientes").append("<h2>"+obj.sobrevivientes[i].apodo+"</h2>");
				}
				
				mostrarEstadisticas();
				
				$("#estado-partida").empty();
				$("#estado-partida").append("<li><p style='color: #2ecc71; font-size: 15px;'><span class='fontawesome-circle' style='color: #2ecc71; margin-right: 6px;'></span>Partida habilitada</p></li>");
			}
		break;
		case "estadoPartida":
			$("#estado-partida").empty();
			if(obj.partidaIniciada == "true")
				$("#estado-partida").append("<li><p style='color: #e74c3c; font-size: 15px;'><span class='fontawesome-circle' style='color: #e74c3c; margin-right: 6px;'></span>Partida en curso</p></li>");
			else
				$("#estado-partida").append("<li><p style='color: #2ecc71; font-size: 15px;'><span class='fontawesome-circle' style='color: #2ecc71; margin-right: 6px;'></span>Partida habilitada</p></li>");
		break;
	}
	
}

function wsClose(message){
	
}

function wserror(message){
	
}



