
var init = function()
{	
	//PARA DISPARO SINCRONIZADO
	var centinelaT1=false;
	var centinelaT2=false;
	var centinelaA1=false;
	var centinelaA2=false;
	
	var yasonoT1=false;
	var yasonoT2=false;
	var yasonoA1=false;
	var yasonoA2=false;
	
	var vBala=600;
	
	var game = new Phaser.Game(1152, 780, Phaser.CANVAS, 'phaser-example', { preload: preload, create: create, update: update, render: render });
	
	//PARA VISTA LATERAL
	if(typeof mascara != 'undefined')
	if (mascara != "")
	var lateral=new Phaser.Game(600, 200, Phaser.CANVAS, 'lateral', { preload: preloadL, create: createL, update: updateL, render: renderL });
	
	var T1lateral;
	var T2lateral;
	var A1lateral;
	var A2lateral;
	
	var miraT1;
	var miraT2;
	
	var dMascaraAereo=600;
	var dMascaraTerrestre=300;
	
	
	var alcanceT1=dMascaraTerrestre/2;
	var alcanceT2=dMascaraTerrestre/2;
	var alcanceA1=dMascaraAereo/2;
	var alcanceA2=dMascaraAereo/2;
	
	var alcanceAereoRoto=alcanceA1*0.2;
	var alcanceTerrestreRoto=alcanceT1*.02;
	
	
	function actualizaVistaLateral(observador,enemigo,coordenadas,objAereo,radio)
	{

		if (Math.sqrt(Math.pow((observador.x - enemigo.x),2) + Math.pow((observador.y - enemigo.y),2)) < radio)
		{
			//////PROYECCIONES SOBRE EL EJE DE DISPARO DEL DRONE
			var yaereo=1-((Math.cos(observador.rotation)*(enemigo.y-observador.y)+Math.sin(observador.rotation)*(observador.x-enemigo.x))/150);
	
			coordenadas.x=lateral.world.centerX+Math.cos(observador.rotation)*(enemigo.x-observador.x)+Math.sin(observador.rotation)*(enemigo.y-observador.y);
			
			coordenadas.scale.x=yaereo;
			coordenadas.scale.y=Math.abs(yaereo);
			
			coordenadas.visible=true;
			
			
			///////NIVELES AEREO
			var zaereo=lateral.height/3;
			
			if (objAereo)
			switch (objAereo.z*1)
			{
			case 0:
				coordenadas.y=(zaereo*3)-25;
				break;
			case 1:
				coordenadas.y=(zaereo*2)-25;
				break;
			case 2:
				coordenadas.y=zaereo-25;
				break;
			}
			

	
		}	
		else coordenadas.visible=false;
				
	}
	
	function alturaDrone(drone,objA)
	{
		var zaereo=lateral.height/3;
		
		if (objA)
		switch (objA.z*1)
		{
		case 0:
			drone.y=(zaereo*3)-25;
			break;
		case 1:
			drone.y=(zaereo*2)-25;
			break;
		case 2:
			drone.y=zaereo-25;
			break;
		}
	}
	
	
	function posCanion(canion,objTerrestre)
	{
		
		var zterrestre=lateral.height/3;
		
		if (objTerrestre)
		switch (objTerrestre.z*1)
		{
		case 0:
			canion.y=zterrestre*2;
			break;
		case 1:
			canion.y=zterrestre;
			break;

		}
	}

	
	function preloadL()
	{
		lateral.load.image('droneterrestre', 'assets/drones/terrestreLateral.png');
		lateral.load.image('droneaereo', 'assets/drones/aereoLateral.png');
		
	}
	
	function createL()
	{
		
		lateral.stage.disableVisibilityChange = true;
		
		T1lateral = lateral.add.sprite(lateral.world.centerX, lateral.height-25, 'droneterrestre');
		T1lateral.anchor.setTo(0.5, 0.5);
		T1lateral.visible=false;
		
		
		A1lateral = lateral.add.sprite(lateral.world.centerX, lateral.height-25, 'droneaereo');
		A1lateral.anchor.setTo(0.5, 0.5);
		A1lateral.visible=false;
		
		A2lateral = lateral.add.sprite(lateral.world.centerX, lateral.height-25, 'droneaereo');
		A2lateral.anchor.setTo(0.5, 0.5);
		A2lateral.visible=false;
		
		
		T2lateral = lateral.add.sprite(lateral.world.centerX, lateral.height-25, 'droneterrestre');
		T2lateral.anchor.setTo(0.5, 0.5);
		T2lateral.visible=false;
		
		
		miraT1 = lateral.add.graphics(0,0);
		miraT1.beginFill(0xbfbfbf);
		miraT1.drawRect(0,0,lateral.width,lateral.height/3);
		miraT1.alpha = 0.2;
		
		
		miraT2 = lateral.add.graphics(0,0);
		miraT2.beginFill(0xbfbfbf);
		miraT2.drawRect(0,0,lateral.width,lateral.height/3);
		miraT2.alpha = 0.2;
		
		miraT1.visible=false;
		miraT2.visible=false;
			
	}
	function updateL()
	{	
		
		if((typeof terrestre1 != 'undefined') && (typeof terrestre2 != 'undefined') 
				&& (typeof aereo1 != 'undefined') && (typeof aereo2 != 'undefined'))
		{
			if (mascara=="T1" && id==objTerrestre1.id)
			{	
				T1lateral.x=lateral.world.centerX;
				T1lateral.y=lateral.height-25;
				T1lateral.scale.x=1;
				T1lateral.scale.y=1;
				T1lateral.visible=true;
				posCanion(miraT1,objTerrestre1);
				miraT1.visible=true;
				miraT2.visible=false;
				
				actualizaVistaLateral(terrestre1,aereo1,A1lateral,objAereo1,alcanceT1);
				actualizaVistaLateral(terrestre1,aereo2,A2lateral,objAereo2,alcanceT1);
				actualizaVistaLateral(terrestre1,terrestre2,T2lateral,null,alcanceT1);
			}
	
			
			if (mascara=="T2" && id == objTerrestre2.id)
			{	
				T2lateral.x=lateral.world.centerX;
				T2lateral.y=lateral.height-25;
				T2lateral.scale.x=1;
				T2lateral.scale.y=1;
				T2lateral.visible=true;
				posCanion(miraT2,objTerrestre2);
				miraT2.visible=true;
				miraT1.visible=false;
				
				actualizaVistaLateral(terrestre2,aereo1,A1lateral,objAereo1,alcanceT2);
				actualizaVistaLateral(terrestre2,aereo2,A2lateral,objAereo2,alcanceT2);
				actualizaVistaLateral(terrestre2,terrestre1,T1lateral,null,alcanceT2);
			}
			
			if (mascara=="A1" && id==objAereo1.id)
			{
				A1lateral.x=lateral.world.centerX;
				alturaDrone(A1lateral,objAereo1);
				A1lateral.scale.x=1;
				A1lateral.scale.y=1;
				A1lateral.visible=true;
				
				actualizaVistaLateral(aereo1,terrestre1,T1lateral,null,alcanceA1); 
				actualizaVistaLateral(aereo1,terrestre2,T2lateral,null,alcanceA1); 
				actualizaVistaLateral(aereo1,aereo2,A2lateral,objAereo2,alcanceA1);
			}
			
			if (mascara=="A2" && id == objAereo2.id)
			{	
				A2lateral.x=lateral.world.centerX;
				alturaDrone(A2lateral,objAereo2);
				A2lateral.scale.x=1;
				A2lateral.scale.y=1;
				A2lateral.visible=true;
				
				actualizaVistaLateral(aereo2,terrestre1,T1lateral,null,alcanceA2);
				actualizaVistaLateral(aereo2,terrestre2,T2lateral,null,alcanceA2);
				actualizaVistaLateral(aereo2,aereo1,A1lateral,objAereo1,alcanceA2);
			}

		}
	}
		function renderL()
		{

		
		};
	
	
	function preload() 
	{
		game.load.image('bullet', 'assets/tanque/bullet.png');
		game.load.image('bg', 'assets/bg.png');
		game.load.image('base', 'assets/base.png');
		game.load.image('baserota', 'assets/baserota.png');
		game.load.image('droneterrestre', 'assets/drones/terrestre.png');
		game.load.image('droneaereo', 'assets/drones/aereoz2.png');
		game.load.image('droneterrestreZ1', 'assets/drones/terrestreCanonArriba.png');
        game.load.image('agua', 'assets/agua.png');
        game.load.image('puente', 'assets/puente.png');
        game.load.image('droneterrestreroto', 'assets/drones/terrestreroto.png');
        game.load.image('droneaereoroto', 'assets/drones/aereoroto.png');
        
        game.load.image('bgEstadoAereo', 'assets/bgEstadoBlancoAereo.png');
        game.load.image('bgEstadoTerrestre', 'assets/bgEstadoBlancoTerrestre.png');
        game.load.image('bgEstadoBase', 'assets/bgEstadoBase.png');
        
        game.load.image('blindaje', 'assets/barraEstado/blindaje.png');
        game.load.image('blindaje1', 'assets/barraEstado/blindaje1.png');
        game.load.image('blindaje2', 'assets/barraEstado/blindaje2.png');
        game.load.image('blindajeInhabilitado', 'assets/barraEstado/blindajeInhabilitado.png');
        game.load.image('bomba', 'assets/barraEstado/bomba.png');
        game.load.image('bombaRecargar', 'assets/barraEstado/bombaRecargar.png');
        game.load.image('camara', 'assets/barraEstado/camara.png');
        game.load.image('camaraInhabilitada', 'assets/barraEstado/camaraInhabilitada.png');
        game.load.image('canon1Municion', 'assets/barraEstado/canon1Municion.png');
        game.load.image('canon2Municiones', 'assets/barraEstado/canon2Municiones.png');
        game.load.image('canon3Municiones', 'assets/barraEstado/canon3Municiones.png');
        game.load.image('canon4Municiones', 'assets/barraEstado/canon4Municiones.png');
        game.load.image('canon5Municiones', 'assets/barraEstado/canon5Municiones.png');
        game.load.image('canonInhabilitado', 'assets/barraEstado/canonInhabilitado.png');
        game.load.image('canonRecargar', 'assets/barraEstado/canonRecargar.png');
        game.load.image('motor1', 'assets/barraEstado/motor1.png');
        game.load.image('motor2', 'assets/barraEstado/motor2.png');
        game.load.image('motorInhabilitado', 'assets/barraEstado/motorInhabilitado.png');
        game.load.image('zAereo0', 'assets/barraEstado/zAereo0.png');
        game.load.image('zAereo1', 'assets/barraEstado/zAereo1.png');
        game.load.image('zAereo2', 'assets/barraEstado/zAereo2.png');
        game.load.image('zTerrestre0', 'assets/barraEstado/zTerrestre0.png');
        game.load.image('zTerrestre1', 'assets/barraEstado/zTerrestre1.png');
        game.load.image('estadoDestruidoAereo', 'assets/bgEstadoDestruidoAereo.png');
        game.load.image('estadoDestruidoTerrestre', 'assets/bgEstadoDestruidoTerrestre.png');
        game.load.spritesheet('kaboom', 'assets/explosion/explosion.png', 64, 64, 23);
        
        game.load.audio('explosion', 'assets/audio/explosion.mp3');
        game.load.audio('disparo', 'assets/audio/shotgun.wav');
        
	}
	
	var terrestre1;
	var terrestre2;
	var aereo1;
	var aereo2;
	var mask1;
	var mask2;
	var mask3;
	var mask4;
	var fondo;
	var estado;
	var turret1;
	var turret2;
	var weapon1;
	var weapon2;
	var weapon3;
	var weapon4;
	var base;
	
	var p0;
	var p1;
	var p2;
	var p3;
	var p4;
	var p5;
	var p6;
	var p7;
	var p8;
	var p9;
	var p10;
	var p11;
	var p12;
	var textAereo1;
	var textAereo2;
	var textTerrestre1;
	var textTerrestre2;
	var style;
	var style2;
	
	var estadoPolvorin;
	var estadoPista;
	
	var estadoAlturaA1;
	var estadoAlturaT1;
	var estadoCamaraA1;
	var estadoCamaraT1;
	var estadoMotorA1;
	var estadoBombaA1;
	var estadoCanionA1;
	var estadoCanionT1;
	var estadoBlindajeT1;
	
	var estadoAlturaA2;
	var estadoAlturaT2;
	var estadoCamaraA2;
	var estadoCamaraT2;
	var estadoMotorA2;
	var estadoBombaA2;
	var estadoCanionA2;
	var estadoCanionT2;
	var estadoBlindajeT2;

	function create() 
	{	//game.sound.setDecodedCallback(explosion, start, this);
		explosion = game.add.audio('explosion');
		explosion.allowMultiple = true;
		
		//disparo = game.add.audio('disparo');
		//disparo.allowMultiple = true;
		
		
		game.stage.disableVisibilityChange = true;
		fondo = game.add.tileSprite(0, 0, 1152, 780, 'bg');
		
		/*estadoA1 = game.add.sprite(10, 10, 'bgEstadoAereo');
		estadoA2 = game.add.sprite(258, 10, 'bgEstadoAereo');
		estadoT2 = game.add.sprite(951, 10, 'bgEstadoTerrestre');*/
		estadoB = game.add.sprite(565, 10, 'bgEstadoBase');
		
		style = { font: "15px Calibri", fill: "#ffffff", align: "center", stroke: "#383838", strokeThickness: 1}; 
		style2 = { font: "16px Calibri", fill: "#383838", align: "center"}; 
		
		if(pasaje0.esPuente == "true"){
			p0 = game.add.sprite(pasaje0.x1, pasaje0.y1, 'puente');
		} else {
			p0 = game.add.sprite(pasaje0.x1, pasaje0.y1, 'agua');
		}

		if(pasaje1.esPuente == "true"){
			p1 = game.add.sprite(pasaje1.x1, pasaje1.y1, 'puente');
		} else {
			p1 = game.add.sprite(pasaje1.x1, pasaje1.y1, 'agua');
		}

		if(pasaje2.esPuente == "true"){
			p2 = game.add.sprite(pasaje2.x1, pasaje2.y1, 'puente');
		} else {
			p2 = game.add.sprite(pasaje2.x1, pasaje2.y1, 'agua');
		}

		if(pasaje3.esPuente == "true"){
			p3 = game.add.sprite(pasaje3.x1, pasaje3.y1, 'puente');
		} else {
			p3 = game.add.sprite(pasaje3.x1, pasaje3.y1, 'agua');
		}

		if(pasaje4.esPuente == "true"){
			p4 = game.add.sprite(pasaje4.x1, pasaje4.y1, 'puente');
		} else {
			p4 = game.add.sprite(pasaje4.x1, pasaje4.y1, 'agua');
		}

		if(pasaje5.esPuente == "true"){
			p5 = game.add.sprite(pasaje5.x1, pasaje5.y1, 'puente');
		} else {
			p5 = game.add.sprite(pasaje5.x1, pasaje5.y1, 'agua');
		}

		if(pasaje6.esPuente == "true"){
			p6 = game.add.sprite(pasaje6.x1, pasaje6.y1, 'puente');
		} else {
			p6 = game.add.sprite(pasaje6.x1, pasaje6.y1, 'agua');
		}

		if(pasaje7.esPuente == "true"){
			p7 = game.add.sprite(pasaje7.x1, pasaje7.y1, 'puente');
		} else {
			p7 = game.add.sprite(pasaje7.x1, pasaje7.y1, 'agua');
		}

		if(pasaje8.esPuente == "true"){
			p8 = game.add.sprite(pasaje8.x1, pasaje8.y1, 'puente');
		} else {
			p8 = game.add.sprite(pasaje8.x1, pasaje8.y1, 'agua');
		}

		if(pasaje9.esPuente == "true"){
			p9 = game.add.sprite(pasaje9.x1, pasaje9.y1, 'puente');
		} else {
			p9 = game.add.sprite(pasaje9.x1, pasaje9.y1, 'agua');
		}
		
		if(pasaje10.esPuente == "true"){
			p10 = game.add.sprite(pasaje10.x1, pasaje10.y1, 'puente');
		} else {
			p10 = game.add.sprite(pasaje10.x1, pasaje10.y1, 'agua');
		}
		
		if(pasaje11.esPuente == "true"){
			p11 = game.add.sprite(pasaje11.x1, pasaje11.y1, 'puente');
		} else {
			p11 = game.add.sprite(pasaje11.x1, pasaje11.y1, 'agua');
		}
	
		if(pasaje12.esPuente == "true"){
			p12 = game.add.sprite(pasaje12.x1, pasaje12.y1, 'puente');
		} else {
			p12 = game.add.sprite(pasaje12.x1, pasaje12.y1, 'agua');
		}
		
		// PARA DISPAROS
		weapon1 = game.add.weapon(1, 'bullet');
		weapon1.bulletKillType = Phaser.Weapon.KILL_DISTANCE; //ANTES KILL_WORLD_BOUNDS
		weapon1.bulletSpeed = vBala;  //PIXELES x SEGUNDOS
		weapon1.fireRate = 100;
		weapon1.bulletKillDistance=dMascaraTerrestre/2;//PIXELES
		
		weapon2 = game.add.weapon(1, 'bullet');
		weapon2.bulletKillType = Phaser.Weapon.KILL_DISTANCE;
		weapon2.bulletSpeed = vBala;
		weapon2.fireRate = 100;
		weapon2.bulletKillDistance=dMascaraTerrestre/2;

		weapon3 = game.add.weapon(1, 'bullet');
		weapon3.bulletKillType = Phaser.Weapon.KILL_DISTANCE;
		weapon3.bulletSpeed = vBala;
		weapon3.fireRate = 100;
		weapon3.bulletKillDistance=dMascaraAereo/2;

		weapon4 = game.add.weapon(1, 'bullet');
		weapon4.bulletKillType = Phaser.Weapon.KILL_DISTANCE;
		weapon4.bulletSpeed = vBala;
		weapon4.fireRate = 100;
		weapon4.bulletKillDistance=dMascaraAereo/2;
		
		////FUEGO
	    explosionsT1 = game.add.group();

	    for (var i = 0; i < 10; i++)
	    {
	        var explosionAnimation = explosionsT1.create(0, 0, 'kaboom', [0], false);
	        explosionAnimation.anchor.setTo(0.5, 0.5);
	        explosionAnimation.animations.add('kaboom');
	    }
	    
	    
	    explosionsT2 = game.add.group();

	    for (var i = 0; i < 10; i++)
	    {
	        var explosionAnimation = explosionsT2.create(0, 0, 'kaboom', [0], false);
	        explosionAnimation.anchor.setTo(0.5, 0.5);
	        explosionAnimation.animations.add('kaboom');
	    }
	    
	    explosionsA1 = game.add.group();

	    for (var i = 0; i < 10; i++)
	    {
	        var explosionAnimation = explosionsA1.create(0, 0, 'kaboom', [0], false);
	        explosionAnimation.anchor.setTo(0.5, 0.5);
	        explosionAnimation.animations.add('kaboom');
	    }
	    
	    
	    explosionsA2 = game.add.group();

	    for (var i = 0; i < 10; i++)
	    {
	        var explosionAnimation = explosionsA2.create(0, 0, 'kaboom', [0], false);
	        explosionAnimation.anchor.setTo(0.5, 0.5);
	        explosionAnimation.animations.add('kaboom');
	    }
	    


		//////TERRESTRES/////
		
		base = game.add.tileSprite(objBase.x1, objBase.y1, 39, 58, 'base');
		
		if(objTerrestre1.destruido == "true")
		{
			terrestre1 = game.add.sprite(objTerrestre1.x, objTerrestre1.y, 'droneterrestreroto');
			estadoT1 = game.add.sprite(750, 10, 'estadoDestruidoTerrestre');
		}
		else
		{
			terrestre1 = game.add.sprite(objTerrestre1.x, objTerrestre1.y, 'droneterrestre');
			estadoT1 = game.add.sprite(750, 10, 'bgEstadoTerrestre');
		}
		
	    terrestre1.anchor.setTo(0.5, 0.5);

	    //terrestre1.animations.add('move', ['tank1', 'tank2', 'tank3', 'tank4', 'tank5', 'tank6'], 20, true);
	    
	    /*turret1 = game.add.sprite(0, 0, 'tankI', 'turret');
	    turret1.anchor.setTo(0.3, 0.5);
	    terrestre1.addChild(turret1);*/
	    
	    //PARA DISPAROS
	    game.physics.arcade.enable(terrestre1);
	    terrestre1.body.drag.set(70);
	    terrestre1.body.maxVelocity.set(200);
	    weapon1.trackSprite(terrestre1, 0, 0, true);
	    

	    if(objTerrestre2.destruido == "true")
		{
			terrestre2 = game.add.sprite(objTerrestre2.x, objTerrestre2.y, 'droneterrestreroto');
			estadoT2 = game.add.sprite(951, 10, 'estadoDestruidoTerrestre');
		}
		else
		{
			terrestre2 = game.add.sprite(objTerrestre2.x, objTerrestre2.y, 'droneterrestre');
			estadoT2 = game.add.sprite(951, 10, 'bgEstadoTerrestre');
		}
	    terrestre2.anchor.setTo(0.5, 0.5);
	    /*terrestre2.animations.add('move', ['tank1', 'tank2', 'tank3', 'tank4', 'tank5', 'tank6'], 20, true);*/
	    
	    /*turret2 = game.add.sprite(0, 0, 'tankI', 'turret');
	    turret2.anchor.setTo(0.3, 0.5);
	    terrestre2.addChild(turret2);*/
	    
	    //PARA DISPAROS
	    game.physics.arcade.enable(terrestre2);
	    terrestre2.body.drag.set(70);
	    terrestre2.body.maxVelocity.set(200);
	    weapon2.trackSprite(terrestre2, 0, 0, true);
	    
	    mask1 = game.add.graphics(0, 0);
	    mask1.drawCircle(0,0,dMascaraTerrestre);
	  	//////////////////////////////////////////terrestre1.addChild(mask1);	    
	    
	    mask2 = game.add.graphics(0, 0);
	    mask2.drawCircle(0,0,dMascaraTerrestre);
	  	//////////////////////////////////////////terrestre2.addChild(mask2); 
	  	
	  	
	  	
	  	///////AEREOS////////
	    if(objAereo1.destruido == "true")
		{
	    	aereo1 = game.add.sprite(objAereo1.x, objAereo1.y, 'droneaereoroto');
			estadoA1 = game.add.sprite(10, 10, 'estadoDestruidoAereo');
		}
		else
		{
			aereo1 = game.add.sprite(objAereo1.x, objAereo1.y, 'droneaereo');
			estadoA1 = game.add.sprite(10, 10, 'bgEstadoAereo');
		}
	    
	    aereo1.anchor.setTo(0.5, 0.5);
	    
	   /* aereo1.animations.add('move', ['tank1', 'tank2', 'tank3', 'tank4', 'tank5', 'tank6'], 20, true);
	    
	    turret1 = game.add.sprite(0, 0, 'tankII', 'turret');
	    turret1.anchor.setTo(0.3, 0.5);
	    aereo1.addChild(turret1);*/
	    
	    //PARA DISPAROS
	    game.physics.arcade.enable(aereo1);
	    aereo1.body.drag.set(70);
	    aereo1.body.maxVelocity.set(200);
	    weapon3.trackSprite(aereo1, 0, 0, true);
	    
	    
	    if(objAereo2.destruido == "true")
		{
	    	aereo2 = game.add.sprite(objAereo2.x, objAereo2.y, 'droneaereoroto');
			estadoA2 = game.add.sprite(258, 10, 'estadoDestruidoAereo');
		}
		else
		{
			aereo2 = game.add.sprite(objAereo2.x, objAereo2.y, 'droneaereo');
			estadoA2 = game.add.sprite(258, 10, 'bgEstadoAereo');
		}
	    aereo2.anchor.setTo(0.5, 0.5);
	    /*aereo2.animations.add('move', ['tank1', 'tank2', 'tank3', 'tank4', 'tank5', 'tank6'], 20, true);
	    
	    turret2 = game.add.sprite(0, 0, 'tankII', 'turret');
	    turret2.anchor.setTo(0.3, 0.5);
	    aereo2.addChild(turret2);*/
	    
	    //DISPAROS
	    game.physics.arcade.enable(aereo2);
	    aereo2.body.drag.set(70);
	    aereo2.body.maxVelocity.set(200);
	    weapon4.trackSprite(aereo2, 0, 0, true);
	    
	    mask3 = game.add.graphics(0, 0);
	    mask3.drawCircle(0, 0, dMascaraAereo);
	  	////////////////////////////////////////////aereo1.addChild(mask3);	    
	    
	    mask4 = game.add.graphics(0, 0);
	    mask4.drawCircle(0, 0, dMascaraAereo);
	  	//////////////////////////////////////////////aereo2.addChild(mask4); 
	  	
	    // Texto arriba del drone
	    if(objAereo1.id == objAereo2.id){
	    	if(objAereo1.apodo != ""){
	    		textAereo1 = game.add.text(0, 0, objAereo1.apodo, style);
	    		textAereo2 = game.add.text(0, 0, objAereo1.apodo, style);
	    	} else {
	    		textAereo1 = game.add.text(0, 0, objAereo2.apodo, style);
	    		textAereo2 = game.add.text(0, 0, objAereo2.apodo, style);
	    	}
	    } else {
	    	if(objAereo1.apodo == "")
		  		textAereo1 = game.add.text(0, 0, "Aereo 1", style);
		  	else
		  		textAereo1 = game.add.text(0, 0, objAereo1.apodo, style);
		  	
		  	if(objAereo2.apodo == "")
		  		textAereo2 = game.add.text(0, 0, "Aereo 2", style);
		  	else
		  		textAereo2 = game.add.text(0, 0, objAereo2.apodo, style);
	    }
	    
	    if(objTerrestre1.id == objTerrestre2.id){
	    	if(objTerrestre1.apodo != ""){
	    		textTerrestre1 = game.add.text(0, 0, objTerrestre1.apodo, style);
	    		textTerrestre2 = game.add.text(0, 0, objTerrestre1.apodo, style);
	    	} else {
	    		textTerrestre1 = game.add.text(0, 0, objTerrestre2.apodo, style);
	    		textTerrestre2 = game.add.text(0, 0, objTerrestre2.apodo, style);
	    	}
	    } else {
	    	if(objTerrestre1.apodo == "")
		  		textTerrestre1 = game.add.text(0, 0, "Terrestre 1", style);
		  	else
		  		textTerrestre1 = game.add.text(0, 0, objTerrestre1.apodo, style);
		  	
		  	if(objTerrestre2.apodo == "")
		  		textTerrestre2 = game.add.text(0, 0, "Terrestre 2", style);
		  	else 
		  		textTerrestre2 = game.add.text(0, 0, objTerrestre2.apodo, style);
	    }
	  	
	  	textAereo1.anchor.set(1);
	    textAereo2.anchor.set(1);
	    textTerrestre1.anchor.set(1);
	    textTerrestre2.anchor.set(1);
	    
	        
	    estadoA1.bringToTop();
		estadoA2.bringToTop();
		estadoT1.bringToTop();
		estadoT2.bringToTop();
		estadoB.bringToTop();
		
		estadoPolvorin = game.add.text(580, 37, objBase.vidaPolvorin, style2);
	    estadoPolvorin.anchor.set(0.5);
	    estadoPista = game.add.text(666, 37, objBase.vidaPista, style2);
	    estadoPista.anchor.set(0.5);
	    
	 // Creacion barra estado terrestre 1
	    
	    if(objTerrestre1.z == "0")
	    	estadoAlturaT1 = game.add.sprite(765, 20, 'zTerrestre0');
	    else if (objTerrestre1.z == "1")
	    	estadoAlturaT1 = game.add.sprite(765, 20, 'zTerrestre1');
	    
	    if(objTerrestre1.camara == "true")
	    	estadoCamaraT1 = game.add.sprite(808, 20, 'camara');
	    else
	    	estadoCamaraT1 = game.add.sprite(808, 20, 'camaraInhabilitada');
	    
	    switch(objTerrestre1.blindaje){
	    	case "3": estadoBlindajeT1 = game.add.sprite(851, 20, 'blindaje'); break;
			case "2": estadoBlindajeT1 = game.add.sprite(851, 20, 'blindaje2'); break;
			case "1": estadoBlindajeT1 = game.add.sprite(851, 20, 'blindaje1'); break;
			case "0": estadoBlindajeT1 = game.add.sprite(851, 20, 'blindajeInhabilitado'); break;
		}
		
		if(objTerrestre1.canion == "false")
			estadoCanionT1 = game.add.sprite(894, 20, 'canonInhabilitado');
		else {
			if(objTerrestre1.municiones == "5")
				estadoCanionT1 = game.add.sprite(894, 20, 'canon5Municiones');
			if(objTerrestre1.municiones == "4")
				estadoCanionT1 = game.add.sprite(894, 20, 'canon4Municiones');
			if(objTerrestre1.municiones == "3")
				estadoCanionT1 = game.add.sprite(894, 20, 'canon3Municiones');
			if(objTerrestre1.municiones == "2")
				estadoCanionT1 = game.add.sprite(894, 20, 'canon2Municiones');
			if(objTerrestre1.municiones == "1")
				estadoCanionT1 = game.add.sprite(894, 20, 'canon1Municion');
			if(objTerrestre1.municiones == "0")
				estadoCanionT1 = game.add.sprite(894, 20, 'canonRecargar');
		}
	    //estadoAlturaT1 = game.add.sprite(765, 20, 'zTerrestre0');
	    //estadoCamaraT1 = game.add.sprite(808, 20, 'camara');
	    //estadoBlindajeT1 = game.add.sprite(851, 20, 'blindaje');
	    //estadoCanionT1 = game.add.sprite(894, 20, 'canon5Municiones');
	
		//Creacion barra estado terrestre 2	
		
		if(objTerrestre2.z == "0")
	    	estadoAlturaT2 = game.add.sprite(966, 20, 'zTerrestre0');
	    else if (objTerrestre2.z == "1")
	    	estadoAlturaT2 = game.add.sprite(966, 20, 'zTerrestre1');
	    
	    if(objTerrestre2.camara == "true")
	    	estadoCamaraT2 = game.add.sprite(1009, 20, 'camara');
	    else
	    	estadoCamaraT2 = game.add.sprite(1009, 20, 'camaraInhabilitada');
	    
	    switch(objTerrestre2.blindaje){
	    	case "3": estadoBlindajeT2 = game.add.sprite(1052, 20, 'blindaje'); break;
			case "2": estadoBlindajeT2 = game.add.sprite(1052, 20, 'blindaje2'); break;
			case "1": estadoBlindajeT2 = game.add.sprite(1052, 20, 'blindaje1'); break;
			case "0": estadoBlindajeT2 = game.add.sprite(1052, 20, 'blindajeInhabilitado'); break;
		}
		
		if(objTerrestre2.canion == "false")
			estadoCanionT2 = game.add.sprite(1095, 20, 'canonInhabilitado');
		else {
			if(objTerrestre2.municiones == "5")
				estadoCanionT2 = game.add.sprite(1095, 20, 'canon5Municiones');
			if(objTerrestre2.municiones == "4")
				estadoCanionT2 = game.add.sprite(1095, 20, 'canon4Municiones');
			if(objTerrestre2.municiones == "3")
				estadoCanionT2 = game.add.sprite(1095, 20, 'canon3Municiones');
			if(objTerrestre2.municiones == "2")
				estadoCanionT2 = game.add.sprite(1095, 20, 'canon2Municiones');
			if(objTerrestre2.municiones == "1")
				estadoCanionT2 = game.add.sprite(1095, 20, 'canon1Municion');
			if(objTerrestre2.municiones == "0")
				estadoCanionT2 = game.add.sprite(1095, 20, 'canonRecargar');
		}
		
		//estadoAlturaT2 = game.add.sprite(966, 20, 'zTerrestre0'); 
		//estadoCamaraT2 = game.add.sprite(1009, 20, 'camara');
		//estadoBlindajeT2 = game.add.sprite(1052, 20, 'blindaje');
		//estadoCanionT2 = game.add.sprite(1095, 20, 'canon5Municiones');

		//Creacion barra estado aereo 1
		if(objAereo1.z == 0){
			estadoAlturaA1 = game.add.sprite(20, 20, 'zAereo0');
		} else if (objAereo1.z == 1){
			estadoAlturaA1 = game.add.sprite(20, 20, 'zAereo1');
		} else if (objAereo1.z == 2) {
			estadoAlturaA1 = game.add.sprite(20, 20, 'zAereo2');
		}
		
		if(objAereo1.camara == "true")
			estadoCamaraA1 = game.add.sprite(63, 20, 'camara');
		else
			estadoCamaraA1 = game.add.sprite(63, 20, 'camaraInhabilitada');
		
		switch(objAereo1.motor){
			case "2": estadoMotorA1 = game.add.sprite(106, 20, 'motor2'); break;
			case "1": estadoMotorA1 = game.add.sprite(106, 20, 'motor1'); break;
			case "0": estadoMotorA1 = game.add.sprite(106, 20, 'motorInhabilitado'); break;
		}
		
		switch(objAereo1.bomba){
			case "true": estadoBombaA1 = game.add.sprite(153, 20, 'bomba'); break;
			case "false": estadoBombaA1 = game.add.sprite(153, 20, 'bombaRecargar'); break;
		}
		
		if(objAereo1.canion == "false")
			estadoCanionA1 = game.add.sprite(201, 20, 'canonInhabilitado');
		else {
			if(objAereo1.municiones == "3")
				estadoCanionA1 = game.add.sprite(201, 20, 'canon3Municiones');
			if(objAereo1.municiones == "2")
				estadoCanionA1 = game.add.sprite(201, 20, 'canon2Municiones');
			if(objAereo1.municiones == "1")
				estadoCanionA1 = game.add.sprite(201, 20, 'canon1Municion');
			if(objAereo1.municiones == "0")
				estadoCanionA1 = game.add.sprite(201, 20, 'canonRecargar');
		}
		
		//estadoCamaraA1 = game.add.sprite(63, 20, 'camara');
		//estadoMotorA1 = game.add.sprite(106, 20, 'motor2');
		//estadoBombaA1 = game.add.sprite(153, 20, 'bomba');
		//estadoCanionA1 = game.add.sprite(201, 20, 'canon3Municiones');
	    		
		//Creacion barra estado aereo 2
		if(objAereo2.z == 0){
			estadoAlturaA2 = game.add.sprite(268, 20, 'zAereo0');
		} else if (objAereo2.z == 1){
			estadoAlturaA2 = game.add.sprite(268, 20, 'zAereo1');
		} else if (objAereo2.z == 2) {
			estadoAlturaA2 = game.add.sprite(268, 20, 'zAereo2');
		}
		
		if(objAereo2.camara == "true")
			estadoCamaraA2 = game.add.sprite(311, 20, 'camara');
		else
			estadoCamaraA2 = game.add.sprite(311, 20, 'camaraInhabilitada');
		
		switch(objAereo2.motor){
			case "2": estadoMotorA2 = game.add.sprite(354, 20, 'motor2'); break;
			case "1": estadoMotorA2 = game.add.sprite(354, 20, 'motor1'); break;
			case "0": estadoMotorA2 = game.add.sprite(354, 20, 'motorInhabilitado'); break;
		}
		
		switch(objAereo2.bomba){
			case "true": estadoBombaA2 = game.add.sprite(401, 20, 'bomba'); break;
			case "false": estadoBombaA2 = game.add.sprite(401, 20, 'bombaRecargar'); break;
		}
		
		if(objAereo2.canion == "false")
			estadoCanionA2 = game.add.sprite(449, 20, 'canonInhabilitado');
		else {
			if(objAereo2.municiones == "3")
				estadoCanionA2 = game.add.sprite(449, 20, 'canon3Municiones');
			if(objAereo2.municiones == "2")
				estadoCanionA2 = game.add.sprite(449, 20, 'canon2Municiones');
			if(objAereo2.municiones == "1")
				estadoCanionA2 = game.add.sprite(449, 20, 'canon1Municion');
			if(objAereo2.municiones == "0")
				estadoCanionA2 = game.add.sprite(449, 20, 'canonRecargar');
		}
		
		//estadoCamaraA2 = game.add.sprite(311, 20, 'camara');
		//estadoMotorA2 = game.add.sprite(354, 20, 'motor2');
		//estadoBombaA2 = game.add.sprite(401, 20, 'bomba');
		//estadoCanionA2 = game.add.sprite(449, 20, 'canon3Municiones');
	}

	function update() 
	{
		//TERRESTRES
		//if (id==objTerrestre1.id)
		if (mascara=="T1" && id==objTerrestre1.id)
		{
			
			if (objTerrestre1.camara=="false")
			{
				mask1.scale.x=0.2;
				mask1.scale.y=0.2;
				alcanceT1=alcanceTerrestreRoto;
				
			}
			
			p0.mask=mask1;
			p1.mask=mask1;
			p2.mask=mask1;
			p3.mask=mask1;
			p4.mask=mask1;
			p5.mask=mask1;
			p6.mask=mask1;
			p7.mask=mask1;
			p8.mask=mask1;
			p9.mask=mask1;
			p10.mask=mask1;
			p11.mask=mask1;
			p12.mask=mask1;
			fondo.mask=mask1;
			aereo1.mask=mask1;
			aereo2.mask=mask1;
			base.mask=mask1;
			textAereo1.mask=mask1;
			textAereo2.mask=mask1;
			explosionsA1.mask=mask1;
			explosionsA2.mask=mask1;
		}
		
		//if (id == objTerrestre2.id)
		if (mascara=="T2" && id == objTerrestre2.id)
		{
			if (objTerrestre2.camara=="false")
			{
				mask2.scale.x=0.2;
				mask2.scale.y=0.2;
				alcanceT2=alcanceTerrestreRoto;
			}
			
			p0.mask=mask2;
			p1.mask=mask2;
			p2.mask=mask2;
			p3.mask=mask2;
			p4.mask=mask2;
			p5.mask=mask2;
			p6.mask=mask2;
			p7.mask=mask2;
			p8.mask=mask2;
			p9.mask=mask2;
			p10.mask=mask2;
			p11.mask=mask2;
			p12.mask=mask2;
			fondo.mask=mask2;
			aereo1.mask=mask2;
			aereo2.mask=mask2;
			base.mask=mask2;
			textAereo1.mask=mask2;
			textAereo2.mask=mask2;
			explosionsA1.mask=mask2;
			explosionsA2.mask=mask2;
		}
		//AEREOS
		//if (id==objAereo1.id)
		if (mascara=="A1" && id==objAereo1.id)
		{			
			if (objAereo1.camara=="false")
			{
				mask3.scale.x=0.2;
				mask3.scale.y=0.2;
				alcanceA1=alcanceAereoRoto;
			}
			
			p0.mask=mask3;
			p1.mask=mask3;
			p2.mask=mask3;
			p3.mask=mask3;
			p4.mask=mask3;
			p5.mask=mask3;
			p6.mask=mask3;
			p7.mask=mask3;
			p8.mask=mask3;
			p9.mask=mask3;
			p10.mask=mask3;
			p11.mask=mask3;
			p12.mask=mask3;
			fondo.mask=mask3;
			terrestre1.mask=mask3;
			terrestre2.mask=mask3;
			textTerrestre1.mask=mask3;
			textTerrestre2.mask=mask3;
			explosionsT1.mask=mask3;
			explosionsT2.mask=mask3;

		}
		
		//if (id == objAereo2.id)
		if (mascara=="A2" && id == objAereo2.id)
		{
			if (objAereo2.camara=="false")
			{
				mask4.scale.x=0.2;
				mask4.scale.y=0.2;
				alcanceA2=alcanceAereoRoto;
			}
			
			p0.mask=mask4;
			p1.mask=mask4;
			p2.mask=mask4;
			p3.mask=mask4;
			p4.mask=mask4;
			p5.mask=mask4;
			p6.mask=mask4;
			p7.mask=mask4;
			p8.mask=mask4;
			p9.mask=mask4;
			p10.mask=mask4;
			p11.mask=mask4;
			p12.mask=mask4;
			fondo.mask=mask4;
			terrestre1.mask=mask4;
			terrestre2.mask=mask4;
			textTerrestre1.mask=mask4;
			textTerrestre2.mask=mask4;
			explosionsT1.mask=mask4;
			explosionsT2.mask=mask4;

		}
		//TERRESTRES
		terrestre1.x=parseFloat(objTerrestre1.x);
		terrestre1.y=parseFloat(objTerrestre1.y);
		terrestre1.angle=parseFloat(objTerrestre1.angulo);
		
		terrestre2.x=parseFloat(objTerrestre2.x);
		terrestre2.y=parseFloat(objTerrestre2.y);
		terrestre2.angle=parseFloat(objTerrestre2.angulo);
		
		//PARA DISPAROS
		if ((objTerrestre1.disparar=="true" && objTerrestre1.municiones >= 0) )
		{
			weapon1.bulletKillDistance=distancia_t1;
			weapon1.fire();
			//disparo.play();
		}
		
		if ((objTerrestre2.disparar=="true" && objTerrestre2.municiones >= 0) )
		{
			weapon2.bulletKillDistance=distancia_t2;
			weapon2.fire();
			//disparo.play();
		}
		
		if ((objAereo1.disparar=="true" && objAereo1.municiones >= 0) )
		{
			weapon3.bulletKillDistance=distancia_a1;
			weapon3.fire();
			//disparo.play();
		}
		
		if ((objAereo2.disparar=="true" && objAereo2.municiones >= 0) )
		{
			weapon4.bulletKillDistance=distancia_a2;
			weapon4.fire();
			//disparo.play();
		}
		
		//AEREOS
		aereo1.x=parseFloat(objAereo1.x);
		aereo1.y=parseFloat(objAereo1.y);
		aereo1.angle=parseFloat(objAereo1.angulo);
		
		aereo2.x=parseFloat(objAereo2.x);
		aereo2.y=parseFloat(objAereo2.y);
		aereo2.angle=parseFloat(objAereo2.angulo);
		
		
		textAereo1.x = Math.floor(aereo1.x + aereo1.width / 2);
		textAereo1.y = Math.floor(aereo1.y + aereo1.height / 2) - 46;
		textAereo2.x = Math.floor(aereo2.x + aereo2.width / 2);
		textAereo2.y = Math.floor(aereo2.y + aereo2.height / 2) - 46;
		textTerrestre1.x = Math.floor(terrestre1.x + terrestre1.width/ 2);
		textTerrestre1.y = Math.floor(terrestre1.y + terrestre1.height / 2) - 46;
		textTerrestre2.x = Math.floor(terrestre2.x + terrestre2.width / 2);
		textTerrestre2.y = Math.floor(terrestre2.y + terrestre2.height / 2) - 46;
		
		
		//ACTUALIZA POSICION MASCARAS
		mask1.x=objTerrestre1.x;
		mask1.y=objTerrestre1.y;
		mask2.x=objTerrestre2.x;
		mask2.y=objTerrestre2.y;
		mask3.x=objAereo1.x;
		mask3.y=objAereo1.y;
		mask4.x=objAereo2.x;
		mask4.y=objAereo2.y;
		
		// Update barra estado
		// T1
    	switch(objTerrestre1.z){
	    	case "0":{estadoAlturaT1.loadTexture('zTerrestre0', 0); terrestre1.loadTexture('droneterrestre', 0); } break;
	    	case "1":{ estadoAlturaT1.loadTexture('zTerrestre1', 0); terrestre1.loadTexture('droneterrestreZ1', 0); } break;
    	}
    	
    	switch(objTerrestre1.camara){
	    	case "true": estadoCamaraT1.loadTexture('camara', 0); break;
	    	case "false": estadoCamaraT1.loadTexture('camaraInhabilitada', 0); break;
    	}
    	
    	switch(objTerrestre1.blindaje){
	    	case "3": estadoBlindajeT1.loadTexture('blindaje', 0); break;
			case "2": estadoBlindajeT1.loadTexture('blindaje2', 0); break;
			case "1": estadoBlindajeT1.loadTexture('blindaje1', 0); break;
			case "0": estadoBlindajeT1.loadTexture('blindajeInhabilitado', 0); break;
    	}
    	
    	if(objTerrestre1.canion == "false")
			estadoCanionT1.loadTexture('canonInhabilitado', 0);
		else {
			if(objTerrestre1.municiones == "5")
				estadoCanionT1.loadTexture('canon5Municiones', 0);
			if(objTerrestre1.municiones == "4")
				estadoCanionT1.loadTexture('canon4Municiones', 0);
			if(objTerrestre1.municiones == "3")
				estadoCanionT1.loadTexture('canon3Municiones', 0);
			if(objTerrestre1.municiones == "2")
				estadoCanionT1.loadTexture('canon2Municiones', 0);
			if(objTerrestre1.municiones == "1")
				estadoCanionT1.loadTexture('canon1Municion', 0);
			if(objTerrestre1.municiones == "0")
				estadoCanionT1.loadTexture('canonRecargar', 0);
		}

	    // T2
		switch(objTerrestre2.z){
		case "0":{estadoAlturaT2.loadTexture('zTerrestre0', 0); terrestre2.loadTexture('droneterrestre', 0); } break;
    	case "1":{ estadoAlturaT2.loadTexture('zTerrestre1', 0); terrestre2.loadTexture('droneterrestreZ1', 0); } break;
    	}
		
		switch(objTerrestre2.camara){
	    	case "true": estadoCamaraT2.loadTexture('camara', 0); break;
	    	case "false": estadoCamaraT2.loadTexture('camaraInhabilitada', 0); break;
    	}
		
		switch(objTerrestre2.blindaje){
    		case "3": estadoBlindajeT2.loadTexture('blindaje', 0); break;
    		case "2": estadoBlindajeT2.loadTexture('blindaje2', 0); break;
    		case "1": estadoBlindajeT2.loadTexture('blindaje1', 0); break;
    		case "0": estadoBlindajeT2.loadTexture('blindajeInhabilitado', 0); break;
    	}
		
		if(objTerrestre2.canion == "false")
			estadoCanionT2.loadTexture('canonInhabilitado', 0);
		else {
			if(objTerrestre2.municiones == "5")
				estadoCanionT2.loadTexture('canon5Municiones', 0);
			if(objTerrestre2.municiones == "4")
				estadoCanionT2.loadTexture('canon4Municiones', 0);
			if(objTerrestre2.municiones == "3")
				estadoCanionT2.loadTexture('canon3Municiones', 0);
			if(objTerrestre2.municiones == "2")
				estadoCanionT2.loadTexture('canon2Municiones', 0);
			if(objTerrestre2.municiones == "1")
				estadoCanionT2.loadTexture('canon1Municion', 0);
			if(objTerrestre2.municiones == "0")
				estadoCanionT2.loadTexture('canonRecargar', 0);
		}
		
		// A1
    	switch(objAereo1.camara){
	    	case "true": estadoCamaraA1.loadTexture('camara', 0); break;
	    	case "false": estadoCamaraA1.loadTexture('camaraInhabilitada', 0); break;
    	}
    	
    	switch(objAereo1.motor){
    		case "2": estadoMotorA1.loadTexture('motor2', 0); break;
    		case "1": estadoMotorA1.loadTexture('motor1', 0); break;
    		case "0": estadoMotorA1.loadTexture('motorInhabilitado', 0); break;
    	}
    	
    	switch(objAereo1.bomba){
    		case "true": estadoBombaA1.loadTexture('bomba', 0); break;
    		case "false": estadoBombaA1.loadTexture('bombaRecargar', 0); break;
    	}
    	
    	if(objAereo1.canion == "false")
			estadoCanionA1.loadTexture('canonInhabilitado', 0);
		else {
			if(objAereo1.municiones == "3")
				estadoCanionA1.loadTexture('canon3Municiones', 0);
			if(objAereo1.municiones == "2")
				estadoCanionA1.loadTexture('canon2Municiones', 0);
			if(objAereo1.municiones == "1")
				estadoCanionA1.loadTexture('canon1Municion', 0);
			if(objAereo1.municiones == "0")
				estadoCanionA1.loadTexture('canonRecargar', 0);
		}
	    		
    	// A2
    	switch(objAereo2.camara){
	    	case "true": estadoCamaraA2.loadTexture('camara', 0); break;
	    	case "false": estadoCamaraA2.loadTexture('camaraInhabilitada', 0); break;
    	}
    	
    	switch(objAereo2.motor){
    		case "2": estadoMotorA2.loadTexture('motor2', 0); break;
    		case "1": estadoMotorA2.loadTexture('motor1', 0); break;
    		case "0": estadoMotorA2.loadTexture('motorInhabilitado', 0); break;
    	}
    	
    	switch(objAereo2.bomba){
    		case "true": estadoBombaA2.loadTexture('bomba', 0); break;
    		case "false": estadoBombaA2.loadTexture('bombaRecargar', 0); break;
    	}
		
		if(objAereo2.canion == "false")
			estadoCanionA2.loadTexture('canonInhabilitado', 0);
		else {
			if(objAereo2.municiones == "3")
				estadoCanionA2.loadTexture('canon3Municiones', 0);
			if(objAereo2.municiones == "2")
				estadoCanionA2.loadTexture('canon2Municiones', 0);
			if(objAereo2.municiones == "1")
				estadoCanionA2.loadTexture('canon1Municion', 0);
			if(objAereo2.municiones == "0")
				estadoCanionA2.loadTexture('canonRecargar', 0);
		}
		
		/////////
		
		/*{
		var explosionAnimation = explosions.getFirstExists(false);
        explosionAnimation.reset(60, 150);
        explosionAnimation.play('kaboom', 30, false, true);
		}*/
		
		if(objTerrestre1.destruido == "true"){
			/////PARA DISPARO SINCRONIZADO
			var distM;
			
			if (objTerrestre1.disparador=="A1")
				distM=distancia_a1;
			
			if (objTerrestre1.disparador=="A2")
				distM=distancia_a2;
			
			var millisecondsToWait = distM*1000/vBala; //600 PIXELES POR SEGUNDO
			
			setTimeout(function() {centinelaT1=true;}, millisecondsToWait);
			
			if (centinelaT1)
				{
					terrestre1.loadTexture('droneterrestreroto', 0);
					estadoT1.loadTexture('estadoDestruidoTerrestre', 0);
					estadoT1.bringToTop();
					
						
				        var explosionAnimation = explosionsT1.getFirstExists(false);
				        
				        if (explosionAnimation)
				        {
				        	
					        explosionAnimation.reset(terrestre1.x, terrestre1.y);
					        explosionAnimation.play('kaboom', 30, false, false);
					        
				        }
				        if (!yasonoT1)
				        	{
					        explosion.play();
					        yasonoT1=true;
				        	}

					
				}
			
			mask1.scale.x=0;
			mask1.scale.y=0;
				
		}
		if(objTerrestre2.destruido == "true"){
			/////PARA DISPARO SINCRONIZADO
			var distM;
			
			
			if (objTerrestre2.disparador=="A1")
				distM=distancia_a1;
			
			if (objTerrestre2.disparador=="A2")
				distM=distancia_a2;
			
			
			var millisecondsToWait = distM*1000/vBala; //600 PIXELES POR SEGUNDO
			setTimeout(function() {centinelaT2=true;}, millisecondsToWait);
			
			if (centinelaT2)
				{
					terrestre2.loadTexture('droneterrestreroto', 0);
					estadoT2.loadTexture('estadoDestruidoTerrestre', 0);
					estadoT2.bringToTop();
					
			        var explosionAnimation = explosionsT2.getFirstExists(false);
			        
			        if (explosionAnimation)
			        {
			        	
				        explosionAnimation.reset(terrestre2.x, terrestre2.y);
				        explosionAnimation.play('kaboom', 30, false, false);
				        
			        }
			        
			        if (!yasonoT2)
		        	{
			        explosion.play();
			        yasonoT2=true;
		        	}

					
				}
			
			mask2.scale.x=0;
			mask2.scale.y=0;
		}
		if(objAereo1.destruido == "true"){
			/////PARA DISPARO SINCRONIZADO
			var distM;
			
			if (objAereo1.disparador=="T1")
				distM=distancia_t1;
			
			if (objAereo1.disparador=="T2")
				distM=distancia_t2;
			
			
			var millisecondsToWait = distM*1000/vBala; //600 PIXELES POR SEGUNDO
			setTimeout(function() {centinelaA1=true;}, millisecondsToWait);
			
			if (centinelaA1)
				{
					aereo1.loadTexture('droneaereoroto', 0);
					estadoA1.loadTexture('estadoDestruidoAereo', 0);
					estadoA1.bringToTop();
					
					
			        var explosionAnimation = explosionsA1.getFirstExists(false);
			        
			        if (explosionAnimation)
			        {
			        	
				        explosionAnimation.reset(aereo1.x, aereo1.y);
				        explosionAnimation.play('kaboom', 30, false, false);
				        
			        }
			        
			        if (!yasonoA1)
		        	{
			        explosion.play();
			        yasonoA1=true;
		        	}

					
				}
			
			mask3.scale.x=0;
			mask3.scale.y=0;

		}
		if(objAereo2.destruido == "true"){
			/////PARA DISPARO SINCRONIZADO
			var distM;
			
			
			if (objAereo2.disparador=="T1")
				distM=distancia_t1;
			
			if (objAereo2.disparador=="T2")
				distM=distancia_t2;
			
			
			var millisecondsToWait = distM*1000/vBala; //600 PIXELES POR SEGUNDO
			setTimeout(function() {centinelaA2=true;}, millisecondsToWait);
			
			if (centinelaA2)
				{
					aereo2.loadTexture('droneaereoroto', 0);
					estadoA2.loadTexture('estadoDestruidoAereo', 0);
					estadoA2.bringToTop();
					
					
			        var explosionAnimation = explosionsA2.getFirstExists(false);
			        
			        if (explosionAnimation)
			        {
			        	
				        explosionAnimation.reset(aereo2.x, aereo2.y);
				        explosionAnimation.play('kaboom', 30, false, false);
				        
			        }
					
			        if (!yasonoA2)
		        	{
			        explosion.play();
			        yasonoA2=true;
		        	}
				}
			
			mask4.scale.x=0;
			mask4.scale.y=0;
		}
		
		if (objAereo1.z== 0)
		{
			aereo1.scale.x=0.6;
			aereo1.scale.y=0.6;
			estadoAlturaA1.loadTexture('zAereo0', 0);
		}
		if (objAereo1.z== 1)
		{
			aereo1.scale.x=0.8;
			aereo1.scale.y=0.8;
			estadoAlturaA1.loadTexture('zAereo1', 0);
		}
		if (objAereo1.z== 2)
		{
			aereo1.scale.x=1;
			aereo1.scale.y=1;
			estadoAlturaA1.loadTexture('zAereo2', 0);
		}
		
		if (objAereo2.z== 0)
		{
			aereo2.scale.x=0.6;
			aereo2.scale.y=0.6;
			estadoAlturaA2.loadTexture('zAereo0', 0);
		}
		if (objAereo2.z== 1)
		{
			aereo2.scale.x=0.8;
			aereo2.scale.y=0.8;
			estadoAlturaA2.loadTexture('zAereo1', 0);
		}
		if (objAereo2.z== 2)
		{
			aereo2.scale.x=1;
			aereo2.scale.y=1;
			estadoAlturaA2.loadTexture('zAereo2', 0);
		}
		
		if(objBase.destruido == "true"){
			base.loadTexture('baserota', 0);
		}
		
		estadoPista.setText(objBase.vidaPista);
		estadoPolvorin.setText(objBase.vidaPolvorin);
		
		

	}

	function render() 
	{
	}
	
	
	function doKeyDown(evt)
	{
		if(objPausa.estaPausada == "false"){
			wsSendMessage(evt.keyCode);			
		}
	}

	window.addEventListener('keydown',doKeyDown,true);
}