function jugar()
{
	wsSendMessage("Jugar");
}

function listarPartidas()
{
	wsSendMessage("ListarPartidas");
}

function cargarPartida(){	
	wsSendMessage("CargarPartida_" + $("#listado-partidas option:selected" ).text());
}

function iniciarPartida(){
	wsSendMessage("IniciarPartida");
}

function irAlLobby(){	
	wsSendMessage("IrAlLobby");
}

function reanudar(){
	wsSendMessage("Reanudar");
}

function menuGuardar(){
	wsSendMessage("IngresarNombrePartida");
}

function guardarPartida(){
	var nombrePartida = document.getElementsByName("nombrePartida")[0].value;
	wsSendMessage("Guardar_"+ nombrePartida);
}

function volverAInicio(){
	$("#seccion-recuperar").hide();
	$("#seccion-menu").show();
}

function volverAlMenuPausaDesdeGuardar(){
	$("#seccion-pausa").show();
	$("#seccion-guardar").hide();
	
	wsSendMessage("VolverAMenuPausa");
}

function volverAlMenuPausaDesdeValidar(){
	$("#seccion-pausa").show();
	$("#seccion-validar-guardar").hide();
}

function abandonar(){
	wsSendMessage("Abandonar");
	$("#seccion-pausa").hide();
	$("#seccion-menu").show();
	$("canvas").addClass( "esconderJuego");
}

function mostrarEstadisticas(){
	
	$("#seccion-finalizada").show();
}

function partidaFinalizada(){
	$("canvas").addClass( "esconderJuego");
	$("#seccion-finalizada").hide();
	location.reload();
	$("#seccion-menu").show();
}

function errorRecuperarVolver(){
	$("#seccion-error-recuperar").hide();
	$('#seccion-recuperar').hide();
	$('#seccion-apodo').hide();
	$("#seccion-menu").show();
}
//Funcion que cuando entre un jugador vaya a la lista del medio
function agregarALaEspera()
{
	var apodo = document.getElementsByName("apodo")[0].value;	
	var cadena = "IrAlLobby_"+apodo;
	
	if(apodo == ""){
		alert("Apodo no puede estar vacío");
	} else {
		wsSendMessage(cadena);
	}
}

function mostrarAyuda(){
	$("#seccion-controles").show();
}

function cerrarAyuda(){
	$("#seccion-controles").hide();
}

function mostrarAyudaPausa(){
	$("#seccion-controles-pausa").show();
}

function cerrarAyudaPausa(){
	$("#seccion-controles-pausa").hide();
}

$(document).ready(function() 
{
	$("#seccion-lobby").hide();
	$("#seccion-apodo").hide();
	$("#seccion-recuperar").hide();
	$("#seccion-pausa").hide();
	$("#seccion-guardar").hide();
	$("#seccion-validar-guardar").hide();
	$("#seccion-finalizada").hide();
	$("#seccion-error-recuperar").hide();
	$("#seccion-controles").hide();
	$("#seccion-controles-pausa").hide();
	
	/*$("#btn-jugar").click(function()
	{
		$("#seccion-menu").hide();
		$("#seccion-apodo").show();	
    });*/
	
	$('body').on('click', '.lista-jugadores li', function()
	{
		$(this).toggleClass('selected');
	});
	
	$('#btn-aereo').click(function()
	{
		var apodoa = document.getElementsByName("apodo")[0].value;
	    var bandoa = "aereo";
	    var cadena = "ElegirBando_"+apodoa+"_aereo";
		wsSendMessage(cadena);
	});
	
	$('#btn-terrestre').click(function()
	{
	    var apodot = document.getElementsByName("apodo")[0].value;
	    var bandot = "terrestre";
	    var cadena = "ElegirBando_"+apodot+"_terrestre";
		wsSendMessage(cadena);
	});
	
	$('#btn-espera').click(function()
	{
	    var apodoe = document.getElementsByName("apodo")[0].value;
	    var bandoe = "noAsignado";
	    var cadena = "ElegirBando_"+apodoe+"_noAsignado";
		wsSendMessage(cadena);
	});
});