package Logica;
import java.io.IOException;

import javax.websocket.Session;

public class detenerDisparo extends Thread
{
	private Session vSession;
	private String vMensaje;
	private Fachada vFachada;
	
	public detenerDisparo (Session pSession, String pMensaje, Fachada pFachada)
	{
		this.vSession = pSession;
		this.vMensaje = pMensaje;
		this.vFachada=pFachada;
	}
	
	
	@Override
	public void run() 
	{
		try {
			Thread.sleep(100);
			
			vMensaje=vMensaje.replaceAll("\"disparar\":\"true\"", "\"disparar\":\"false\"");
			for (Session vSession : vSession.getOpenSessions()) 
		    {
		        if (vSession.isOpen()) 
		        {		        	
		        	if ((vFachada.esJugadorPorId(vSession.getId())) || (vFachada.esEspectadorPorId(vSession.getId())))
		        	{
		        		vSession.getBasicRemote().sendText(vMensaje);
		        	}		        	
		        }
		    }			
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
