package Logica;

import java.util.ArrayList;
import Persistencia.DAOBase;
import Persistencia.DAODrone;
import Persistencia.DAOpartida;
import ValueObjects.VOPartida;
import Persistencia.DAOPasaje;

public class Partida {	
	private Mapa mapa;	
	private Drones drones;
	private Espectadores vEspectadores;
	private boolean partidaIniciada;
	private boolean partidaPausada;
	private boolean partidaRecuperada;
	private boolean partidaCargada;
	private boolean respaldoEnCurso;
	private DAOBase vDAOBase;
	private DAODrone vDAODrone;
	private DAOpartida vDAOpartida;
	private DAOPasaje vDAOPasaje;
	
	public Partida(Mapa mapa) 
	{		
		this.mapa = mapa;		
		this.drones = new Drones();
		this.vEspectadores= new Espectadores();
		this.partidaIniciada=false;
		this.partidaPausada=false;
		this.partidaRecuperada=false;
		this.partidaCargada=false;
		this.respaldoEnCurso=false;
		this.vDAOBase=new DAOBase();
		this.vDAODrone=new DAODrone();
		this.vDAOpartida=new DAOpartida();
		this.vDAOPasaje=new DAOPasaje();
	}
	
	public Partida() 
	{	
		this.mapa = new Mapa();		
		this.drones = new Drones();
		this.vEspectadores= new Espectadores();
		this.partidaIniciada=false;
		this.partidaPausada=false;
		this.partidaRecuperada=false;
		this.partidaCargada=false;
		this.respaldoEnCurso=false;
		this.vDAOBase=new DAOBase();
		this.vDAODrone=new DAODrone();
		this.vDAOpartida=new DAOpartida();
		this.vDAOPasaje=new DAOPasaje();
	}
	

	public boolean isPartidaIniciada() {
		return partidaIniciada;
	}

	public void setPartidaIniciada(boolean partidaIniciada) {
		this.partidaIniciada = partidaIniciada;
	}

	public boolean isPartidaPausada() {
		return partidaPausada;
	}

	public void setPartidaPausada(boolean partidaPausada) {
		this.partidaPausada = partidaPausada;
	}

	public boolean isPartidaRecuperada() {
		return partidaRecuperada;
	}

	public void setPartidaRecuperada(boolean partidaRecuperada) {
		this.partidaRecuperada = partidaRecuperada;
	}
	
	public boolean isPartidaCargada() {
		return partidaCargada;
	}

	public void setPartidaCargada(boolean partidaCargada) {
		this.partidaCargada = partidaCargada;
	}

	public boolean isRespaldoEnCurso() {
		return respaldoEnCurso;
	}

	public void setRespaldoEnCurso(boolean respaldoEnCurso) {
		this.respaldoEnCurso = respaldoEnCurso;
	}

	public Mapa getMapa() 
	{
		return mapa;
	}

	public void setMapa(Mapa mapa) 
	{
		this.mapa = mapa;
	}

	public Drones getDrones() 
	{
		return drones;
	}
	
	public Drone obtenerDronPorRol (int pRol)
	{
		return drones.obtenerDronPorRol (pRol);
	}
	
	public String getRolDrone (int pRol)
	{
		return drones.getRolDrone(pRol);
	}
	
	public int getRolPorId(String iD)
	{
		int vRol =0;
		while((vRol<4)&& !(drones.getIdPorRol(vRol).equals(iD)))
		{
			vRol++;	
		}
		if (drones.getActivoI(vRol))
		{
			
		}
		else
		{
			
		vRol++;
			while((vRol<4)&& !(drones.getIdPorRol(vRol).equals(iD)))
			{
				vRol++;	
			}
			
		}
		return vRol;
	}
	
	public Drone getDronPorId (String pId)
	{
		return drones.obtenerDronPorRol(getRolPorId(pId));
				
	}
	
	public String rotarIzquierda (String iD)
	{
		int vRol = getRolPorId(iD);
		Drone vdrone=null;
		if (vRol==0||vRol==1)
		{
			vdrone = (DroneTerrestre)obtenerDronPorRol(vRol);
		}
		else
		{
			vdrone = (DroneAereo)obtenerDronPorRol(vRol);
		}		
		
		vdrone.rotarIzquierda();
		
		String stringRol=getRolDrone(vRol);
		float x=vdrone.getCoordX();
		float y=vdrone.getCoordY();
		float angulo=vdrone.getAngulo();	      
	      
		String json="{\"id\": \""+iD+"\",\"x\":\""+x+"\",\"rol\":\""+stringRol+"\""+",\"y\":\""+y+"\""+",\"angulo\":\""+angulo+"\""+"}";
		json= "mover_"+json;	
		return json;
	}
	
	public String rotarDerecha (String iD)
	{
		int vRol = getRolPorId(iD);
		
		Drone vdrone=null;
		if (vRol==0||vRol==1)
		{
			vdrone = (DroneTerrestre)obtenerDronPorRol(vRol);
		}
		else
		{
			vdrone = (DroneAereo)obtenerDronPorRol(vRol);
		}
		
		vdrone.rotarDerecha();
		
		String stringRol=getRolDrone(vRol);
		float x=vdrone.getCoordX();
		float y=vdrone.getCoordY();
		float angulo=vdrone.getAngulo();
		
		String json="{\"id\": \""+iD+"\",\"x\":\""+x+"\",\"rol\":\""+stringRol+"\""+",\"y\":\""+y+"\""+",\"angulo\":\""+angulo+"\""+"}";
		json= "mover_"+json;	
		return json;
	}
	
	public String avanzar (String iD)
	{
		int vRol = getRolPorId(iD);
		Drone vdrone=null;
		if (vRol==0||vRol==1)
		{
			vdrone = (DroneTerrestre)obtenerDronPorRol(vRol);
		}
		else
		{
			vdrone = (DroneAereo)obtenerDronPorRol(vRol);
		}	
		
		vdrone.avanzar(mapa);
		
		String stringRol=getRolDrone(vRol);
		float x=vdrone.getCoordX();
		float y=vdrone.getCoordY();
		float angulo=vdrone.getAngulo();
		
		String json="{\"id\": \""+iD+"\",\"x\":\""+x+"\",\"rol\":\""+stringRol+"\""+",\"y\":\""+y+"\""+",\"angulo\":\""+angulo+"\""+"}";
		
		json= "mover_"+json;
			
		return json;
	}
	
	public String retroceder(String iD)
	{
		int vRol = getRolPorId(iD);
		Drone vdrone=null;
		if (vRol==0||vRol==1)
		{
			vdrone = (DroneTerrestre)obtenerDronPorRol(vRol);
		}
		else
		{
			vdrone = (DroneAereo)obtenerDronPorRol(vRol);
		}	
		
		vdrone.retroceder(mapa);
		
		String stringRol=getRolDrone(vRol);
		float x=vdrone.getCoordX();
		float y=vdrone.getCoordY();
		float angulo=vdrone.getAngulo();
		
		String json="{\"id\": \""+iD+"\",\"x\":\""+x+"\",\"rol\":\""+stringRol+"\""+",\"y\":\""+y+"\""+",\"angulo\":\""+angulo+"\""+"}";
			
		json= "mover_"+json;
		return json;
	}
	
	public boolean partidaFinalizada()
	{
		if ((!(drones.estaVivoDroneI(0)) && !(drones.estaVivoDroneI(1)))||(!(drones.estaVivoDroneI(2)) && !(drones.estaVivoDroneI(3)))||(mapa.baseDestruida()))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public String bandoGanador()
	{
		String bandoGanador="";
		if (!(drones.estaVivoDroneI(0)) && !(drones.estaVivoDroneI(1)))
		{
			bandoGanador="Aereo";
		}
		else if((!(drones.estaVivoDroneI(2)) && !(drones.estaVivoDroneI(3)))||(mapa.baseDestruida()))
		{
			bandoGanador="Terrestre";
		}
		return bandoGanador;
	}
	
	public boolean municionesDisponibles(String iD)
	{
		int vRol = getRolPorId(iD);
		return drones.municionesDisponibles(vRol);
	}
	

	public String disparar(String iD)
	{
		String impacto="";
		String json="";
		int vRol = getRolPorId(iD);
		String stringRol = getRolDrone(vRol);
		if (drones.canionDisponible(vRol))
		{
			if (drones.municionesDisponibles(vRol))
			{
				String disparar=",\"disparar\":\"true\"";
				int municiones = drones.actualizarMunicionesDroneI(vRol);
				float x= drones.getCoordXDroneI(vRol);
				float y= drones.getCoordYDroneI(vRol);
				int z = drones.getZDroneI(vRol);
				float angulo= drones.getAnguloDroneI(vRol);		
				
				
				if (vRol==0||vRol==1)
				{
					if (drones.estaVivoDroneI(2))
					{					
						if (drones.recibioImpactoDroneI(2, x, y, z, angulo))
						{								
							impacto="A1";
						}
					}	
					if (drones.estaVivoDroneI(3)) 
					{						
						if(drones.recibioImpactoDroneI(3, x, y, z, angulo))
						{							
							impacto="A2";
						}
					}
					if(mapa.recibioImpactoBase(x, y, z,angulo))
					{
						impacto="BASE";
					}
				}
				else
				{
					if (drones.estaVivoDroneI(0))
					{
						if (drones.recibioImpactoDroneI(0, x, y, z, angulo))
						{						
							impacto="T1";
						}
					}
					if (drones.estaVivoDroneI(1))
					{
						if(drones.recibioImpactoDroneI(1, x, y, z, angulo))
						{						
							impacto="T2";
						}
					}
				}		
				
				if (!(impacto.equals("")))
				{
					boolean destruido=false;
					json="";
					
					switch (impacto)
					{
					case "A1":
					{					
						json = drones.recibirImpactoDroneI(2);
						if (!drones.estaVivoDroneI(2))
							destruido=true;
						json= "impacto_{\"rolDispara\":\""+stringRol+"\",\"municiones\":\""+municiones+"\",\"rolRecibe\": \"A1\",\"destruido\":\""+destruido+"\""+ disparar + json;
					}
					break;
					case "A2":
					{					
						json = drones.recibirImpactoDroneI(3);
						if (!drones.estaVivoDroneI(3))
							destruido=true;
						json= "impacto_{\"rolDispara\":\""+stringRol+"\",\"municiones\":\""+municiones+"\",\"rolRecibe\": \"A2\",\"destruido\":\""+destruido+"\"" + disparar + json;
					}
					break;
					case "T1":
					{					
						json = drones.recibirImpactoDroneI(0);
						if (!drones.estaVivoDroneI(0))
							destruido=true;
						json = "impacto_{\"rolDispara\":\""+stringRol+"\",\"municiones\":\""+municiones+"\",\"rolRecibe\": \"T1\",\"destruido\":\""+destruido+"\"" + disparar + json;
					}
					break;
					case "T2":
					{					
						json = drones.recibirImpactoDroneI(1);
						if (!drones.estaVivoDroneI(1))
							destruido=true;
						json = "impacto_{\"rolDispara\":\""+stringRol+"\",\"municiones\":\""+municiones+"\",\"rolRecibe\": \"T2\",\"destruido\":\""+destruido+"\"" + disparar + json; 
					}
					break;
					case "BASE":
					{					
						json = mapa.recibirImpactoBase();
						if (mapa.baseDestruida())
							destruido=true;
						json = "impacto_{\"rolDispara\":\""+stringRol+"\",\"municiones\":\""+municiones+"\",\"rolRecibe\": \"BASE\",\"destruido\":\""+destruido+"\"" + disparar + json;
					}
					break;
					}
					if (partidaFinalizada())
					{
						this.partidaIniciada=false;
						json= json+"\"finalizarPartida\":\"true\",";
						json=json+this.obtenerSobrevivientes();
						if (bandoGanador().equals("Aereo"))
						{
							json=json+",\"bandoGanador\":\"aereo\"}";
						}
						else
						{
							json=json+",\"bandoGanador\":\"terrestre\"}";	
						}						
					}
					else
					{
						json= json+"\"finalizarPartida\":\"false\"}";
					}
				}
				else
				{
					 json = "impacto_{\"impacta\":\""+false+"\",\"rolDispara\":\""+stringRol+"\",\"municiones\":\""+ municiones+"\""+ disparar + "}";
				}
			}			
		}
		return json;
	}
	
	public void setIdSessionDroneI (int pRol,String iD)
	{
		drones.setIdPorRol(pRol, iD);
	}
	
	public String subirNivel (String iD)
	{
		int vRol = getRolPorId(iD);
		Drone vdrone=null;
		if (vRol==0||vRol==1)
		{
			vdrone = (DroneTerrestre)obtenerDronPorRol(vRol);
		}
		else
		{
			vdrone = (DroneAereo)obtenerDronPorRol(vRol);
		}	
		
		vdrone.subirNivel();

		int z=vdrone.getCoordZ();

		
		String json="{\"id\": \""+vRol+"\",\"z\":\""+z+"\""+"}";
		json="cambiarAltura_"+json;
		return json;
	}
	
	public String bajarNivel (String iD)
	{
		int vRol = getRolPorId(iD);
		Drone vdrone=null;
		if (vRol==0||vRol==1)
		{
			vdrone = (DroneTerrestre)obtenerDronPorRol(vRol);
		}
		else
		{
			vdrone = (DroneAereo)obtenerDronPorRol(vRol);
		}	
		
		vdrone.bajarNivel();

		int z=vdrone.getCoordZ();

		
		String json="{\"id\": \""+vRol+"\",\"z\":\""+z+"\""+"}";
		json="cambiarAltura_"+json;	
		return json;
	}
	
	public void recargarMunicionesDroneI(String Id)
	{
		int vRol = getRolPorId(Id);
		drones.recargarMunicionesDroneI(vRol);
	}
	
	public boolean canionDisponible (int rol)
	{		
		return drones.canionDisponible(rol);
	}
	
	public boolean esJugadorPorId(String iD)
	{
		boolean esJugador = false;
		int i=0;
		while (i<4 && !esJugador)
		{
			if ((drones.getIdPorRol(i)).equals(iD))
			{
				esJugador=true;
			}
			i++;
		}
		return esJugador;
	}
	
	public void liberarDronId (String iD)
	{			
		String vacio="";
		int vRol=getRolPorId(iD);		
		drones.setIdDroneI(vRol, vacio);
		drones.setApodoDroneI(vRol, vacio);		
		drones.setInactivoDroneI(vRol);		
	}
	public void liberarEspectadorId (String iD)
	{
		vEspectadores.eliminarEspectador(iD);
	}
	public void liberarTodosLosEspectadores()
	{
		vEspectadores.liberarTodosLosEspectadores();
	}
	
	public void liberarTodosLosDrones()
	{
		drones.liberarTodosLosDrones();
	}
	
	public void liberarConexionesADronesYEspectadores()
	{
		liberarTodosLosEspectadores();
		liberarTodosLosDrones();
	}
	
	public String setInfoJugador(String iD, String infoJugador)
	{	
		if (esJugadorPorId(iD))
		{
			liberarDronId(iD);
		}
		if (vEspectadores.esEspectador(iD))
		{
			vEspectadores.eliminarEspectador(iD);
		}
		
		
		String[] parts = infoJugador.split("_");
		String apodo = parts[1];
		String bando = parts[2];		
		if (bando.equals("aereo"))
		{			
			if (drones.obtenerDronPorRol(2).getApodo().equals(""))
			{
				drones.setApodoDroneI(2, apodo);
				drones.setIdDroneI(2, iD);				
				drones.setActivoDroneI(2);
			}
			else 
				if (drones.obtenerDronPorRol(3).getApodo().equals(""))
			{
				drones.setApodoDroneI(3, apodo);
				drones.setIdDroneI(3, iD);				
				drones.setActivoDroneI(3);
			}
		}
		else if(bando.equals("terrestre"))
		{			
			if(drones.obtenerDronPorRol(0).getApodo().equals(""))
			{
				drones.setApodoDroneI(0, apodo);
				drones.setIdDroneI(0, iD);
				drones.setActivoDroneI(0);
			}			
			else
			if(drones.obtenerDronPorRol(1).getApodo().equals(""))
			{
				drones.setApodoDroneI(1, apodo);
				drones.setIdDroneI(1, iD);
				drones.setActivoDroneI(1);
			}
		}
		else
		{
			Espectador vEspectador = new Espectador (iD, apodo);
			vEspectadores.addEspectador(vEspectador);
		}
		
			
		String json= "{\"participantesEnLobby\":[";		
		
		for (int i=0;i<4;i++)
		{
			String stringRol=getRolDrone(i);
			String apodoI = drones.obtenerDronPorRol(i).getApodo();
			if (apodoI!="")
			{
				json= json+"{"+"\"stringRol\":\""+stringRol+"\",\"apodo\":\""+apodoI+"\""+"},";
			}
		}
		String apodoEspectador;
		for (int j=0;j<vEspectadores.getLargo();j++)
		{
			apodoEspectador = (vEspectadores.getEspectador(j)).getApodo();
			json= json+"{"+"\"stringRol\":\""+"noAsignado"+"\",\"apodo\":\""+apodoEspectador+"\""+"},";
		}		
		
		json = json.substring(0, json.length()-1);
		json = json + "]}";	
		return json;
	}
	
	public String mostrarLobby(String iD, String infoJugador)
	{	
		String[] parts = infoJugador.split("_");
		String apodo = parts[1];
		
		Espectador vEspectador = new Espectador (iD, apodo);
		
		vEspectadores.addEspectador(vEspectador);
		
		String json="{\"myid\":\""+iD+"\",";
		json+= "\"participantes\":[";
		
		for (int i=0;i<4;i++)
		{
			String stringRol=getRolDrone(i);
			String apodoI = drones.obtenerDronPorRol(i).getApodo();
			if (apodoI!="")
			{
				json= json+"{"+"\"stringRol\":\""+stringRol+"\",\"apodo\":\""+apodoI+"\""+"},";
			}
		}
		String apodoEspectador;
		for (int j=0;j<vEspectadores.getLargo();j++)
		{
			apodoEspectador = (vEspectadores.getEspectador(j)).getApodo();
			json= json+"{"+"\"stringRol\":\""+"noAsignado"+"\",\"apodo\":\""+apodoEspectador+"\""+"},";
		}		
		
		json = json.substring(0, json.length()-1);
		json = json + "]}";		
		return json;
	}
	
	public String quitarDelLobby()
	{
		String json = "{\"participantes\":[";
		
		for (int i=0;i<4;i++)
		{
			String stringRol=getRolDrone(i);
			String apodoI = drones.obtenerDronPorRol(i).getApodo();
			if (apodoI!="")
			{
				json= json+"{"+"\"stringRol\":\""+stringRol+"\",\"apodo\":\""+apodoI+"\""+"},";
			}
		}
		String apodoEspectador;
		for (int j=0;j<vEspectadores.getLargo();j++)
		{
			apodoEspectador = (vEspectadores.getEspectador(j)).getApodo();
			json= json+"{"+"\"stringRol\":\""+"noAsignado"+"\",\"apodo\":\""+apodoEspectador+"\""+"},";
		}		
		
		json = json.substring(0, json.length()-1);
		json = json + "]}";		
		return json;
	}
	
	public String obtenerDatosDrones()
	{
		String json = "\"drones\":[";
		for (int i=0;i<4;i++)
		{
			json= json+drones.obtenerDatosDroneI(i)+","; 
		}
		json = json.substring(0, json.length()-1);
		json=json+"]";
		return json;
	}
	
	public String obtenerDatosBase ()
	{
		return mapa.obtenerDatosBase();
	}
	
	public boolean esEspectadorPorId(String iD)
	{
		return vEspectadores.esEspectador(iD);
	}
	
	public String cambiarDeDrone(String iD)
	{
		
		int rol = getRolPorId(iD);
		String json="{\"cambiarDrone\":\"desactivado\"}";
		int nuevoRol=0;
		
		if (rol < 4)
		{
			if (rol==0)
				{nuevoRol=1;}
			if (rol==1)
				{nuevoRol=0;}
			if (rol==2)
				{nuevoRol=3;}
			if (rol==3)
				{nuevoRol=2;}
			
			if (drones.getIdDroneI(nuevoRol).equals(iD))
			{
				drones.setActivoDroneI(nuevoRol);
				
				drones.setInactivoDroneI(rol);
					
				json="cambiarDrone_"+"{"+"\"id\":\""+iD+"\","+"\"mask\":\""+drones.getRolDrone(nuevoRol)+"\""+"}";
			}
		}
		
		return json;
	}
	
	public String tirarBomba(String iD)
	{
		String impacto="";
		String json="";
		int vRol = getRolPorId(iD);
		String stringRol = getRolDrone(vRol);
		if (vRol==2||vRol==3)
		{
			if (drones.tieneBombaDroneI(vRol))
			{
				drones.actualizarBombaDroneI(vRol,false);
				float x= drones.getCoordXDroneI(vRol);
				float y= drones.getCoordYDroneI(vRol);			

				if ((drones).impactoBombaDroneI(0, x, y))
				{
					if (drones.estaVivoDroneI(0))
						impacto="T1";
				}
				else if(drones.impactoBombaDroneI(1, x, y))
				{
					if (drones.estaVivoDroneI(1))
						impacto="T2";
				}
				else if(mapa.impactoBombaBase(x, y))
				{
					impacto="BASE";
				}
				else
				{
					impacto="";
				}
				
				if (!(impacto.equals("")))
				{
					switch (impacto)
					{
					case "T1":
					{						
						drones.recibirImpactoBombaDroneI(0);
						json = "disparoBomba_{\"rolDispara\":\""+stringRol+"\",\"rolRecibe\": \"T1\", ";						
					}
					break;
					case "T2":
					{
						drones.recibirImpactoBombaDroneI(1);
						json = "disparoBomba_{\"rolDispara\":\""+stringRol+"\",\"rolRecibe\": \"T2\", ";					
					}
					break;
					case "BASE":
					{
						mapa.recibirImpactoBombaBase();
						json = "disparoBomba_{\"rolDispara\":\""+stringRol+"\",\"rolRecibe\": \"BASE\", ";	
					}
					break;
					}
					if (partidaFinalizada())
					{
						this.partidaIniciada=false;
						json= json+"\"finalizarPartida\":\"true\",";
						json=json+this.obtenerSobrevivientes();
						if (bandoGanador().equals("Aereo"))
						{
							json=json+",\"bandoGanador\":\"aereo\"}";
						}
						else
						{
							json=json+",\"bandoGanador\":\"terrestre\"}";	
						}						
					}
					else
					{
						json = json+"\"finalizarPartida\":\"false\"}";
					}
				}
				else
				{
					 json = "disparoBomba_{\"impacta\":\""+false+"\",\"rolDispara\":\""+stringRol+"\"}";
				}
			}
		}			
		return json;
	}
	
	public boolean estaVivoDrone(String iD)
	{
		int vRol = getRolPorId(iD);
		return drones.estaVivoDroneI(vRol);
	}
	
	public boolean estaVivoDrone(int pRol)
	{		
		return drones.estaVivoDroneI(pRol);
	}
	
	public String recgargarBomba (String Id)
	{
		String json="";
		int vRol = getRolPorId(Id);
		if (vRol==2||vRol==3)
		{
			float coordX= drones.getCoordXDroneI(vRol);
			float coordY= drones.getCoordYDroneI(vRol);
			int coordZ= drones.getZDroneI(vRol);
			if (mapa.estaEnLaBaseDroneI(coordX, coordY, coordZ)){
				drones.actualizarBombaDroneI(vRol, true);
				String stringRol = drones.getRolDrone(vRol);
				json= "recargar_{\"rol\":\""+stringRol+"\",\"bomba\":\"true\"}";
			}
		}
		return json;
	}
	
	public void guardarPartida(String message) throws Exception
	{		
		String[] parts = message.split("_");
		String nombrePartida = parts[1];
		
		vDAOpartida.insert(nombrePartida);
		Puente[] pasajes = mapa.obtenerPasajes();
		vDAOPasaje.insertarPasajes(pasajes, nombrePartida);
		Base vBase = mapa.getBase();
		vDAOBase.insert(vBase, nombrePartida);
		vDAODrone.insertarDrones(drones, nombrePartida);
		
	}
	
	public String listarPartidas () throws Exception
	{
		String json="listarPartidas_{\"error\":\"false\",\"partidas\":[";
		ArrayList <VOPartida> partidas = vDAOpartida.listarPartidas();
		String id;
		String fecha;		
		for (int i=0;i<partidas.size();i++)
		{
			id = partidas.get(i).getId();
			fecha = partidas.get(i).getFecha().toString();
			json=json+"{\"id\":\""+id+"\",\"fecha\":\""+fecha+"\"},";
		}
		json = json.substring(0, json.length()-1);
		if(partidas.size()==0)
		{
			
			json = json+"\"\"}";
		}
		else
		{
			json = json+"]}";
		}

		return json;
	}
	
	public void recuperarPartida(String message) throws Exception
	{
		String[] parts1 = message.split("_");		
		String stringPartida = parts1[1];
		String[] parts2 = stringPartida.split("-");
		String nombrePartida = parts2[0].substring(0, parts2[0].length()-1);		
		this.drones = vDAODrone.obtenerDrones(nombrePartida);	
		partidaIniciada=false;
		partidaPausada=false;
		mapa.generarBase(vDAOBase.find(nombrePartida));		
		mapa.setPuentes(vDAOPasaje.obtenerPasajes(nombrePartida));		
	}
	
	public boolean puedeComenzar()
	{
		if (((!(drones.getApodoDroneI(0).equals("")))||(!(drones.getApodoDroneI(1).equals(""))))&&((!(drones.getApodoDroneI(2).equals("")))||(!(drones.getApodoDroneI(3).equals("")))))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public boolean puedeJugar(String message)
	{
		boolean puedeJugar=false;
		String[] parts = message.split("_");		
		String bando = parts[2];
		
		switch(bando)
		{
			case "aereo":
			{	
				int cantAereosVivos=0;				
				if (this.partidaRecuperada)
				{
					if (drones.estaVivoDroneI(2))
					{
						cantAereosVivos++;
					}
					if (drones.estaVivoDroneI(3))
					{
						cantAereosVivos++;
					}
					if (cantAereosVivos==2)
					{
						if ((drones.getApodoDroneI(2).equals(""))||(drones.getApodoDroneI(3).equals("")))
						{
							puedeJugar=true;
						}
					}
					else
					{
						if ((drones.getApodoDroneI(2).equals(""))&&(drones.getApodoDroneI(3).equals("")))
						{
							puedeJugar=true;
						}
					}
				}
				else if ((drones.getApodoDroneI(2).equals(""))||(drones.getApodoDroneI(3).equals("")))
				{
					puedeJugar=true;
				}
			}
			break;
			case "terrestre":
			{					
				if (this.partidaRecuperada)
				{
					int cantTerrestresVivos=0;
					if (drones.estaVivoDroneI(0))
					{
						cantTerrestresVivos++;
					}
					if (drones.estaVivoDroneI(1))
					{
						cantTerrestresVivos++;
					}
					if (cantTerrestresVivos==2)
					{
						if ((drones.getApodoDroneI(0).equals(""))||(drones.getApodoDroneI(1).equals("")))
						{
							puedeJugar=true;
						}
					}
					else
					{
						if ((drones.getApodoDroneI(0).equals(""))&&(drones.getApodoDroneI(1).equals("")))
						{
							puedeJugar=true;
						}
					}
				}
				else if ((drones.getApodoDroneI(0).equals(""))||(drones.getApodoDroneI(1).equals("")))
				{
					puedeJugar=true;
				}
			}
			break;
		}
		return puedeJugar;
	}
	
	public void destruirDrone (String pId)
	{
		int vRol = getRolPorId(pId);
		drones.destruirDrone(vRol);
	}
	
	public boolean unJugador (String pId)
	{
		boolean unJugador=false;
		int vRol = getRolPorId(pId);
		if (vRol==0||vRol==1)
		{
			if(drones.getIdPorRol(0).equals(drones.getIdPorRol(1)))
			{
				unJugador= true;
			}
			else
			{
				unJugador= false;
			}
		}
		if (vRol==2||vRol==3)
		{
			if(drones.getIdPorRol(2).equals(drones.getIdPorRol(3)))
			{
				unJugador= true;
			}
			else
			{
				unJugador= false;
			}
		}
		return unJugador;
		
	}
	
	public String obtenerSobrevivientes()
	{		
		String json="\"sobrevivientes\":[";
		for (int i=0;i<4;i++)
		{
			if (drones.estaVivoDroneI(i))
				json = json + "{\"apodo\":\""+drones.getApodoDroneI(i)+"\"},";
		}
		json = json.substring(0, json.length()-1);
		json = json + "]";
		return json;
	}	
	
	public void inicializarSesiones()
	{
		drones.inicializarSesiones();
	}

	public String obtenerJsonPuentes() 
	{
		return this.mapa.obtenerJsonPuentes();
	}
}
