package Logica;

public class Drones {
	
	private  Drone vDrones[];
	
	
	public enum TDrone
	{
		T1,T2,A1,A2
	}
	
	public String getRolDrone(int i)
	{
		String rol=null;
		switch(i)
		{
			case 0:
				rol="T1";
			break;
			case 1:
				rol="T2";
			break;
			case 2:
				rol="A1";
			break;
			case 3:
				rol="A2";
			break;
			
		}
		
		return rol;
	}
	
	public Drones()
	{	
		vDrones = new Drone[4];
		vDrones[0] = new DroneTerrestre();
		((DroneTerrestre) vDrones[0]).ubicarDroneTerrestre();
		vDrones[1] = new DroneTerrestre();
		((DroneTerrestre) vDrones[1]).ubicarDroneTerrestre();
		vDrones[2] = new DroneAereo();
		((DroneAereo) vDrones[2]).ubicarDroneAereo();
		vDrones[3] = new DroneAereo();
		((DroneAereo) vDrones[3]).ubicarDroneAereo();
	}
	
	public Drones (DroneTerrestre pDt1, DroneTerrestre pDt2, DroneAereo pDa1, DroneAereo pDa2)
	{
		vDrones = new Drone[4];
		vDrones[0]=pDt1;
		vDrones[1]=pDt2;
		vDrones[2]=pDa1;
		vDrones[3]=pDa2;
		
	}

	public Drone obtenerDronPorRol(int i)
	{
		return vDrones[i];
	}
	
	public String getIdPorRol(int i)
	{
		return vDrones[i].getId();
	}
	
	public void setIdPorRol (int pRol, String iD)
	{
		vDrones[pRol].setId(iD);
	}
	
	public void setDroneAereo(DroneAereo pDrone, int posicion)
	{		
		vDrones[posicion].setCoordX(pDrone.getCoordX());
		vDrones[posicion].setCoordY(pDrone.getCoordY());
		vDrones[posicion].setCoordZ(pDrone.getCoordZ());
		vDrones[posicion].setAngulo(pDrone.getAngulo());
		vDrones[posicion].setCamara(pDrone.isCamara());
		vDrones[posicion].setCanion(pDrone.isCanion());
		vDrones[posicion].setMuniciones(pDrone.getMuniciones());
		((DroneAereo)vDrones[posicion]).setBomba(pDrone.isBomba());
		((DroneAereo)vDrones[posicion]).setMotor(pDrone.getMotor());	
	}
	
	public void setDroneTerrestre(DroneTerrestre pDrone, int posicion)
	{		
		vDrones[posicion].setCoordX(pDrone.getCoordX());
		vDrones[posicion].setCoordY(pDrone.getCoordY());
		vDrones[posicion].setCoordZ(pDrone.getCoordZ());
		vDrones[posicion].setAngulo(pDrone.getAngulo());
		vDrones[posicion].setCamara(pDrone.isCamara());
		vDrones[posicion].setCanion(pDrone.isCanion());
		vDrones[posicion].setMuniciones(pDrone.getMuniciones());
		((DroneTerrestre)vDrones[posicion]).setBlindaje(pDrone.getBlindaje());						
	}
	
	public String obtenerDatosDroneI(int i)
	{
		String stringRol = getRolDrone(i);
		if (i==0||i==1)
			return "{\"id\":\""+((DroneTerrestre) vDrones[i]).getId()+"\",\"rol\": \""+stringRol+"\",\"apodo\":\""+vDrones[i].getApodo()+"\""+ ((DroneTerrestre) vDrones[i]).obtenerDatosDrone();
		else
			return "{\"id\":\""+((DroneAereo) vDrones[i]).getId()+"\",\"rol\": \""+stringRol+"\",\"apodo\":\""+vDrones[i].getApodo()+"\""+ ((DroneAereo) vDrones[i]).obtenerDatosDrone();
		
		
	}
	
	public void setApodoDroneI(int i, String pApodo)
	{
		vDrones[i].setApodo(pApodo);
	}
	
	public String getApodoDroneI(int i)
	{
		return vDrones[i].getApodo();
	}
	
	public void setIdDroneI(int i, String pId)
	{		
		vDrones[i].setId(pId);
	}	

	public void setActivoDroneI(int i)
	{
		vDrones[i].setActivo();
	}
	
	public void setInactivoDroneI(int i)
	{
		vDrones[i].setInactivo();
	}
	
	public boolean getActivoI(int i)
	{
		return vDrones[i].getActivo();
	}	
	
	public String recibirImpactoDroneI(int i)
	{
		if (i==0||i==1)
			return ((DroneTerrestre) vDrones[i]).recibirImpacto();
		else
			return ((DroneAereo) vDrones[i]).recibirImpacto();
	}
	
	public boolean estaVivoDroneI(int i)
	{
		return vDrones[i].estaVivo();
	}
	
	public int actualizarMunicionesDroneI(int i)
	{
		return vDrones[i].actualizarMuniciones();
	}
	
	public float getCoordXDroneI(int i)
	{
		return vDrones[i].getCoordX();
	}
	
	public float getCoordYDroneI(int i)
	{
		return vDrones[i].getCoordY();
	}
	
	public int getZDroneI(int i)
	{
		return vDrones[i].getCoordZ();
	}
	
	public float getAnguloDroneI(int i)
	{
		return vDrones[i].getAngulo();
	}
	
	public String getIdDroneI(int i)
	{
		return vDrones[i].getId();
	}
	
	public boolean recibioImpactoDroneI(int i, float x, float y, int z, float angulo){
		return vDrones[i].estaEnLaRecta(x,y,z,angulo);
	}
	
	public void actualizarBombaDroneI (int i, boolean pBomba)
	{
		((DroneAereo) vDrones[i]).actualizarBomba(pBomba);
	}
	
	public boolean tieneBombaDroneI (int i)
	{
		return ((DroneAereo) vDrones[i]).isBomba();
	}
	
	public boolean impactoBombaDroneI (int i, float x, float y)
	{
		return ((DroneTerrestre) vDrones[i]).impactoBomba(x, y);
	}
	
	public void recibirImpactoBombaDroneI(int i)
	{
		((DroneTerrestre) vDrones[i]).recibirImpactoBomba();
	}
	
	public boolean canionDisponible (int i)
	{
		return vDrones[i].isCanion();
	}
	
	public boolean municionesDisponibles(int i)
	{
		if (vDrones[i].getMuniciones() > 0)
			return true;
		else
			return false;
	}
	
	public void recargarMunicionesDroneI (int Id)
	{
		if (Id == 0 || Id == 1)
		{
			vDrones[Id].setMuniciones(5);
		}
		else
		{
			vDrones[Id].setMuniciones(3);
		}
	}	
	
	public void inicializarSesiones()
	{
		if (!vDrones[0].getId().equals("")) //T1
			if (vDrones[1].getId().equals("")) //T2
				vDrones[1].setId(vDrones[0].getId());
		
		if (!vDrones[2].getId().equals("")) //A1
			if (vDrones[3].getId().equals("")) //A2
				vDrones[3].setId(vDrones[2].getId());
		
	}

	public void destruirDrone(int vRol) 
	{
		if (vRol==0||vRol==1)
		{
			if (vDrones[0].getId().equals(vDrones[1].getId()))
			{
				((DroneTerrestre)vDrones[0]).destruirDrone();
				((DroneTerrestre)vDrones[1]).destruirDrone();
			}
			else
			{
				((DroneTerrestre)vDrones[vRol]).destruirDrone();
			}			
		}
		else
		{
			if (vDrones[2].getId().equals(vDrones[3].getId()))
			{
				((DroneAereo)vDrones[2]).destruirDrone();
				((DroneAereo)vDrones[3]).destruirDrone();
			}
			else
			{
				((DroneAereo)vDrones[vRol]).destruirDrone();
			}			
		}
	}

	public void liberarTodosLosDrones() 
	{
		vDrones[0].setId("");
		vDrones[0].setApodo("");
		vDrones[1].setId("");
		vDrones[1].setApodo("");
		vDrones[2].setId("");
		vDrones[2].setApodo("");
		vDrones[3].setId("");
		vDrones[3].setApodo("");
	}
}
