package Logica;

public class DroneTerrestre extends Drone
{
	private int blindaje;
	
	public DroneTerrestre(float x, float y, int z, float ang, boolean cam, boolean can, int mun, String apd, String idS, int bl)
	{
		super(x, y, z, ang, cam, can, mun, apd, idS);
		this.blindaje=bl;
	}
	
	public DroneTerrestre()
	{
		super (180,true,true,5,"","");
		this.blindaje=3;
	}

	public int getBlindaje() 
	{
		return blindaje;
	}

	public void setBlindaje(int blindaje) 
	{
		this.blindaje = blindaje;
	}

	public void subirNivel() 
	{
		if (coordZ<1)
		{
			coordZ++;
		}
		
	}
	
	public void bajarNivel() 
	{
		if (coordZ>0)
		{
			coordZ--;
		}
		
	}
	
	public boolean estaVivo() {
		boolean estaVivo=true;
		if (!camara && !canion && blindaje==0)
			estaVivo=false;
		return estaVivo;
	}
	
	public void ubicarDroneTerrestre()
	{		
		coordX = 899 + ((float)(Math.random()*(227)));
		coordY = 26 + ((float) (Math.random()*(728)));
		coordZ = 0;
	}
	
	public String obtenerDatosDrone()
	{	
		
		float x = getCoordX();
		float y = getCoordY();
		int z = getCoordZ();	
		float angulo= getAngulo();
		boolean camara = isCamara();
		boolean canion = isCanion();
		int municiones = getMuniciones();
		int blindaje = getBlindaje();
		String json = ",\"x\": \""+x+"\",\"y\":\""+y+"\",\"z\":\""+z+"\",\"angulo\":\""+angulo+"\",\"camara\":\""+camara+"\",\"canion\":\""+canion+"\",\"municiones\":\""+municiones+"\",\"blindaje\":\""+blindaje+"\",\"destruido\":\""+(!(this.estaVivo()))+"\"}";
		return json;
		
		
	}
	
	public String recibirImpacto()
	{
		String json="";
		int impacto = (int)(Math.random()*4);
		boolean pudoImpactar = false;
		while (!pudoImpactar)
		{
			switch(impacto)
			{
			case 0:		//IMPACTA LA CAMARA
			{
				if (camara)
				{
					pudoImpactar=true;
					camara=false;
					json=",\"camara\": \"false\", ";
				}
				
			}
			break;
			case 1:		//IMPACTA EL CANION
			{
				if (canion)
				{
					pudoImpactar=true;
					canion=false;
					json=",\"canion\": \"false\", ";
				}
				
			}
			break;
			case 2:		//IMPACTA EL BLINDAJE
			{
				if (blindaje>0)
				{
					pudoImpactar=true;
					blindaje--;
					json= ",\"blindaje\": \""+blindaje+"\", ";
				}			
			}
			break;
			}
			if (!pudoImpactar)
			{
				impacto = (int)(Math.random()*4);
			}
		}
		return json;
	}
	
	public boolean impactoBomba(float x, float y)
	{
		boolean impacto=false;
		float x1=this.coordX-25;
		float x2=this.coordX+25;
		float y1=this.coordY-25;
		float y2=this.coordY+25;
		
		if (x1<=x && x<=x2 && y1<=y && y<=y2)
			impacto=true;
		
		return impacto;
	}
	
	public void recibirImpactoBomba()
	{
		this.camara=false;
		this.canion=false;
		this.blindaje=0;
	}
	
	public boolean estaEnLaRecta(float x, float y, int z, float pAngulo)
	{
		boolean impacta = false;
		
		float catX2 = (coordX-x)*(coordX-x);
		float catY2 = (coordY-y)*(coordY-y);
		float hyp = (float)Math.sqrt(catX2 +catY2);
		
		if(z < 2)
		{
			if(hyp<300)
			{			
				if(((0 < pAngulo && pAngulo < 180)&& coordY > y) || ((0 > pAngulo && pAngulo > -180)&& coordY < y) || ((pAngulo == 0)&& coordX > x)|| ((pAngulo == -180) && coordX < x) || ((pAngulo == 180) && coordX < x) || ((180 < pAngulo && pAngulo < 360)&& coordY < y) || ((-360 < pAngulo && pAngulo < -180)&& coordY > y))
				{
					float vCoordX = this.coordX;
					float vCoordY = this.coordY;
					float pendiente = (float)(Math.tan(Math.toRadians(pAngulo)));
					
					float xSub1 = vCoordX - 25;
					float xSub2 = vCoordX + 25;
					float ySub1 = vCoordY - 25;
					float ySub2 = vCoordY + 25;
					
					float yaux = (pendiente *(xSub1-x))+y;
					if (ySub2>=yaux &&  yaux>=ySub1)
					{
						impacta = true;
					}
					
					yaux = (pendiente *(xSub2-x))+y;
					if (!impacta && ySub2>=yaux &&  yaux>=ySub1)
					{
						impacta = true;
					}
					
					float xaux = ((ySub1-y)/pendiente)+x;
					if (!impacta && xSub2 >= xaux && xaux>=xSub1)
					{
						impacta=true;
					}
					
					xaux = ((ySub2-y)/pendiente)+x;
					if (!impacta && xSub2 >= xaux && xaux>=xSub1)
					{
						impacta=true;
					}
				}
			}
		}
		return impacta;
	}
	
	public void avanzar (Mapa pMapa)
	{		
		if (puedeAvanzar(pMapa))
		{
			switch (blindaje)
			{
				case 0:
				{
					coordX+=Math.cos(Math.toRadians(angulo))*0;
					coordY+=Math.sin(Math.toRadians(angulo))*0;	
				}
				break;
				case 1:
				{
					coordX+=Math.cos(Math.toRadians(angulo))*1;
					coordY+=Math.sin(Math.toRadians(angulo))*1;	
				}
				break;
				case 2:
				{
					coordX+=Math.cos(Math.toRadians(angulo))*2;
					coordY+=Math.sin(Math.toRadians(angulo))*2;	
				}
				break;
				case 3:
				{
					coordX+=Math.cos(Math.toRadians(angulo))*4;
					coordY+=Math.sin(Math.toRadians(angulo))*4;	
				}
				break;
			}
		}
	}
	
	public void retroceder (Mapa pMapa)
	{
		if (puedeRetroceder(pMapa))
		{
			switch (blindaje)
			{
				case 0:
				{
					coordX-=(Math.cos(Math.toRadians(angulo)))*0;
					coordY-=(Math.sin(Math.toRadians(angulo)))*0;	
				}
				break;
				case 1:
				{
					coordX-=(Math.cos(Math.toRadians(angulo)))*1;
					coordY-=(Math.sin(Math.toRadians(angulo)))*1;	
				}
				break;
				case 2:
				{
					coordX-=(Math.cos(Math.toRadians(angulo)))*2;
					coordY-=(Math.sin(Math.toRadians(angulo)))*2;	
				}
				break;
				case 3:
				{
					coordX-=Math.cos(Math.toRadians(angulo))*4;
					coordY-=Math.sin(Math.toRadians(angulo))*4;	
				}
				break;
			}	
		}
	}

	public void destruirDrone() 
	{
		camara=false;
		canion=false;
		blindaje=0;
	}
}
