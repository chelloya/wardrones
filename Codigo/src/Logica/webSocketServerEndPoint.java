package Logica;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import javax.naming.NamingException;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

@ServerEndpoint("/websocketendpoint")
public class webSocketServerEndPoint {	
	 	
	
	private static final Set<Session> vSesiones = Collections.synchronizedSet(new HashSet<Session>());
	private Fachada vFachada = Fachada.getInstancia();
	
		@OnOpen
		public void onOpen(Session vSession)
		{	
			/*
			 * La posicion en el arreglo define el rol del drone
			 */	
			
            String json="estadoPartida_{\"partidaIniciada\":\""+vFachada.partidaIniciada()+"\"}";
			vSesiones.add(vSession);	
			try {
				vSession.getBasicRemote().sendText(json);
			} catch (IOException e) {				
				e.printStackTrace();
			}
		}
		
		@OnClose
		public void onClose(Session vSession)
		{		
			String json="";
			
			if (vFachada.partidaIniciada())
			{				
				vFachada.destruirDrone(vSession.getId());
				boolean partidaFinalizada = vFachada.partidaFinalizada();
				boolean unJugador = false;
				if(vFachada.unJugador(vSession.getId())) 
				{
					unJugador = true;
				}			
				
				json="abandonoSesion_{\"finalizarPartida\":\""+partidaFinalizada+"\",\"rol\":\"";
				int vRol = vFachada.getRolPorId(vSession.getId());
				switch (vRol)
				{
					case 0:
					{		
						json=json+"T1\",\"destruido\":\"true\", \"unJugador\":\""+unJugador+"\"}";
					}
					break;
					case 1:
					{
						json=json+"T2\",\"destruido\":\"true\", \"unJugador\":\""+unJugador+"\"}";
					}
					break;
					case 2:
					{
						json=json+"A1\",\"destruido\":\"true\", \"unJugador\":\""+unJugador+"\"}";
					}
					break;
					case 3:
					{
						json=json+"A2\",\"destruido\":\"true\", \"unJugador\":\""+unJugador+"\"}";
					}
					break;
				}				
			}
			else if (vFachada.esJugadorPorId(vSession.getId()))
			{				
				vFachada.liberarDronId(vSession.getId());
				json = vFachada.quitarDelLobby();
				json = "participantes_"+json;
			}
			else if(vFachada.esEspectadorPorId(vSession.getId()))
			{
				vFachada.liberarEspectadorId(vSession.getId());
				json = vFachada.quitarDelLobby();
				json = "participantes_"+json;
			}
		
			
			if (vFachada.partidaFinalizada())
			{
				vFachada.setPartidaInicidad(false);
				json = json.substring(0, json.length()-1);
				json = json + "," + vFachada.obtenerSobrevivientes();
				
				if (vFachada.bandoGanador().equals("Aereo"))
				{
					json=json+",\"bandoGanador\":\"aereo\"}";
				}
				else
				{
					json=json+",\"bandoGanador\":\"terrestre\"}";	
				}				
			}
			
			try {  
				for (Session s : vSession.getOpenSessions()) 
			      {
			        if (s.isOpen()) 
			        {				        	
		        		if ((vFachada.esJugadorPorId(s.getId())) || (vFachada.esEspectadorPorId(s.getId())))
		        		{
		        			s.getBasicRemote().sendText(json);
		        		}	
			        }
			      } 
				
	    	  	if (!vFachada.partidaIniciada())
	    	  	{
					for (Session s : vSession.getOpenSessions()) 
				      {
				        if (s.isOpen()) 
				        {				        	
			        		if (!(vFachada.esJugadorPorId(s.getId())) && !(vFachada.esEspectadorPorId(s.getId())))
			        		{
			        			s.getBasicRemote().sendText("estadoPartida_{\"partidaIniciada\":\"false\"}");
			        		}
			        		
				        }
				        
				      }
	    	  	}
			}			
			catch (IOException ex) 
			{ 
				ex.printStackTrace();
			}
			if (vFachada.partidaFinalizada())
			{
				vFachada.liberarConexionesADronesYEspectadores();
				vFachada.inicializarPartida();
			}
			vSesiones.remove(vSession);
		}
		
		@OnMessage
		public void onMessage(Session vSession,String message) throws InterruptedException, IOException, ClassNotFoundException, SQLException, NamingException
		{				
			boolean esJugador;		
			if (vFachada.esJugadorPorId(vSession.getId()))
			{
				esJugador=true;
			}
			else
			{
				esJugador=false;
			}
			
			String[] parts = message.split("_");
			String accion = parts[0];
			String json="";
			
			if (accion.equals("Jugar"))
			{
				if (!vFachada.partidaIniciada())
				{
					vFachada.setPartidaCargada(true);
					json = "irAlApodo_{\"error\":\"false\",\"texto\":\"Ir a menu apodo\"}";
				}
				else
				{
					json = "irAlApodo_{\"error\":\"true\",\"texto\":\"Ya hay partida en juego\"}";
				}
				vSession.getBasicRemote().sendText(json);					
			}
			else if (accion.equals("ListarPartidas"))
			{
				if (!vFachada.partidaRecuperada() && !vFachada.partidaCargada())
				{
					try {
						json = vFachada.listarPartidas();
					} catch (Exception e) {
						json = "listarPartidas_{\"error\":\"true\",\"texto\":\"No se pudieron listar las partidas\"}";
					}
				}
				else
				{
					json = "listarPartidas_{\"error\":\"true\",\"texto\":\"Ya hay partida cargada o creada\"}";
				}
				vSession.getBasicRemote().sendText(json);	
			}			
			else if (accion.equals("IrAlLobby"))
			{									
				if (vFachada.partidaIniciada())
				{
					json = "validarIrAlLobby_{\"error\":\"true\",\"texto\":\"Ya hay partida en juego\"}";
					vSession.getBasicRemote().sendText(json);
				}
				else
				{
					json = vFachada.mostrarLobby(vSession.getId(), message);
					json = "participantes_"+json;				
					for (Session s : vSession.getOpenSessions()) 
				      {
				        if (s.isOpen()) 
				        {				        	
			        		if ((vFachada.esJugadorPorId(s.getId())) || (vFachada.esEspectadorPorId(s.getId())))
			        		{
			        			s.getBasicRemote().sendText(json);
			        		}
			        		
				        }
				        
				      }
				}
			}
			else if (!(vFachada.partidaIniciada()))
			{							
				switch (accion) 
				{
					case "IniciarPartida":
					{
						if (vFachada.puedeComenzar() && vFachada.esJugadorPorId(vSession.getId()))
						{
							vFachada.setPartidaInicidad(true);						
							vFachada.inicializarSesiones();
							String datosDronesIniciales = vFachada.obtenerDatosDrones();
							String coordenadasBase = vFachada.obtenerDatosBase();		
							
							String Partida= "{\"puentes\":[";
							
							Partida = Partida + vFachada.obtenerJsonPuentes();
							
							Partida = Partida.substring(0, Partida.length()-1);
							Partida = Partida + "],\"base\":"+coordenadasBase+","+datosDronesIniciales+"}";							
							json="iniciar_"+Partida;							
						}
						
					}
					break;					
					case "CargarPartida":
					{
						if (!vFachada.partidaRecuperada() && !vFachada.partidaCargada())
						{
							try {								
								vFachada.recuperarPartida(message);
								vFachada.setPartidaRecuperada(true);
								json = "recuperar_{\"error\":\"false\",\"texto\":\"Partida recuperada con exito\"}";
							} catch (Exception e) {
								json = "recuperar_{\"error\":\"true\",\"texto\":\"No se pudo recuperar la partida\"}";
							}	
						}
						else
						{
							json = "recuperar_{\"error\":\"true\",\"texto\":\"Ya hay partida cargada o cargada\"}";
						}
											
					}
					break;
					case "ElegirBando":
					{
						if (vFachada.puedeJugar(message))
						{
							json = vFachada.setInfoJugador(vSession.getId(), message);							
							json="participantesEnLobby_"+json;
						}						
					}
					break;					
				}
				try {  
					
					if (accion.equals("CargarPartida"))
					{		     
				        vSession.getBasicRemote().sendText(json);				        		
					}
					else
					{
						for (Session s : vSession.getOpenSessions()) 
					      {
					        if (s.isOpen()) 
					        {				        	
				        		if ((vFachada.esJugadorPorId(s.getId())) || (vFachada.esEspectadorPorId(s.getId())))
				        		{
				        			s.getBasicRemote().sendText(json);
				        		}
				        		
					        }
					        
					      } 
					}
					
					if (accion.equals("IniciarPartida"))
					{
						if (vFachada.partidaIniciada())
						{
							for (Session s : vSession.getOpenSessions()) 
						      {
						        if (s.isOpen()) 
						        {				        	
					        		if (!(vFachada.esJugadorPorId(s.getId())) && !(vFachada.esEspectadorPorId(s.getId())))
					        		{
					        			s.getBasicRemote().sendText("estadoPartida_{\"partidaIniciada\":\"true\"}");
					        		}
					        		
						        }
						        
						      }
						}
					}
				      
				}catch (IOException ex) { ex.printStackTrace();}
			}
			else if (!vFachada.partidaPausada())
			{			
				if (accion.equals("NuevaPartida"))
				{
					json="{\"noEnJuego\":\"true\"}";
					vSession.getBasicRemote().sendText(json);
				}
				if (esJugador)
				{
					
					if( accion.equals("81")) //LETRA Q, CAMBIAR DRONE
					{
						json=vFachada.cambiarDeDrone(vSession.getId());
						try {  

						      for (Session s : vSession.getOpenSessions()) 
						      {
						        if (s.isOpen()) 
						        {				        	
						        	if ((vFachada.esJugadorPorId(s.getId())) || (vFachada.esEspectadorPorId(s.getId())))
					        		{
						        		
					        			s.getBasicRemote().sendText(json);
					        		}
					        		
						        }
						        
						      } 
						} catch (IOException ex) { ex.printStackTrace();}
						
					}
					
					if (vFachada.estaVivoDrone(vSession.getId()))
					{
						switch (accion) 
						{
							case "38":  /* ARRIBA */
							{
								json = vFachada.avanzar(vSession.getId());
							}	
							break;							
							case "40":  /* ABAJO  */
							{
								json = vFachada.retroceder(vSession.getId());
							}
							break;
							case "37":  /* IZQUIERDA */
							{
								json = vFachada.rotarIzquierda(vSession.getId());
							}
							break;		
							case "39":  /* DERECHA */
							{
								json = vFachada.rotarDerecha(vSession.getId());
							}		
							break;
							case "32":
							{
								json = vFachada.disparar(vSession.getId());
							}		
							break;
							case "65": //Letra A
							{
								json = vFachada.subirNivel(vSession.getId());
							}
							break;
							case "90": //Letra Z
							{
								json = vFachada.bajarNivel(vSession.getId());
							}
							break;	
							case "83": //LETRA S
							{
								json=vFachada.tirarBomba(vSession.getId());
							}
							break;
							case "82": // LETRA R
							{
								json=vFachada.recargarMuniciones(vSession.getId());
							}
							break;
							case "66": // LETRA B
							{
								json=vFachada.recargarBomba(vSession.getId());
							}
							break;
							case "80": // LETRA P
							{
								json=vFachada.pausarPartida();
							}
							break;		
						}				
						
						try {  


						      for (Session s : vSession.getOpenSessions()) 
						      {
						        if (s.isOpen()) 
						        {				        	
						        	if ((vFachada.esJugadorPorId(s.getId())) || (vFachada.esEspectadorPorId(s.getId())))
					        		{
						        		
					        			s.getBasicRemote().sendText(json);
					        		}
					        		
						        }
						        
						      } 
						      
						      if (accion.equals("32")||accion.equals("83"))
								{
						    	  	if (!vFachada.partidaIniciada())
						    	  	{
										for (Session s : vSession.getOpenSessions()) 
									      {
									        if (s.isOpen()) 
									        {				        	
								        		if (!(vFachada.esJugadorPorId(s.getId())) && !(vFachada.esEspectadorPorId(s.getId())))
								        		{
								        			s.getBasicRemote().sendText("estadoPartida_{\"partidaIniciada\":\"false\"}");
								        		}
								        		
									        }
									        
									      }
						    	  	}
								}
						      
						      
						} catch (IOException ex) { ex.printStackTrace();}						
						
						if (message.equals("32"))
						{
							
							detenerDisparo one = new detenerDisparo(vSession,json, this.vFachada);
							one.start();
							

						}
						
					}
				}
			}
			else
			{
				
				switch (accion) 
				{
					case "VolverAMenuPausa":
					{	
						vFachada.setRespaldoEnCurso(false);
					}
					break;
					case "IngresarNombrePartida":
					{	
						if (!vFachada.respaldoEnCurso())				
						{
							if(vFachada.esJugadorPorId(vSession.getId()))
							{
								vFachada.setRespaldoEnCurso(true);
								json = "validarIrAGuardar_{\"error\":\"false\"}";
							}
							else
							{
								json = "validarIrAGuardar_{\"error\":\"true\",\"texto\":\"Solo pueden guardar los jugadores\"}";
							}
							
						}
						else
						{
							json = "validarIrAGuardar_{\"error\":\"true\",\"texto\":\"Ya hay alguien guardando\"}";
						}
					}
					break;
					case "Guardar": 
					{						
						try {
							vFachada.guardarPartida(message);							
							json = "guardar_{\"error\":\"false\",\"texto\":\"Partida guardada con éxito\"}";
						} catch (Exception e) {
							e.printStackTrace();
							json = "guardar_{\"error\":\"true\",\"texto\":\"Error al guardar la partida\"}";
						}					
						vFachada.setRespaldoEnCurso(false);
					}
					break;
					case "Reanudar": 
					{								
						if(vFachada.esJugadorPorId(vSession.getId()))
						{
							if (!vFachada.respaldoEnCurso())
							{
								json = vFachada.despausarPartida();
							}
							else
							{
								json = "validarIrAGuardar_{\"error\":\"true\",\"texto\":\"Ya hay alguien guardando\"}";
							}
						}
						else
						{
							json = "validarIrAGuardar_{\"error\":\"true\",\"texto\":\"Solo pueden reanudar los jugadores\"}";
						}	
					}
					break;
					case "Abandonar": 
					{							
						if (vFachada.esEspectadorPorId(vSession.getId()))
							vFachada.liberarEspectadorId(vSession.getId());
						else if (vFachada.esJugadorPorId(vSession.getId()))
						{
							vFachada.destruirDrone(vSession.getId());							
						
							boolean partidaFinalizada = vFachada.partidaFinalizada();
							boolean unJugador = false;
							if(vFachada.unJugador(vSession.getId()))
							{
								unJugador = true;
							}			
							
							json="abandonoPartida_{\"finalizarPartida\":\""+partidaFinalizada+"\",\"rol\":\"";
							int vRol = vFachada.getRolPorId(vSession.getId());
							switch (vRol)
							{
								case 0:
								{		
									json=json+"T1\",\"destruido\":\"true\", \"unJugador\":\""+unJugador+"\"}";
								}
								break;
								case 1:
								{
									json=json+"T2\",\"destruido\":\"true\", \"unJugador\":\""+unJugador+"\"}";
								}
								break;
								case 2:
								{
									json=json+"A1\",\"destruido\":\"true\", \"unJugador\":\""+unJugador+"\"}";
								}
								break;
								case 3:
								{
									json=json+"A2\",\"destruido\":\"true\", \"unJugador\":\""+unJugador+"\"}";
								}
								break;
							}
							if (partidaFinalizada)
							{
								vFachada.setPartidaInicidad(false);
								json = json.substring(0, json.length()-1);
								json = json + "," + vFachada.obtenerSobrevivientes();
								
								if (vFachada.bandoGanador().equals("Aereo"))
								{
									json=json+",\"bandoGanador\":\"aereo\"}";
								}
								else
								{
									json=json+",\"bandoGanador\":\"terrestre\"}";	
								}
							}
						}
					}
					break;
				
				}
				
				try {  
					
					if (accion.equals("IngresarNombrePartida")||accion.equals("Guardar")||accion.equals("Reanudar"))
					{
						if(accion.equals("Reanudar"))
						{	
							if(vFachada.esJugadorPorId(vSession.getId()))
							{
								if (!vFachada.respaldoEnCurso())
								{
									for (Session s : vSession.getOpenSessions()) 
								    {
								        if (s.isOpen()) 
								        {				        	
							        		if ((vFachada.esJugadorPorId(s.getId())) || (vFachada.esEspectadorPorId(s.getId())))
							        		{
							        			s.getBasicRemote().sendText(json);
							        		}
							        		
								        }
								        
								     } 
								}
								else
								{
									vSession.getBasicRemote().sendText(json);
								}
							}	
							else
							{
								//json = "validarIrAGuardar_{\"error\":\"true\",\"texto\":\"Ya hay alguien guardando\"}";
								vSession.getBasicRemote().sendText(json);
							}
						}
						else
						{
							vSession.getBasicRemote().sendText(json);
						}
					}
					else
					{
					      for (Session s : vSession.getOpenSessions()) 
					      {
					        if (s.isOpen()) 
					        {				        	
				        		if ((vFachada.esJugadorPorId(s.getId())) || (vFachada.esEspectadorPorId(s.getId())))
				        		{
				        			s.getBasicRemote().sendText(json);
				        		}
				        		
					        }
					        
					      } 
						}
					
					if (accion.equals("Abandonar"))
					{
			    	  	if (!vFachada.partidaIniciada())
			    	  	{
							for (Session s : vSession.getOpenSessions()) 
						      {
						        if (s.isOpen()) 
						        {				        	
					        		if (!(vFachada.esJugadorPorId(s.getId())) && !(vFachada.esEspectadorPorId(s.getId())))
					        		{
					        			s.getBasicRemote().sendText("estadoPartida_{\"partidaIniciada\":\"false\"}");
					        		}
					        		
						        }
						        
						      }
			    	  	}
					}
				}catch (IOException ex) { ex.printStackTrace();}
			}
			
			if (vFachada.partidaFinalizada())
			{				
				vFachada.liberarConexionesADronesYEspectadores();
				vFachada.inicializarPartida();
			}
		}

		@OnError
		public void onError(Throwable e)
		{
			System.out.println("********   ERROR DE CONEXION  lo corrige el onClose  *************");
			//e.printStackTrace();
		}
}
