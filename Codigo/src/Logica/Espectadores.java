package Logica;
import java.util.ArrayList;

public class Espectadores 
{
	private ArrayList<Espectador> vEspectadores;
		
	public Espectadores()
	{
			this.vEspectadores=new ArrayList<Espectador>();
	}
	
	public int getLargo ()
	{
		return vEspectadores.size();
	}
	
	public Espectador getEspectador (int i)
	{
		return vEspectadores.get(i);
	}
	
	public void addEspectador (Espectador pEspectador)
	{
		vEspectadores.add(pEspectador);
	}
	
	public void eliminarEspectador (String iD)
	{
		int i=0;
		while (i<vEspectadores.size() && iD  != vEspectadores.get(i).getId())
		{
			i++;
		}		
		if (i<vEspectadores.size())
		{
			vEspectadores.remove(i);
		}
	}

	public boolean esEspectador (String iD)
	{
		boolean es = false;
		int i=0;
		while (i<vEspectadores.size() && !es)
		{
			if(iD.equals(vEspectadores.get(i).getId()))
			{
				es = true;
			}
			i++;
		}
		return es;
	}

	public void liberarTodosLosEspectadores() 
	{
		vEspectadores.clear();
	}
}
