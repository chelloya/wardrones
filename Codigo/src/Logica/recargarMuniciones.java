package Logica;

public class recargarMuniciones extends Thread
{
	private String vId;
	
	public recargarMuniciones (String pId)
	{
		this.vId = pId;
	}
	
	@Override
	public void run() 
	{	
		try {
			Thread.sleep(3000);
			Fachada vFachada = Fachada.getInstancia();
			vFachada.recargarMunicionesDroneI(vId);
		} 
		catch (InterruptedException e) 
		{			
			e.printStackTrace();
		}
		
	}
	
}