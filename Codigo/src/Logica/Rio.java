package Logica;

public class Rio 
{
	private Puente puentes[] = new Puente[13];
	
	public Rio()
	{
		int pasajes =0;
		int aguas = 0;
		for(int i=0;i<13; i++)
		{
			int j= (int)(Math.random()*3);
			
			float x1 = 526+(j*20);
			float x2 = 586+(j*20);
			float x3 = 586+(j*20);
			float x4 = 526+(j*20);
			float y1 = 0 + (i*60);
			float y2 = 0 + (i*60);
			float y3 = 60 + (i*60);
			float y4 = 60 + (i*60);
			
			boolean esPuente = true;
			if (pasajes<8 && aguas <8)
			{
				if(0 == (int)(Math.random()*2))
				{
					esPuente = false;
					aguas++;
				}
				else
				{
					pasajes++;
				}
			}
			else
			{
				if(pasajes==8)
				{
					esPuente = false;
				}
			}
			
			puentes[i]= new Puente(x1,y1,x2,y2,x3,y3,x4,y4,esPuente);
		}
	}

	public Puente[] getPuentes() {
		return puentes;
	}
	
	public void setPuentes(Puente pPuentes[])
	{
		for (int i=0;i<13;i++)
		{		
			puentes[i].setX1(pPuentes[i].getX1());
			puentes[i].setY1(pPuentes[i].getY1());
			puentes[i].setX2(pPuentes[i].getX2());
			puentes[i].setY2(pPuentes[i].getY2());
			puentes[i].setX3(pPuentes[i].getX3());
			puentes[i].setY3(pPuentes[i].getY3());
			puentes[i].setX4(pPuentes[i].getX4());
			puentes[i].setY4(pPuentes[i].getY4());
			puentes[i].setEsPuente(pPuentes[i].isEsPuente());
		}
	}

	public String obtenerJsonPuentes() 
	{
		String Partida = "";
		for (int i=0;i<13;i++)
		{
			Partida = Partida + "{";
			Partida = Partida + "\"x1\":\"";
			Partida = Partida + getPuentes()[i].getX1();
			Partida = Partida + "\",\"y1\":\"";
			Partida = Partida + getPuentes()[i].getY1();
			Partida = Partida + "\",";
			Partida = Partida + "\"x2\":\"";
			Partida = Partida + getPuentes()[i].getX2();
			Partida = Partida + "\",\"y2\":\"";
			Partida = Partida + getPuentes()[i].getY2();
			Partida = Partida + "\",";
			Partida = Partida + "\"x3\":\"";
			Partida = Partida + getPuentes()[i].getX3();
			Partida = Partida + "\",\"y3\":\"";
			Partida = Partida + getPuentes()[i].getY3();
			Partida = Partida + "\",";
			Partida = Partida + "\"x4\":\"";
			Partida = Partida + getPuentes()[i].getX4();
			Partida = Partida + "\",\"y4\":\"";
			Partida = Partida + getPuentes()[i].getY4();
			Partida = Partida + "\",";
			Partida = Partida + "\"esPuente\":\"";
			Partida = Partida + getPuentes()[i].isEsPuente();
			Partida = Partida + "\"},";
			
		}
		return Partida;
	}	
}
