package Logica;

public class Fachada 
{
	private static Fachada instancia;
	private Partida vPartida;
	
	
	//singleton
	public static synchronized Fachada getInstancia() 
	{
		if(instancia==null)
		{
			instancia=new Fachada();			
		}
		return instancia;		
	}
	
	private Fachada() 
	{
		this.vPartida = new Partida();
	}
	
	public void inicializarPartida() 
	{
		this.vPartida = new Partida();
	}
	
	public boolean partidaIniciada()
	{
		return vPartida.isPartidaIniciada();
	}
	
	public boolean partidaRecuperada()
	{
		return vPartida.isPartidaRecuperada();
	}
	
	public void setPartidaInicidad(boolean pPartidaIniciada)
	{
		vPartida.setPartidaIniciada(pPartidaIniciada);
	}
	
	public void setPartidaRecuperada(boolean pPartidaRecuperada)
	{
		vPartida.setPartidaRecuperada(pPartidaRecuperada);
	}
	
	public boolean partidaCargada()
	{
		return vPartida.isPartidaCargada();
	}
	
	public void setPartidaCargada(boolean pPartidaCargada)
	{
		vPartida.setPartidaCargada(pPartidaCargada);
	}

	public boolean partidaPausada()
	{
		return vPartida.isPartidaPausada();
	}
	
	public void setPartidaPausada(boolean pPartidaPausada)
	{
		vPartida.setPartidaPausada(pPartidaPausada);
	}
	
	public boolean respaldoEnCurso()
	{
		return vPartida.isRespaldoEnCurso();
	}
	
	public void setRespaldoEnCurso(boolean pRespaldoEnCurso)
	{
		vPartida.setRespaldoEnCurso(pRespaldoEnCurso);
	}
	
	public String rotarIzquierda (String iDSession)
	{
		return vPartida.rotarIzquierda(iDSession);
	}
	
	public String rotarDerecha (String iDSession)
	{
		return vPartida.rotarDerecha(iDSession);
	}
	
	public String avanzar (String iD)
	{
		return vPartida.avanzar(iD);
	}
	
	public String retroceder(String iDSession)
	{
		return vPartida.retroceder(iDSession);
	}
	
	public String disparar(String iDSession)
	{
		return vPartida.disparar(iDSession);
	}
	
	public void setIdSessionDroneI (int pRol, String iD)
	{
		vPartida.setIdSessionDroneI(pRol, iD);
	}
	
	public Drone obtenerDronPorRol (int pRol)
	{
		return vPartida.obtenerDronPorRol (pRol);
	}
	
	public String getRolDrone (int pRol)
	{
		return vPartida.getRolDrone(pRol);
	}
	
	public Mapa getMapaPartida ()
	{
		return vPartida.getMapa();
	}
	
	public String subirNivel (String iD)
	{
		return vPartida.subirNivel(iD);
	}
	
	public String bajarNivel (String iD)
	{
		return vPartida.bajarNivel(iD);
	}
	
	public boolean esJugadorPorId(String iD)
	{
		return vPartida.esJugadorPorId(iD);
	}
	
	public boolean esEspectadorPorId(String iD)
	{
		return vPartida.esEspectadorPorId(iD);
	}
	
	public String setInfoJugador (String iD, String infoJugador)
	{
		return vPartida.setInfoJugador(iD, infoJugador);
	}
	
	public String mostrarLobby (String iD, String infoJugador)
	{
		return vPartida.mostrarLobby(iD, infoJugador);
	}
	
	public String quitarDelLobby()
	{
		return vPartida.quitarDelLobby();
	}
	
	public String obtenerDatosDrones()
	{
		return vPartida.obtenerDatosDrones();
	}
	
	public String obtenerDatosBase ()
	{
		return vPartida.obtenerDatosBase();
	}
	
	public String cambiarDeDrone(String iD)
	{
		return vPartida.cambiarDeDrone(iD);
	}
	
	public String tirarBomba(String iD)
	{
		return vPartida.tirarBomba(iD);
	}
	
	public boolean estaVivoDrone(String iD)
	{
		return vPartida.estaVivoDrone(iD);
	}
	
	public boolean estaVivoDrone(int pRol)
	{
		return vPartida.estaVivoDrone(pRol);
	}
	
	public void recargarMunicionesDroneI(String Id)
	{
		vPartida.recargarMunicionesDroneI(Id);
	}
	
	public String recargarMuniciones(String pId)
	{
		String json="";
		int vRol= vPartida.getRolPorId(pId);
		String stringRol = vPartida.getRolDrone(vRol);
		if (vPartida.canionDisponible(vRol))
		{
			recargarMuniciones vRm = new recargarMuniciones(pId);
			vRm.start();
			if (vRol==0||vRol==1)
			{
				json= "recargar_{\"rol\":\""+stringRol+"\",\"municiones\":\"5\"}";
			}
			else
			{
				json= "recargar_{\"rol\":\""+stringRol+"\",\"municiones\":\"3\"}";
			}
		}
		return json;
	}
	
	public String recargarBomba(String Id)
	{
		return vPartida.recgargarBomba(Id);
	}
	
	public void guardarPartida(String message) throws Exception
	{
		vPartida.guardarPartida(message);
	}
	
	public String listarPartidas () throws Exception
	{
		return vPartida.listarPartidas();
	}
	
	public void recuperarPartida(String message) throws Exception
	{
		vPartida.recuperarPartida(message);
	}
	
	public String pausarPartida ()
	{
		setPartidaPausada(true);
		return "pausar_{\"pausar\":\"true\"}";
	}
	
	public String despausarPartida ()
	{
		setPartidaPausada(false);
		return "pausar_{\"pausar\":\"false\"}";
	}
	
	public boolean puedeComenzar()
	{
		return vPartida.puedeComenzar();
	}
	
	public boolean puedeJugar(String message)
	{
		return vPartida.puedeJugar(message);
	}
	
	public int getRolPorId (String pId)
	{
		return vPartida.getRolPorId(pId);
	}
	
	public void destruirDrone (String pId)
	{
		vPartida.destruirDrone(pId);
	}
	
	public boolean partidaFinalizada() 
	{	
		return vPartida.partidaFinalizada();
	}
	
	public boolean unJugador (String pId)
	{
		return vPartida.unJugador(pId);
	}	

	public void inicializarSesiones()
	{
		vPartida.inicializarSesiones();
	}

	public String obtenerJsonPuentes() 
	{
		return vPartida.obtenerJsonPuentes();
	}

	public void liberarConexionesADronesYEspectadores()
	{
		vPartida.liberarConexionesADronesYEspectadores();
	}
	
	public String obtenerSobrevivientes()
	{
		return vPartida.obtenerSobrevivientes();
	}
	
	public String bandoGanador()
	{
		return vPartida.bandoGanador();
	}
	
	public void liberarDronId (String iD)
	{
		vPartida.liberarDronId(iD);
	}
	public void liberarEspectadorId (String iD)
	{
		vPartida.liberarEspectadorId(iD);
	}
}
