package Logica;

public class DroneAereo extends Drone{

	private boolean bomba;
	private int motor;
	
	public DroneAereo(float x, float y, int z, float ang, boolean cam, boolean can, int mun, String apd, String idS, boolean bom, int mot) 
	{
		super(x, y, z, ang, cam, can, mun, apd, idS);
		this.bomba=bom;
		this.motor=mot;
	}
	
	public DroneAereo()
	{
		super (0,true,true,3,"","");
		this.bomba=true;
		this.motor=2;
	}

	public boolean isBomba() {
		return bomba;
	}

	public void setBomba(boolean bomba) {
		this.bomba = bomba;
	}

	public int getMotor() {
		return motor;
	}

	public void setMotor(int motor) {
		this.motor = motor;
	}
	
	public void subirNivel() 
	{
		if (coordZ<2)
		{
			coordZ++;
		}
		
	}
	
	public void bajarNivel() 
	{
		if (coordZ>0)
		{
			coordZ--;
		}
		
	}
	
	public boolean estaVivo()
	{
		boolean estaVivo=true;
		if (this.motor==0)
			estaVivo= false;
		
		return estaVivo;
	}
	
	public void ubicarDroneAereo()
	{
		 coordX = 26 + ((float)(Math.random()*(237)));
		 coordY = 26 + ((float) (Math.random()*(728)));
		 coordZ = (int)(Math.random()*3);
	}
	
	public String obtenerDatosDrone()
	{		
		float x = getCoordX();
		float y = getCoordY();
		int z = getCoordZ();		
		float angulo= getAngulo();
		boolean camara = isCamara();
		boolean canion = isCanion();
		int municiones = getMuniciones();
		boolean bomba = isBomba();
		int motor = getMotor();
		String json = ",\"x\": \""+x+"\",\"y\":\""+y+"\",\"z\":\""+z+"\",\"angulo\":\""+angulo+"\",\"camara\":\""+camara+"\",\"canion\":\""+canion+"\",\"municiones\":\""+municiones+"\",\"bomba\":\""+bomba+"\",\"motor\":\""+motor+"\",\"destruido\":\""+(!(this.estaVivo()))+"\"}";
		return json;
	}
	
	public String recibirImpacto()
	{
		String json = "";
		int impacto = (int)(Math.random()*4);
		boolean pudoImpactar = false;
		while (!pudoImpactar)
		{
			switch(impacto)
			{
			case 0:		//IMPACTA LA CAMARA
			{
				if (camara)
				{
					pudoImpactar=true;
					camara=false;
					json=",\"camara\": \"false\", ";
				}
				
			}
			break;
			case 1:		//IMPACTA EL CANION
			{
				if (canion)
				{
					pudoImpactar=true;
					canion=false;
					json=",\"canion\": \"false\", ";
				}
				
			}
			break;
			case 2:		//IMPACTA LA BOMBA
			{
				if (bomba)
				{
					pudoImpactar=true;
					motor=0;
					json=",\"explotoBomba\": \"true\", ";
				}				
			}
			break;
			case 3:		//IMPACTA EL MOTOR
			{
				if (motor>0)
				{
					pudoImpactar=true;
					motor--;
					json= ",\"motor\": \""+motor+"\", ";
				}				
			}
			break;
			}
			if (!pudoImpactar)
			{
				impacto = (int)(Math.random()*4);
			}
		}		
		return json;
	}
	
	public void actualizarBomba(boolean pBomba)
	{
		bomba = pBomba;
	}
	
	public boolean estaEnLaRecta(float x, float y, int z, float pAngulo)
	{
		boolean impacta = false;
		
		if(this.coordZ == z)
		{
			float catX2 = (coordX-x)*(coordX-x);
			float catY2 = (coordY-y)*(coordY-y);
			float hyp = (float)Math.sqrt(catX2 +catY2);			
			
			if(hyp<150)
			{				
				if(((0 < pAngulo && pAngulo < 180)&& coordY > y) || ((0 > pAngulo && pAngulo > -180)&& coordY < y) || ((pAngulo == 0)&& coordX > x)|| ((pAngulo == -180) && coordX < x) || ((pAngulo == 180) && coordX < x) || ((180 < pAngulo && pAngulo < 360)&& coordY < y) || ((-360 < pAngulo && pAngulo < -180)&& coordY > y))
				{
					float vCoordX = this.coordX;
					float vCoordY = this.coordY;
					float pendiente = (float)(Math.tan(Math.toRadians(pAngulo)));
					
					
					float xSub1 = vCoordX - 25;
					float xSub2 = vCoordX + 25;
					float ySub1 = vCoordY - 25;
					float ySub2 = vCoordY + 25;
					
					float yaux = (pendiente *(xSub1-x))+y;
					if (ySub2>=yaux &&  yaux>=ySub1)
					{
						impacta = true;
					}
					
					yaux = (pendiente *(xSub2-x))+y;
					if (!impacta && ySub2>=yaux &&  yaux>=ySub1)
					{
						impacta = true;
					}
					
					float xaux = ((ySub1-y)/pendiente)+x;
					if (!impacta && xSub2 >= xaux && xaux>=xSub1)
					{
						impacta=true;
					}
					
					xaux = ((ySub2-y)/pendiente)+x;
					if (!impacta && xSub2 >= xaux && xaux>=xSub1)
					{
						impacta=true;
					}
				}
			}	
		}
		return impacta;
	}
	
	public void avanzar (Mapa pMapa)
	{	
		if (puedeAvanzar(pMapa))
		{
			switch (motor)
			{
				case 0:
				{
					coordX+=(Math.cos(Math.toRadians(angulo)))*0;
					coordY+=(Math.sin(Math.toRadians(angulo)))*0;	
				}
				break;
				case 1:
				{
					coordX+=(Math.cos(Math.toRadians(angulo)))*2;
					coordY+=(Math.sin(Math.toRadians(angulo)))*2;	
				}
				break;
				case 2:
				{
					coordX+=(Math.cos(Math.toRadians(angulo)))*4;
					coordY+=(Math.sin(Math.toRadians(angulo)))*4;	
				}
				break;
			}	
		}
	}
	
	public void retroceder (Mapa pMapa)
	{
		if (puedeRetroceder(pMapa))
		{
			switch (motor)
			{
				case 0:
				{
					coordX-=(Math.cos(Math.toRadians(angulo)))*0;
					coordY-=(Math.sin(Math.toRadians(angulo)))*0;	
				}
				break;
				case 1:
				{
					coordX-=(Math.cos(Math.toRadians(angulo)))*2;
					coordY-=(Math.sin(Math.toRadians(angulo)))*2;	
				}
				break;
				case 2:
				{
					coordX-=(Math.cos(Math.toRadians(angulo)))*4;
					coordY-=(Math.sin(Math.toRadians(angulo)))*4;	
				}
				break;
			}	
		}
	}

	public void destruirDrone() {
		motor=0;
	}
}
