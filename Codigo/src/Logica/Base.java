package Logica;

import ValueObjects.VOBase;

public class Base {
	private float coordX1;
	private float coordX2;
	private float coordY1;
	private float coordY2;
	private float coordX3;
	private float coordX4;
	private float coordY3;
	private float coordY4;
	private int vidaPolvorin;
	private int vidaPista;
	
	public Base(float coordX1, float coordX2, float coordY1, float coordY2, float coordX3, float coordX4, float coordY3, float coordY4,int vidaPolvorin, int vidaPista) 
	{
		this.coordX1 = coordX1;
		this.coordX2 = coordX2;
		this.coordY1 = coordY1;
		this.coordY2 = coordY2;
		this.coordX3 = coordX3;
		this.coordX4 = coordX4;
		this.coordY3 = coordY3;
		this.coordY4 = coordY4;
		this.vidaPolvorin = vidaPolvorin;
		this.vidaPista = vidaPista;
	}
	
	public Base() 
	{
		ubicarBase();
		this.vidaPolvorin = 2;
		this.vidaPista = 10;
	}

	public float getCoordX1() {
		return coordX1;
	}

	public void setCoordX1(float coordX1) {
		this.coordX1 = coordX1;
	}
	
	public float getCoordX2() {
		return coordX2;
	}

	public void setCoordX2(float coordX2) {
		this.coordX2 = coordX2;
	}

	public float getCoordY1() {
		return coordY1;
	}

	public void setCoordY1(float coordY1) {
		this.coordY1 = coordY1;
	}
	
	public float getCoordY2() {
		return coordY2;
	}

	public void setCoordY2(float coordY2) {
		this.coordY2 = coordY2;
	}

	public float getCoordX3() {
		return coordX3;
	}

	public void setCoordX3(float coordX3) {
		this.coordX3 = coordX3;
	}

	public float getCoordX4() {
		return coordX4;
	}

	public void setCoordX4(float coordX4) {
		this.coordX4 = coordX4;
	}

	public float getCoordY3() {
		return coordY3;
	}

	public void setCoordY3(float coordY3) {
		this.coordY3 = coordY3;
	}

	public float getCoordY4() {
		return coordY4;
	}

	public void setCoordY4(float coordY4) {
		this.coordY4 = coordY4;
	}

	public int getVidaPolvorin() {
		return vidaPolvorin;
	}

	public void setVidaPolvorin(int vidaPolvorin) {
		this.vidaPolvorin = vidaPolvorin;
	}

	public int getVidaPista() {
		return vidaPista;
	}

	public void setVidaPista(int vidaPista) {
		this.vidaPista = vidaPista;
	}
	
	public void ubicarBase()
	{
		coordX1=(float)(Math.random()*(60));
		coordY1=(float)(Math.random()*(781-58));
		coordX2=coordX1+58;
		coordY2 = coordY1;
		coordX3=coordX2;
		coordY3=coordY1+39;
		coordX4=coordX1;
		coordY4 = coordY3;
	}
	
	public String obtenerDatosBase()
	{
		String json="{";
		
		float x1 = getCoordX1();
		float y1 = getCoordY1();
		float x2 = getCoordX2();
		float y2 = getCoordY2();
		float x3 = getCoordX3();
		float y3 = getCoordY3();
		float x4 = getCoordX4();
		float y4 = getCoordY4();
		
		json = json + "\"x1\": \""+x1+"\",\"x2\":\""+x2+"\",\"y1\":\""+y1+"\",\"y2\":\""+y2+"\""+",\"x3\": \""+x3+"\",\"x4\":\""+x4+"\",\"y3\":\""+y3+"\",\"y4\":\""+y4+"\",\"vidaPolvorin\":\""+vidaPolvorin+"\",\"vidaPista\":\""+vidaPista+"\"}";
		return json;
	}
	
	public String recibirImpacto()
	{
		String json="";
		int impacto = (int)(Math.random()*2);
		if (impacto == 0)
		{
			vidaPolvorin--;
			json= ",\"vidaPolvorin\": \""+vidaPolvorin+"\",";
			
		}
		else
		{
			vidaPista--;
			json= ",\"vidaPista\": \""+vidaPista+"\",";
		}
		return json;
	}
	
	public boolean baseDestruida()
	{
		if (vidaPolvorin==0||vidaPista==0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public boolean estaEnLaRecta (float x, float y, int z, float pAngulo)
	{
		boolean impacta = false;
		float pendiente = (float)(Math.tan(Math.toRadians(pAngulo)));	
		
		if (z==0)
		{
			float coordX = coordX1+((coordX2-coordX1)/2);
			float coordY = coordY1+((coordY4-coordY1)/2);
			float catX2 = (coordX-x)*(coordX-x);
			float catY2 = (coordY-y)*(coordY-y);
			float hyp = (float)Math.sqrt(catX2 +catY2);
			if(hyp<150)
			{
				if(((0 < pAngulo && pAngulo < 180)&& coordY > y) || ((0 > pAngulo && pAngulo > -180)&& coordY < y) || ((pAngulo == 0)&& coordX > x)|| ((pAngulo == -180) && coordX < x) || ((pAngulo == 180) && coordX < x) || ((180 < pAngulo && pAngulo < 360)&& coordY < y) || ((-360 < pAngulo && pAngulo < -180)&& coordY > y))
				{
					float xSub1 = this.coordX1;
					float xSub2 = this.coordX3;
					float ySub1 = this.coordY1;
					float ySub2 = this.coordY3;
					
					float yaux = (pendiente *(xSub1-x))+y;
					if (ySub2>=yaux &&  yaux>=ySub1)
					{
						impacta = true;
					}
					
					yaux = (pendiente *(xSub2-x))+y;
					if (!impacta && ySub2>=yaux &&  yaux>=ySub1)
					{
						impacta = true;
					}
					
					float xaux = ((ySub1-y)/pendiente)+x;
					if (!impacta && xSub2 >= xaux && xaux>=xSub1)
					{
						impacta=true;
					}
					
					xaux = ((ySub2-y)/pendiente)+x;
					if (!impacta && xSub2 >= xaux && xaux>=xSub1)
					{
						impacta=true;
					}
				}
			}
		}
		
		return impacta;
	}
	
	public void recibirImpactoBomba()
	{
		vidaPolvorin=0;
		vidaPista=0;
	}
	
	public boolean impactoBomba(float x, float y)
	{
		boolean impacto=false;		
		if (coordX1<=x && x<=coordX2 && coordY1<=y && y<=coordY3)
			impacto=true;		
		return impacto;
	}
	
	public boolean estaEnLaBaseDroneI(float x, float y, int z)
	{
		boolean esta=false;		
		float coordYaux = coordY1+((coordY3-coordY1)/2); 
		if (coordX1<=x && x<=coordX2 && coordYaux<=y && y<=coordY3 && z==0)
		{
			esta=true;
		}
		return esta;
	}
	
	public void generarBase (VOBase pBase)
	{
		this.coordX1 = pBase.getCoordX1();
		this.coordY1 = pBase.getCoordY1();
		coordX2=coordX1+30;
		coordY2 = coordY1;
		coordX3=coordX2;
		coordY3=coordY1+24;
		coordX4=coordX1;
		coordY4 = coordY3;
		this.vidaPista = pBase.getVidaPista();
		this.vidaPolvorin = pBase.getVidaPolvorin();
	}
	
}