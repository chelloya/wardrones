package Logica;

public abstract class Drone {
	protected float coordX;
	protected float coordY;
	protected int coordZ;
	protected float angulo;
	protected boolean camara;
	protected boolean canion;
	protected int municiones;
	protected String apodo;
	protected String id;	
	protected boolean activo;
	
	
	public Drone(float x, float y, int z, float ang, boolean cam, boolean can, int mun, String apd, String idS)
	{
		this.coordX=x;
		this.coordY=y;
		this.coordZ=z;
		this.angulo=ang;
		this.camara=cam;
		this.canion=can;
		this.municiones=mun;
		this.apodo=apd;
		this.id=idS;		
		this.activo=false;
	}
	
	public Drone(float ang, boolean cam, boolean can, int mun, String apd, String idS)
	{
		this.angulo=ang;
		this.camara=cam;
		this.canion=can;
		this.municiones=mun;
		this.apodo=apd;
		this.id=idS;		
		this.activo=false;
	}

	public float getCoordX() {
		return coordX;
	}

	public void setCoordX(float coordX) {
		this.coordX = coordX;
	}

	public float getCoordY() {
		return coordY;
	}

	public void setCoordY(float coordY) {
		this.coordY = coordY;
	}

	public int getCoordZ() {
		return coordZ;
	}

	public void setCoordZ(int coordZ) {
		this.coordZ = coordZ;
	}
	
	public float getAngulo() {
		return angulo;
	}
	
	public void setAngulo(float ang) {
		this.angulo = ang;
	}

	public boolean isCamara() {
		return camara;
	}

	public void setCamara(boolean camara) {
		this.camara = camara;
	}

	public boolean isCanion() {
		return canion;
	}

	public void setCanion(boolean canion) {
		this.canion = canion;
	}

	public int getMuniciones() {
		return municiones;
	}

	public void setMuniciones(int municiones) {
		this.municiones = municiones;
	}
	
	public int actualizarMuniciones()
	{
		this.municiones--;
		return municiones;
	}

	public String getApodo() {
		return apodo;
	}

	public void setApodo(String apodo) {
		this.apodo = apodo;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}	
	
	public void setActivo() {
		this.activo=true;
	}
	
	public void setInactivo() {
		this.activo=false;
	}
	
	public boolean getActivo() {
		return activo;
	}	
	
	public void rotarIzquierda ()
	{
		angulo -= 4;
		if (angulo==-360)
			angulo=0;
	}
	
	public void rotarDerecha ()
	{
		angulo += 4;
		if (angulo==360)
			angulo=0;
	}
	
	public boolean puedeAvanzar (Mapa pMapa)
	{
		float ancho = pMapa.getAncho();
		float largo = pMapa.getLargo();
		Puente[] puentes = pMapa.getRio().getPuentes();
		
		double dx = Math.cos(Math.toRadians(angulo))*4;
		double dy = Math.sin(Math.toRadians(angulo))*4;
		
		double coordXAUX = coordX + Math.cos(Math.toRadians(angulo))*4;
		double coordYAUX = coordY + Math.sin(Math.toRadians(angulo))*4;
		
		boolean puedePasar=true;
		int i=0;
		
		if(this instanceof DroneTerrestre)
		{
			while (puedePasar && i<13)
			{
				if (!puentes[i].isEsPuente())
				{
					if ((puentes[i].getX1()<= coordXAUX && coordXAUX <= puentes[i].getX2()) && (puentes[i].getY1()<= coordYAUX && coordYAUX <= puentes[i].getY3()))
					{
						puedePasar = false;
					}
				}
				i++;			
			}
		}
		
		if (puedePasar && (coordX + dx > 0 ) && (coordX + dx < ancho) && (coordY + dy >0 ) && (coordY + dy < largo))
		{
			return true;
		}
		else
		{
			return false;
		}
		
	}
	
	public boolean puedeRetroceder (Mapa pMapa)
	{
		float ancho = pMapa.getAncho();
		float largo = pMapa.getLargo();
		Puente[] puentes = pMapa.getRio().getPuentes();
		
		double dx = Math.cos(Math.toRadians(angulo))*4;
		double dy = Math.sin(Math.toRadians(angulo))*4;
		
		double coordXAUX = coordX - Math.cos(Math.toRadians(angulo))*4;
		double coordYAUX = coordY - Math.sin(Math.toRadians(angulo))*4;
		
		boolean puedePasar=true;
		int i=0;
		
		if(this instanceof DroneTerrestre)
		{
			while (puedePasar && i<13)
			{
				if (!puentes[i].isEsPuente())
				{
					if ((puentes[i].getX1()<= coordXAUX && coordXAUX <= puentes[i].getX2()) && (puentes[i].getY1()<= coordYAUX && coordYAUX <= puentes[i].getY3()))
					{
						puedePasar = false;
					}
				}
				i++;			
			}
		}
		
		if (puedePasar && (coordX - dx > 0 ) && (coordX - dx < ancho) && (coordY - dy >0 ) && (coordY - dy < largo))
		{
			return true;
		}
		else
		{
			return false;
		}
		
	}
	
	public abstract boolean estaEnLaRecta(float x, float y, int z, float pAngulo);
	public abstract void subirNivel ();
	public abstract void bajarNivel ();
	public abstract boolean estaVivo();
	public abstract String obtenerDatosDrone();
	public abstract String recibirImpacto();
	public abstract void avanzar (Mapa pMapa);	
	public abstract void retroceder (Mapa pMapa);
	public abstract void destruirDrone();

}
