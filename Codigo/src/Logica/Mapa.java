package Logica;

import ValueObjects.VOBase;

public class Mapa 
{
	private Rio rio;
	private Base base;
	private int ancho = 1152;
	private int largo = 780;
	
	public Mapa(Rio rio, Base base, int ancho, int largo)
	{		
		this.rio = rio;
		this.base = base;
		this.ancho = ancho;
		this.largo = largo;
	}
	
	public Mapa()
	{
		this.base= new Base();
		this.rio = new Rio();
	}

	public Rio getRio() {
		return rio;
	}

	public void setRio(Rio rio) {
		this.rio = rio;
	}

	public Base getBase() {
		return base;
	}

	public void setBase(Base base) {
		this.base = base;
	}

	public int getAncho() {
		return ancho;
	}

	public void setAncho(int ancho) {
		this.ancho = ancho;
	}

	public int getLargo() {
		return largo;
	}

	public void setLargo(int largo) {
		this.largo = largo;
	}
	
	public String obtenerDatosBase ()
	{
		return this.base.obtenerDatosBase();
	}
	
	public String recibirImpactoBase()
	{
		return base.recibirImpacto();
	}
	
	public boolean baseDestruida()
	{
		return base.baseDestruida();
	}
	
	public boolean recibioImpactoBase(float x, float y, int z, float angulo)
	{
		return base.estaEnLaRecta(x,y,z,angulo);
	}
	
	public boolean impactoBombaBase(float x, float y)
	{
		return base.impactoBomba(x, y);
	}
	
	public void recibirImpactoBombaBase()
	{
		base.recibirImpactoBomba();
	}
	
	public boolean estaEnLaBaseDroneI(float x, float y, int z)
	{
		return base.estaEnLaBaseDroneI(x, y, z);
	}
	
	public Puente[] obtenerPasajes()
	{
		return rio.getPuentes();
	}
	
	public void generarBase (VOBase pBase)
	{
		base.generarBase(pBase);
	}
	
	public void setPuentes(Puente pPuentes[])
	{
		rio.setPuentes(pPuentes);
	}

	public String obtenerJsonPuentes()
	{
		return this.rio.obtenerJsonPuentes();
	}
}
