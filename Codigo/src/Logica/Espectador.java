package Logica;

public class Espectador 
{
	String id;
	String apodo;
	
	public Espectador(String id, String apodo) 
	{
		this.id = id;
		this.apodo = apodo;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getApodo() {
		return apodo;
	}
	public void setApodo(String apodo) {
		this.apodo = apodo;
	}
	

}
