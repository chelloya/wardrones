package ValueObjects;

public class VOBase {
	private float coordX1;
	private float coordY1;
	private int vidaPolvorin;
	private int vidaPista;
	
	public VOBase(float coordX1, float coordY1, int vidaPolvorin, int vidaPista) {		
		this.coordX1 = coordX1;
		this.coordY1 = coordY1;
		this.vidaPolvorin = vidaPolvorin;
		this.vidaPista = vidaPista;
	}
	
	public VOBase()
	{
		
	}

	public float getCoordX1() {
		return coordX1;
	}

	public void setCoordX1(float coordX1) {
		this.coordX1 = coordX1;
	}

	public float getCoordY1() {
		return coordY1;
	}

	public void setCoordY1(float coordY1) {
		this.coordY1 = coordY1;
	}

	public int getVidaPolvorin() {
		return vidaPolvorin;
	}

	public void setVidaPolvorin(int vidaPolvorin) {
		this.vidaPolvorin = vidaPolvorin;
	}

	public int getVidaPista() {
		return vidaPista;
	}

	public void setVidaPista(int vidaPista) {
		this.vidaPista = vidaPista;
	}	
	
}
