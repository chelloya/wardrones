package ValueObjects;

public class VODrone {

	private float coordX;
	private float coordY;
	private int coordZ;
	private float angulo;
	private boolean camara;
	private boolean canion;
	private int municiones;
	private boolean bomba;
	private int motor;
	private int blindaje;
	
	public VODrone(float coordX, float coordY, int coordZ, float angulo, boolean camara, boolean canion, int municiones,
			boolean bomba, int motor, int blindaje) {		
		this.coordX = coordX;
		this.coordY = coordY;
		this.coordZ = coordZ;
		this.angulo = angulo;
		this.camara = camara;
		this.canion = canion;
		this.municiones = municiones;
		this.bomba = bomba;
		this.motor = motor;
		this.blindaje = blindaje;
	}

	public float getCoordX() {
		return coordX;
	}

	public void setCoordX(float coordX) {
		this.coordX = coordX;
	}

	public float getCoordY() {
		return coordY;
	}

	public void setCoordY(float coordY) {
		this.coordY = coordY;
	}

	public int getCoordZ() {
		return coordZ;
	}

	public void setCoordZ(int coordZ) {
		this.coordZ = coordZ;
	}

	public float getAngulo() {
		return angulo;
	}

	public void setAngulo(float angulo) {
		this.angulo = angulo;
	}

	public boolean isCamara() {
		return camara;
	}

	public void setCamara(boolean camara) {
		this.camara = camara;
	}

	public boolean isCanion() {
		return canion;
	}

	public void setCanion(boolean canion) {
		this.canion = canion;
	}

	public int getMuniciones() {
		return municiones;
	}

	public void setMuniciones(int municiones) {
		this.municiones = municiones;
	}

	public boolean isBomba() {
		return bomba;
	}

	public void setBomba(boolean bomba) {
		this.bomba = bomba;
	}

	public int getMotor() {
		return motor;
	}

	public void setMotor(int motor) {
		this.motor = motor;
	}

	public int getBlindaje() {
		return blindaje;
	}

	public void setBlindaje(int blindaje) {
		this.blindaje = blindaje;
	}
	
}
