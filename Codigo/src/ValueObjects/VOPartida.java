package ValueObjects;

import java.sql.Timestamp;

public class VOPartida {
	private String id;
	private Timestamp fecha;
	
	public VOPartida(String id, Timestamp fecha) {		
		this.id = id;
		this.fecha = fecha;		
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Timestamp getFecha() {
		return fecha;
	}

	public void setFecha(Timestamp fecha) {
		this.fecha = fecha;
	}	
	
}
