package Persistencia;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;


public class AccesoBD {

	public Connection obtenerConexion() throws Exception
	{				
		Connection con=null;		
		InitialContext context = new InitialContext();
		DataSource datasource = (DataSource) context.lookup("java:/mysql");			
		con = datasource.getConnection();		
		return con;
	}
	
	public void liberarConexion(Connection pCon) throws PersistenciaException
	{
		try {
			pCon.close();
		} catch (SQLException e) {
			throw new PersistenciaException("Error al guardar partida");
		}
	}
	
}
