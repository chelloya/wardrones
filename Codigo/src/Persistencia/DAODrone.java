package Persistencia;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import Logica.Drone;
import Logica.DroneAereo;
import Logica.Drones;
import Logica.DroneTerrestre;

public class DAODrone {

	public DAODrone ()
	{
		
	}
	
	public void insertarDrones (Drones pDrones, String idPartida) throws Exception
	{
		AccesoBD vABD = new AccesoBD();
		Connection pCon = vABD.obtenerConexion();	
		
		String insertarDroneAereo = Querys.insertarDroneAereo();
		String insertarDroneTerrestre = Querys.insertarDroneTerrestre();
		Drone aux=null;
		
		try 
		{
			for (int i=0;i<2;i++)
			{				
				PreparedStatement pstmtInsertarDroneTerrestre;
				pstmtInsertarDroneTerrestre = pCon.prepareStatement(insertarDroneTerrestre);
				aux=pDrones.obtenerDronPorRol(i);
				pstmtInsertarDroneTerrestre.setString(1,pDrones.getRolDrone(i));
				pstmtInsertarDroneTerrestre.setFloat(2,aux.getCoordX());
				pstmtInsertarDroneTerrestre.setFloat(3,aux.getCoordY());
				pstmtInsertarDroneTerrestre.setInt(4,aux.getCoordZ());
				pstmtInsertarDroneTerrestre.setFloat(5,aux.getAngulo());
				pstmtInsertarDroneTerrestre.setBoolean(6,aux.isCamara());
				pstmtInsertarDroneTerrestre.setBoolean(7,aux.isCanion());
				pstmtInsertarDroneTerrestre.setInt(8,aux.getMuniciones());			
				pstmtInsertarDroneTerrestre.setInt(9,((DroneTerrestre)aux).getBlindaje());
				pstmtInsertarDroneTerrestre.setString(10, idPartida);				
				pstmtInsertarDroneTerrestre.executeUpdate();
				pstmtInsertarDroneTerrestre.close();
			}
			
			for (int j=2;j<4;j++)
			{
				PreparedStatement pstmtInsertarDroneAereo;
				pstmtInsertarDroneAereo = pCon.prepareStatement(insertarDroneAereo);
				aux=pDrones.obtenerDronPorRol(j);
				pstmtInsertarDroneAereo.setString(1,pDrones.getRolDrone(j));
				pstmtInsertarDroneAereo.setFloat(2,aux.getCoordX());
				pstmtInsertarDroneAereo.setFloat(3,aux.getCoordY());
				pstmtInsertarDroneAereo.setInt(4,aux.getCoordZ());
				pstmtInsertarDroneAereo.setFloat(5,aux.getAngulo());
				pstmtInsertarDroneAereo.setBoolean(6,aux.isCamara());
				pstmtInsertarDroneAereo.setBoolean(7,aux.isCanion());
				pstmtInsertarDroneAereo.setInt(8,aux.getMuniciones());
				pstmtInsertarDroneAereo.setBoolean(9,((DroneAereo)aux).isBomba());
				pstmtInsertarDroneAereo.setInt(10,((DroneAereo)aux).getMotor());				
				pstmtInsertarDroneAereo.setString(11, idPartida);			
				pstmtInsertarDroneAereo.executeUpdate();
				pstmtInsertarDroneAereo.close();
			}
			vABD.liberarConexion(pCon);
		}
		catch (SQLException e) 
		{
			throw new PersistenciaException("Error al guardar partida");
		}
	}
	
	 public Drones obtenerDrones (String idPartida) throws Exception
	 {
		 	AccesoBD vABD = new AccesoBD();
			Connection pCon = vABD.obtenerConexion();		
			
			Drones vDrones = new Drones();
			DroneTerrestre vDt1 = new DroneTerrestre();
			DroneTerrestre vDt2 = new DroneTerrestre();
			DroneAereo vDa1 = new DroneAereo();
			DroneAereo vDa2 = new DroneAereo();
				
			String obtenerDrones = Querys.obtenerDrones();
			PreparedStatement pstmtObtenerDrones;
			try {
				pstmtObtenerDrones = pCon.prepareStatement(obtenerDrones);
				pstmtObtenerDrones.setString(1, idPartida);
				ResultSet rs = pstmtObtenerDrones.executeQuery();
				
				while (rs.next())
				{				
					String rol = rs.getString(1);
					float x = rs.getFloat(2);
					float y = rs.getFloat(3);
					int z = rs.getInt(4);				
					float angulo = rs.getFloat(5);
					boolean camara = rs.getBoolean(6);
					boolean canion = rs.getBoolean(7);
					int municiones = rs.getInt(8);					
					boolean bomba = rs.getBoolean(9);					
					int motor = rs.getInt(10);
					int blindaje = rs.getInt(11);
					
					switch(rol)
					{
						case "T1":
						{
							vDt1.setCoordX(x);
							vDt1.setCoordY(y);
							vDt1.setCoordZ(z);
							vDt1.setAngulo(angulo);
							vDt1.setCamara(camara);
							vDt1.setCanion(canion);
							vDt1.setMuniciones(municiones);
							vDt1.setBlindaje(blindaje);						
							
						}
						break;
						case "T2":
						{

							vDt2.setCoordX(x);
							vDt2.setCoordY(y);
							vDt2.setCoordZ(z);
							vDt2.setAngulo(angulo);
							vDt2.setCamara(camara);
							vDt2.setCanion(canion);
							vDt2.setMuniciones(municiones);
							vDt2.setBlindaje(blindaje);						
							
						}
						break;
						case "A1":
						{
							
							vDa1.setCoordX(x);
							vDa1.setCoordY(y);
							vDa1.setCoordZ(z);
							vDa1.setAngulo(angulo);
							vDa1.setCamara(camara);
							vDa1.setCanion(canion);
							vDa1.setMuniciones(municiones);
							vDa1.setBomba(bomba);
							vDa1.setMotor(motor);			
							
						}
						break;
						case "A2":
						{
							vDa2.setCoordX(x);
							vDa2.setCoordY(y);
							vDa2.setCoordZ(z);
							vDa2.setAngulo(angulo);
							vDa2.setCamara(camara);
							vDa2.setCanion(canion);
							vDa2.setMuniciones(municiones);
							vDa2.setBomba(bomba);
							vDa2.setMotor(motor);						
							
						}
						break;
					}
					
				}			
				rs.close();
				pstmtObtenerDrones.close();
				vABD.liberarConexion(pCon);				
				vDrones.setDroneTerrestre(vDt1, 0);
				vDrones.setDroneTerrestre(vDt2, 1);
				vDrones.setDroneAereo(vDa1, 2);
				vDrones.setDroneAereo(vDa2, 3);				
				
			} catch (SQLException e) {
				throw new PersistenciaException("Error al guardar partida");
			}		
			return vDrones;		 
	 }	
	
}
