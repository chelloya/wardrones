package Persistencia;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;

import ValueObjects.VOPartida;

public class DAOpartida 
{	
	public DAOpartida ()
	{
		
	}
	
	public void insert (String pId) throws Exception
	{
		AccesoBD vABD = new AccesoBD();
		Connection pCon = vABD.obtenerConexion();
		
		String insertarPartida = Querys.insertarPartida();
		
		try 
		{
			PreparedStatement pstmtInsertarPartida;
			pstmtInsertarPartida = pCon.prepareStatement(insertarPartida);	
			pstmtInsertarPartida.setString(1,pId);				
			pstmtInsertarPartida.executeUpdate();			
			pstmtInsertarPartida.close();			
			vABD.liberarConexion(pCon);
		}
		catch (SQLException e) 
		{
			throw new PersistenciaException("Error al guardar partida");
		}
	}
	
	 public ArrayList <VOPartida> listarPartidas () throws Exception
	 {
		AccesoBD vABD = new AccesoBD();
		Connection pCon = vABD.obtenerConexion();		
		 
		ArrayList <VOPartida> auxList = new ArrayList<VOPartida>();
			
		String listarPartidas = Querys.listarPartidas();
		Statement stmtListarVOPartidas;
		try {
			stmtListarVOPartidas = pCon.createStatement();
			ResultSet rs = stmtListarVOPartidas.executeQuery(listarPartidas);
			
			while (rs.next())
			{
				String idPartida = rs.getString(1);				
				Timestamp fecha = rs.getTimestamp(2);
				
				VOPartida aux= new VOPartida(idPartida,fecha);
				
				auxList.add(aux);
			}
			
			rs.close();
			stmtListarVOPartidas.close();
			vABD.liberarConexion(pCon);
		} catch (SQLException e) {
			throw new PersistenciaException("Error al guardar partida");
		}		
		return auxList;	 
	 }
}
