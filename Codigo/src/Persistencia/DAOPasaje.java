package Persistencia;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import Logica.Puente;

public class DAOPasaje {
	
	public DAOPasaje ()
	{
		
	}
	
	public void insertarPasajes (Puente pPuentes[], String idPartida) throws Exception
	{
		AccesoBD vABD = new AccesoBD();
		Connection pCon = vABD.obtenerConexion();	
		
		String insertarPasaje = Querys.insertarPasaje();
		
		try 
		{
			for (int i=0;i<13;i++)
			{
				PreparedStatement pstmtInsertarPasaje;
				pstmtInsertarPasaje = pCon.prepareStatement(insertarPasaje);
				pstmtInsertarPasaje.setFloat(1,pPuentes[i].getX1());
				pstmtInsertarPasaje.setFloat(2,pPuentes[i].getY1());
				pstmtInsertarPasaje.setFloat(3,pPuentes[i].getX2());
				pstmtInsertarPasaje.setFloat(4,pPuentes[i].getY2());
				pstmtInsertarPasaje.setFloat(5,pPuentes[i].getX3());
				pstmtInsertarPasaje.setFloat(6,pPuentes[i].getY3());
				pstmtInsertarPasaje.setFloat(7,pPuentes[i].getX4());
				pstmtInsertarPasaje.setFloat(8,pPuentes[i].getY4());
				pstmtInsertarPasaje.setBoolean(9, pPuentes[i].isEsPuente());
				pstmtInsertarPasaje.setString(10, idPartida);			
				pstmtInsertarPasaje.executeUpdate();
				pstmtInsertarPasaje.close();
			}
			vABD.liberarConexion(pCon);
		}
		catch (SQLException e) 
		{
			throw new PersistenciaException("Error al guardar partida");
		}
	}
	
	 public Puente[] obtenerPasajes (String idPartida) throws Exception 
	 {
		 AccesoBD vABD = new AccesoBD();
			Connection pCon = vABD.obtenerConexion();		
			 
			Puente auxList[] = new Puente[13];
				
			String obtenerPasajes = Querys.obtenerPasajes();
			PreparedStatement pstmtObtenerPasajes;
			try {
				pstmtObtenerPasajes = pCon.prepareStatement(obtenerPasajes);
				pstmtObtenerPasajes.setString(1, idPartida);
				ResultSet rs = pstmtObtenerPasajes.executeQuery();
				
				int i=0;
				while (rs.next())
				{
					float x1 = rs.getFloat(1);
					float y1 = rs.getFloat(2);
					float x2 = rs.getFloat(3);
					float y2 = rs.getFloat(4);
					float x3 = rs.getFloat(5);
					float y3 = rs.getFloat(6);					
					float x4 = rs.getFloat(7);
					float y4 = rs.getFloat(8);
					boolean esPuente = rs.getBoolean(9);
					
					auxList[i]=new Puente();
					auxList[i].setX1(x1);
					auxList[i].setY1(y1);
					auxList[i].setX2(x2);
					auxList[i].setY2(y2);
					auxList[i].setX3(x3);
					auxList[i].setY3(y3);
					auxList[i].setX4(x4);
					auxList[i].setY4(y4);
					auxList[i].setEsPuente(esPuente);
					i++;
				}
				
				rs.close();
				pstmtObtenerPasajes.close();
				vABD.liberarConexion(pCon);
			} catch (SQLException e) {
				throw new PersistenciaException("Error al guardar partida");
			}		
			return auxList;		 	 
	 }

}
