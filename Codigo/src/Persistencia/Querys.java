package Persistencia;

public class Querys {
	
	public static String obtenerPartida()
	{
		return "SELECT * FROM partidas WHERE id=(?)";		
	}
	
	public static String insertarPartida()
	{
		return "INSERT INTO partidas (id) VALUES (?)";
	}
	
	public static String listarPartidas()
	{
		return "SELECT * FROM partidas";		
	}
	
	public static String insertarPasaje()
	{
		return "INSERT INTO pasajes VALUES (?,?,?,?,?,?,?,?,?,?)";
	}
	
	public static String obtenerPasajes()
	{
		return "SELECT * FROM pasajes WHERE idPartida=(?)";
	}
	
	public static String insertarBase()
	{
		return "INSERT INTO bases VALUES (?,?,?,?,?)";
	}
	
	public static String obtenerBase()
	{
		return "SELECT * FROM bases WHERE idPartida=(?)";
	}
	
	public static String insertarDrone()
	{
		return "INSERT INTO drones (rol, coordX, coordY, coordZ, angulo, camara, canion, municiones, bomba, motor, blindaje, idPartida) VALUES(?,?,?,?,?,?,?,?,?,?,?,?)";		
	}
	
	public static String insertarDroneAereo()
	{
		return "INSERT INTO drones (rol, coordX, coordY, coordZ, angulo, camara, canion, municiones, bomba, motor, idPartida) VALUES(?,?,?,?,?,?,?,?,?,?,?)";		
	}
	
	public static String insertarDroneTerrestre()
	{
		return "INSERT INTO drones (rol, coordX, coordY, coordZ, angulo, camara, canion, municiones, blindaje, idPartida) VALUES(?,?,?,?,?,?,?,?,?,?)";		
	}
	
	public static String obtenerDrones()
	{
		return "SELECT * FROM drones WHERE idPartida=(?)";
	}

}
