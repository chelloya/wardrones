package Persistencia;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import Logica.Base;
import ValueObjects.VOBase;

public class DAOBase {

	public DAOBase ()
	{
		
	}
	
	public VOBase find (String pId) throws Exception
	{
		
		AccesoBD vABD = new AccesoBD();
		Connection pCon = vABD.obtenerConexion();	
		
		VOBase base = new VOBase();
		
		String obtenerDatosBase = Querys.obtenerBase();
		PreparedStatement pstmtObtenerDatosBase;
		try {
			pstmtObtenerDatosBase = pCon.prepareStatement(obtenerDatosBase);
			pstmtObtenerDatosBase.setString(1, pId);
			
			ResultSet rs = pstmtObtenerDatosBase.executeQuery();
			rs.next();
			
			float x1 = rs.getFloat(1);
			float y1 = rs.getFloat(2);
			int vidaPolvorin = rs.getInt(3);
			int vidaPista = rs.getInt(4);
			
			rs.close();
			pstmtObtenerDatosBase.close();
			vABD.liberarConexion(pCon);
			
			base.setCoordX1(x1);
			base.setCoordY1(y1);
			base.setVidaPolvorin(vidaPolvorin);
			base.setVidaPista(vidaPista);
			
		} catch (SQLException e) {
			throw new PersistenciaException("Error al guardar partida");
		}

		return base;
		
	}
	
	public void insert (Base pBase, String idPartida) throws Exception
	{
		AccesoBD vABD = new AccesoBD();
		Connection pCon = vABD.obtenerConexion();	
		
		String insertarBase = Querys.insertarBase();
		
		try 
		{
			PreparedStatement pstmtInsertarBase;
			pstmtInsertarBase = pCon.prepareStatement(insertarBase);
			pstmtInsertarBase.setFloat(1,pBase.getCoordX1());
			pstmtInsertarBase.setFloat(2,pBase.getCoordY1());
			pstmtInsertarBase.setFloat(3,pBase.getVidaPolvorin());
			pstmtInsertarBase.setFloat(4,pBase.getVidaPista());
			pstmtInsertarBase.setString(5, idPartida);			
			pstmtInsertarBase.executeUpdate();
			pstmtInsertarBase.close();			
			vABD.liberarConexion(pCon);
		}
		catch (SQLException e) 
		{
			throw new PersistenciaException("Error al guardar partida");	
		}
	}
}
